#include "VoicePrompt.h"
//#include "board.h"
extern int Mp3PlayData(unsigned char *buf, int len, unsigned int timeout);


const unsigned char WifiConnecting[] = {
       #include "../sound_files/WifiConnecting.dat"
       };
const unsigned char WifiConnectSucess[] = {
       #include "../sound_files/WifiConnectSucess.dat"
       };
const unsigned char WifiConnectError[] = {
       #include "../sound_files/WifiConnectError.dat"
       };
const unsigned char VoiceTest[] = {
       #include "../sound_files/VoiceTest.dat"
       };

bool_t VoicePrompt(Prompttone cmd)
{
    int len;

    if(!Get_CloseScreen_Statue())
    {
        OpenSpeaker();
        switch (cmd)
        {
            case   wifi_connecting:
                len = sizeof(WifiConnecting);
                Mp3PlayData(WifiConnecting, len, 0xFFFFFFFF); //连网中
                Mp3DataEndPlay();
                break;
            case wifi_connect_sucess:
                len = sizeof(WifiConnectSucess);
                Mp3PlayData(WifiConnectSucess, len, 0xFFFFFFFF); //网络连接成功
                Mp3DataEndPlay();
                break;
            case wifi_connect_error:
                len = sizeof(WifiConnectError);
                Mp3PlayData(WifiConnectError, len, 0xFFFFFFFF); //网络连接失败
                Mp3DataEndPlay();
                break;
            case Voice_Test:
                len = sizeof(VoiceTest);
                Mp3PlayData(VoiceTest, len, 0xFFFFFFFF); //网络连接失败
                Mp3DataEndPlay();
                break;
            default:
                break;
        }
    }
//    CloseSpeaker();
    return false;
}


