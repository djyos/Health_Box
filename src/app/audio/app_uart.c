#include "stdint.h"
#include "stddef.h"
#include "cpu_peri.h"
#include "uartctrl.h"
#if 0
#include "cJSON.h"
#include "mongoose.h"

typedef struct StWorkData {
    int status;
    char *new_data;
    int new_len;
}StWorkData;

s32 uartfd;
char ucDataBuf[100];
u8 recv_flag = 0;
//char sn_tmp[50];

//extern int login_ok;
extern void synDateWithHttpPkg(char* buf);
unsigned int GetRtcTickSecTimestamp(void);

bool_t Uart_Open()
{
    uartfd = open("/dev/UART1",O_RDWR,0);
    if(uartfd == -1)
    {
        printf("/dev/uart1 open error \n\r");
        return false;
    }
    printf("/dev/uart1 open success \n\r");

    return true;
}

s32 Uart_Send(const void *buf, size_t count)
{
    u32 ret = 0;
    djy_gpio_write(GPIO5,0);
    DJY_DelayUs(500);
    djy_gpio_write(GPIO5,1);
    ret =  write(uartfd,buf,count);
    printf("ret = %d \n\r",ret);
    return ret;
}

#if 0
void DecodeForServer(char* str)
{
    int i = 0;
    u8 first=0;
    u8 sendBuff[6] = {0x45,0x04,0x00,0x00,0x00,0x00};
    char *p = NULL;
    char estate[3] = {0};
    char *oldP = str;
    uint8 data[16] = {0};         //***zhai adjust 20-->30 该数组长度对插头与APP的协议长度有限制作用
    int bufLen = 0;

    printf("\n\r\n\rWebSocket str : %s\r\n" , oldP); //读到的数据打印出来

    while((NULL != (p = strchr(oldP, ':'))) && (0 < strlen(oldP)))
    {
        bufLen = strlen(oldP)-strlen(p);
//        printf("\n\r\n\rbufLen : %d\r\n" , bufLen); //读到的数据打印出来
        memset(estate, 0, sizeof(estate));
        if(!first)
        {
            first = 1;
            oldP ++;
            memcpy(estate, (char*)oldP, bufLen-1);
        }else
            memcpy(estate, (char*)oldP, bufLen);
        data[i++] = (uint8)atoi(estate);
        oldP = p+1;
    }
    memset(estate, 0, sizeof(estate));
    memcpy(estate, (char*)oldP, strlen(oldP)-1);
    data[i++] = (uint8)atoi(estate);

    sendBuff[2] = data[0];
    sendBuff[3] = data[1];
    sendBuff[4] = data[2];

    for(i = 0; i < sizeof(sendBuff)- 1; i++) {
       sendBuff[5] +=  sendBuff[i];
    }

//    for(i=0;i<6;i++)
//        printf("send uartBuf[%d]%x \r\n",i,sendBuff[i]);

    Uart_Send(sendBuff,sizeof(sendBuff));
}

//返回数据给服务器
void SetStatue(uint8* uartBuf, uint16 bufLen)
{
    u8 i ;
    for(i=0;i<bufLen;i++)
        printf("recv uartBuf[%d]%x \r\n",i,uartBuf[i]);
}

//void DecodeForMCU(uint8* uartBuf, uint16 bufLen)
//{
//    if(NULL == uartBuf)
//        return;

//    switch(uartBuf[2])
//    {
//        case SOUND_MODE:
//        case VOICE_MODE:
//        case DODGE:
//        case VOICES_ELIMINATE:
//        case MIC_VOLUME:
//        case REVERB_VOLUME:
//        case REVERB_TIME:
//        case MUSIC_VOLUME:
//        case MIC_BASS:
//        case MIC_TREBLE:
//             SetStatue(uartBuf,bufLen);
//             break;
//        default:
//            break;
//    }
//}
#endif

//编码一个url
void urlencode(const char* url,int len)
{
    int i = 0;
    int res_len = 0;
    char res[100];
    unsigned char highByte, lowByte;

//    len = strlen(url);
    for (i = 0; i < len; ++i)
    {
        highByte = (url[i] >> 4) & 0x0f;
        lowByte = url[i] & 0x0f ;

        highByte += 0x30;
        if (highByte > 0x39)
            res[res_len++] = highByte + 0x07;
        else
            res[res_len++] = highByte;

        lowByte += 0x30;
        if (lowByte > 0x39)
            res[res_len++] = lowByte + 0x07;
        else
            res[res_len++] = lowByte;

        if(i<(len-1))
        {
            res[res_len++] = '%';
            res[res_len++] = '2';
            res[res_len++] = '0';
        }
    }
    res[res_len] = '\0';
    strcpy(url, res);
}

ptu32_t uart_recv(void)
{
//    s32 i;
    while(1)
    {
//        i=0;
        s32 len = read(uartfd,ucDataBuf,20);
        if(len > 0)
        {
//            for(i=0;i<len;i++)
//            {
                urlencode(ucDataBuf,len);
                printf("uart_recv ucDataBuf = %s\r\n",ucDataBuf);
                recv_flag = 1;
//                login_ok = 0;
//            }
//            if(ucDataBuf[0] == '{')
//            {
//                i += len;
//                while(i<(s32)sizeof(ucDataBuf))
//                {
//                    len = read(uartfd,&ucDataBuf[i],sizeof(ucDataBuf)-i);
//                    i += len;
//                }
//                printf("ucDataBuf = %s\r\n",ucDataBuf);
////                DecodeForMCU(ucDataBuf,i);
//            }
        }
    }
    return 0;
}

int DoHttpBody(char *body, int len, char *outbuf)
{
    int ret = 0;
    cJSON *cjson = 0;
    if (body == 0 || len <= 0) return -1;

    cjson = cJSON_Parse(body);
    if (cjson)
    {
        cJSON*  results = cJSON_GetObjectItem(cjson, "result");
        if (results)
        {
            printf("submit success: %s\r\n", results->valuestring);
            if (strcmp(results->valuestring, "SUCCESS") == 0)
            {
                cJSON* subjson = cJSON_GetObjectItem(cjson, "data");
                if (subjson)
                {
                    results = cJSON_GetObjectItem(subjson, "deviceKey");
                    if(results)
                    {
                        ret = strlen(results->valuestring) + 1;
                        strcpy(outbuf, results->valuestring);
                        printf("message: %s\r\n", results->valuestring);
                    }

                }
            }
        }
    }

//END_FUN:
    if (cjson) cJSON_Delete(cjson);
//    if (pnew) free(pnew);
//    if (pJsonStr) free(pJsonStr);
    return ret;
}

static void cb_ev_handler(struct mg_connection *nc, int ev, void *ev_data) {
    struct http_message *hm = (struct http_message *) ev_data;
    struct StWorkData *pQuestData = (struct StWorkData*)nc->user_data;
    char *p = 0;
//
    int is_chunked = 0;
    struct mg_str *s;

    if (hm) {
        if ((s = mg_get_http_header(hm, "Transfer-Encoding")) != NULL &&
            mg_vcasecmp(s, "chunked") == 0) {
            is_chunked = 1;
        }
    }
    printf("-----ev %d\r\n",ev);
    switch (ev) {
    case MG_EV_CONNECT:
        printf("MG_EV_CONNECT\r\n");
        if (*(int *)ev_data != 0) {
            /*  fprintf(stderr, "connect() failed: %s\n", strerror(*(int *)ev_data));*/
            pQuestData->status = -1;
        }
        if (pQuestData->new_data) {
            free(pQuestData->new_data);
        }
        pQuestData->new_data = 0;
        pQuestData->new_len = 0;
        break;
    case MG_EV_HTTP_REPLY:
    {
        printf("hm->resp_code = %d\r\n",hm->resp_code);
        switch (hm->resp_code) {
        case 200:
            nc->flags |= MG_F_CLOSE_IMMEDIATELY;
            if(GetRtcTickSecTimestamp() == 0)
            {
                for (int i = 0; i<10; i++) {
                    char *pname = malloc(hm->header_names[i].len+1);
                    char *pval = malloc(hm->header_values[i].len+1);
                    if (pname){
                        memset(pname, 0, hm->header_names[i].len+1);
                        memcpy(pname, hm->header_names[i].p, hm->header_names[i].len);
                        printf("name = %s\r\n", pname);
                    }
                    if (pval){
                        memset(pval, 0, hm->header_values[i].len+1);
                        memcpy(pval, hm->header_values[i].p, hm->header_values[i].len);
                    }
                    if(0==strcmp(pname,"Date"))//返回
                    {
                        synDateWithHttpPkg(pval);
                        if (pname) free(pname);
                        if (pval) free(pval);
                        break;
                    }
                    if (pname) free(pname);
                    if (pval) free(pval);
                }

            if (hm->body.len <= 0) {
                if (pQuestData->status == 0) pQuestData->status = -1;
                break;
            }
            }
            //process the whole body.
            pQuestData->status = -1;
            p = realloc(pQuestData->new_data, hm->body.len+1);
            if (p == 0) {
                printf("error: cb_ev_handler->realloc MG_EV_HTTP_REPLY, p==null!\r\n");
                break;
            }
            pQuestData->new_data = p;
            pQuestData->new_len = hm->body.len+1;
            memcpy(pQuestData->new_data, hm->body.p, hm->body.len);
            pQuestData->new_data[hm->body.len] = 0;

            if (DoHttpBody(pQuestData->new_data, pQuestData->new_len, pQuestData->new_data) > 0) {
//                printf("%s\r\n", pQuestData->new_data);
                pQuestData->status = 1;
            }
            break;
        case 302:
            pQuestData->status = -302;
            break;
        case 401:
            pQuestData->status = -401;
            break;
        default:
            pQuestData->status = -1;
            break;
        }
        break;
    }

    case MG_EV_HTTP_CHUNK:
    {
        if (!is_chunked) break;

        nc->flags = MG_F_DELETE_CHUNK;
//        hex_dump("cb_http_upload_handler MG_EV_HTTP_CHUNK:", hm->body.p, hm->body.len);
        if (hm->body.len > 0) {
            p = realloc(pQuestData->new_data, pQuestData->new_len + hm->body.len);
            if (p == 0) {
                printf("error: cb_ev_handler->realloc MG_EV_HTTP_CHUNK, p==null!\r\n");
                break;
            }
            pQuestData->new_data = p;
            memcpy(&pQuestData->new_data[pQuestData->new_len], hm->body.p, hm->body.len);
            pQuestData->new_len += hm->body.len;
        }
        else if (hm->body.len == 0) {//end flag
            if (pQuestData->status != 0) break;
            p = realloc(pQuestData->new_data, pQuestData->new_len+1);
            if (p == 0) {
                printf("error: cb_ev_handler->realloc MG_EV_HTTP_CHUNK, p==null!\r\n");
                break;
            }
            pQuestData->new_data = p;
            pQuestData->new_data[pQuestData->new_len] = 0;
            pQuestData->new_len += 1;

            //process the whole body.
            pQuestData->status = -1;

            if (DoHttpBody(pQuestData->new_data, pQuestData->new_len, pQuestData->new_data) > 0) {
                printf("%s\r\n", pQuestData->new_data);
                pQuestData->status = 1;
            }
        }
        break;
    }
    case MG_EV_CLOSE:
//        printf("MG_EV_CLOSE\r\n");
        if (pQuestData && pQuestData->status==0)
        {
           pQuestData->status = -1;
        }
        break;
    default:
//        printf("default\r\n");
        break;
    }
}

int DevGetDeviceKeychar(char *out_json, int len)
{
    char *path  = 0;
    char *meta = 0;
    char *url = 0;
    struct mg_connection *nc;
    struct mg_mgr mgr;
    int ret = 0;
    struct StWorkData user_data;
    memset(&user_data, 0, sizeof(user_data));

    printf("ENTRY: %s!\r\n", __FUNCTION__);

    mg_mgr_init(&mgr, NULL);

    path = (char*)malloc(200);
    if (path == 0) return -1;

    meta = (char*)malloc(1024);
    if (meta == 0) return -1;

    url = (char*)malloc(1024);
    if (url == 0) return -1;

    user_data.new_len = 0;
    user_data.new_data = 0;

    sprintf(path, "/user-app/healthCabin/authorizeKey?deviceID=%s",ucDataBuf);

    strcpy(meta,
        "Accept: */*\r\n"
        "Connection: keep-alive\r\n"
        "User-Agent: Mongoose/6.15\r\n"
        "Cache-Control: no-cache\r\n"
        "Content-Type: application/json\r\n");

    sprintf(url, "http://%s%s","47.97.254.199", path);
    nc = mg_connect_http(&mgr, cb_ev_handler, url, meta, "123");
    if (nc == 0) goto END_QUEST;
    nc->user_data = &user_data;
    unsigned int timemark = DJY_GetSysTime()/1000;
    unsigned int timeout_ms = 10000;
    while (user_data.status == 0) {
        if (DJY_GetSysTime()/1000-timemark > timeout_ms) {
            printf("DevGetCommon: timeout break!\r\n");
            break;
        }
        mg_mgr_poll(&mgr, 500);
        DJY_EventDelay(10*1000);
    }
    if (user_data.status == 1) {
        if (user_data.new_data) {
            int min = strlen(user_data.new_data) + 1;
            min = min < len ? min : len;
            memcpy(out_json, user_data.new_data, min);
            user_data.status = min;
        }
        else {
            user_data.status = 0;
        }
    }
    ret = user_data.status;
END_QUEST:
    mg_mgr_free(&mgr);
    if (path) free(path);
    if (meta) free(meta);
    if (url) free(url);
    if (user_data.new_data) {
        free(user_data.new_data);
        user_data.new_data = 0;
        user_data.new_len = 0;
    }
    return ret;
}
int  Get_DeviceKey(char *buf,int buflen)
{
    int i;
    int ret = 0;
    char  DevId[]= "{SFAHTIDR}";

//    if(buf == NULL)
//        return -1;
    Uart_Send(DevId,sizeof(DevId));

    if(recv_flag)
    {
        recv_flag = 0;
        for(i=0;i<3;i++)
        {
            ret = DevGetDeviceKeychar(buf, buflen);
            if(ret > 0)
                break;
        }
    }
    return ret;
}

int GetDeviceKey(void)
{
    char buf[1024] = { 0 };

    printf("ENTRY: %s!\r\n", __FUNCTION__);
//    if (!is_wifi_connected()) return -1;
    int ret = DevGetDeviceKeychar(buf, sizeof(buf));
//    memset(sn_tmp, 0, strlen(sn_tmp));
//    memcpy(sn_tmp, buf, ret);
//    printf("ret = %d,buf: %s\r\n",ret, sn_tmp);
    return ret;
}
#endif
