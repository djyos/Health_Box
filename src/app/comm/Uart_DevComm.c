//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * Dev_Comm.c
 *
 *  Created on: 2020年1月3日
 *      Author: czz
 */

#include <Net_DevApi.h>
#include "uartctrl.h"
#include "stdlib.h"
#include "Uart_DevComm.h"
#include "gpio_pub.h"
#include "board.h"
#include "app_flash.h"
struct UartCommHandle{
    s32 uartfd;
    unsigned char devid[32];
    int idlen;
    u16 evtt;
    char *computeOrder;
    size_t maxsize;
};
#define RESULT_BUF   1024

#ifndef free
#define free(x) M_Free(x)
#endif

struct UartCommHandle p_uartcommCb;
void djy_gpio_write(GPIO_INDEX pin, uint32_t value);

extern void Set_UartRecFlag(int statue);

unsigned char Get_Devid(size_t cnt)
{
    return p_uartcommCb.devid[cnt];
}

s32 Uart_Send(s32 fd, char *buf, size_t count)
{
    u32 ret = 0;
//    djy_gpio_write(GPIO5,0);
//    DJY_DelayUs(500);
    djy_gpio_write(GPIO5,1);
    DJY_DelayUs(500);
    djy_gpio_write(GPIO5,0);
    ret =  write(fd,buf,count);
//    printf("Uart_Send = %s\r\n",buf);
    return ret;
}

char * Get_NextCmd(char *input,size_t *len)
{
    char *ret = NULL;
    size_t i = 0;
    size_t j = 0;
    if(*input == '{')
        input++;
    size_t str_len = strlen(input);
    *len = 0;
    for(i=0;i<str_len;i++)
    {
        if(input[i] == 0)
            break;
        if(input[i] == '{')
        {
            ret = &input[i];
            break;
        }
    }
    if(i<str_len)
    for(j=i;j<str_len;j++)
    {
        if(input[j] == 0)
            break;
        if(input[j] == '}')
        {
           *len = j-i+1;
           break;
        }
    }
    return ret;
}

size_t Get_CmdCount(char *input)
{
    size_t ret = 0;
    size_t i = 0;
    size_t str_len = strlen(input);

    for(i=0;i<str_len;i++)
    {
        if(input[i] == '{')
        {
            ret++;
        }
    }
    return ret;
}

#include <shell.h>

#define CLOSE_UART_DEBUG  1

ADD_TO_IN_SHELL_DATA u32 dt = 100;
static ptu32_t Uart_Event(void)
{
    int readzerocnt = 0;
    size_t readlen = 0;
    int  ret = 0;
    size_t UartSend_cnt = 0;
    s64 timebak = 0;

    printf("Uart_Event run\r\n");
    Start_DevResultCallback callbackfun;
    int id;
    DJY_GetEventPara((ptu32_t*)&callbackfun,(ptu32_t*)&id);
    s32 len = 0;
    size_t NeedDataLen = Get_ComputeOrderRcvlen();
    s32 Rcvlenofcnt = 0;
    UartSend_cnt = Get_ComputeOrderNum();
    printf("UartSend_cnt = %d\r\n",UartSend_cnt);

//    if(GetSamlpingState() == Samlping_off)
//        EnableSampling();

    char  *pcmmd= "{SFAHTIDR}";
    p_uartcommCb.idlen = 0;
    while(1)
    {
        if(GetSamlpingState() == Samlping_off)
        {
            EnableSampling();
            printf("%s ,%d\r\n",__func__,__LINE__);
            return ret;
        }
        len = strlen(pcmmd);
        if(len != Uart_Send(p_uartcommCb.uartfd,pcmmd,len))
        {
            ret = -4;
//            goto Error_Del;
        }

        DJY_EventDelay(10*mS);
        len = read(p_uartcommCb.uartfd,&p_uartcommCb.devid[readlen],sizeof(p_uartcommCb.devid)-readlen);

        if((ret++ >= 5)||(len != 0))
        {
            p_uartcommCb.idlen = len;
            break;
        }

        if(len == 0)
            printf("error : read dev id error \n\r");
    }

    printf("--------ret = %d,len = %d------\n\r",ret,len);
    if(ret < 5)
    {
        for(size_t i=0;i<UartSend_cnt;i++)
        {
            Rcvlenofcnt = 0;
            readzerocnt = 0;
            struct computeOrder*ComputeOrder = Get_ComputeOrderOfcnt(i+1);
            if(ComputeOrder == NULL)
                return -5;
            size_t send_len = strlen(ComputeOrder->data);

            printf("send_len = %d,ComputeOrder->data = %s \r\n",send_len,ComputeOrder->data);
            if(ComputeOrder->data != NULL)
            {
                if((s32)send_len != Uart_Send(p_uartcommCb.uartfd,ComputeOrder->data,send_len))
                {
                    printf("error: uart start Get Result !!!\n\r");
                    return -1;
                }
            }
            while(1)
            {
                if(!djy_gpio_read(GPIO8))
                {
                    if(timebak == 0)
                        timebak = DJY_GetSysTime();
                    if((DJY_GetSysTime() - timebak) > 1000*mS)
                    {
                        DisableSampling();
                        Set_UartRecFlag(5);
                        printf("%s ,%d\r\n",__func__,__LINE__);
                        return ret;
                    }
                }else{
                    timebak = 0;
                }

                if(GetSamlpingState() == Samlping_off)
                {
                    EnableSampling();
                    printf("%s ,%d\r\n",__func__,__LINE__);
                    return ret;
                }

                if(p_uartcommCb.maxsize <= readlen)
                {
                    printf("error: computeOrder buf overflow \r\n");
                    break;
                }
                len = read(p_uartcommCb.uartfd,&p_uartcommCb.computeOrder[readlen],p_uartcommCb.maxsize-readlen);
                readlen += len;
                Rcvlenofcnt+=len;

                Set_UartProgerss((readlen*100)/NeedDataLen);
                if(len==0)
                {
//                    if(readzerocnt++ > 100 || Rcvlenofcnt > NeedDataLen)
                    if(readzerocnt++ > 100 || Rcvlenofcnt >= ComputeOrder->rcvlen)
                    {
                        printf("uart readlen = %d\r\n",readlen);
                        break;
                    }
                    DJY_EventDelay(10*mS);
                }else
                {
                    readzerocnt = 0;
                }
            }
        }
    }
    if(p_uartcommCb.maxsize-readlen>0)
        p_uartcommCb.computeOrder[readlen]='\0';
    else
        printf("error:ComputeOrder buf overflow \r\n");

    if(NeedDataLen!=readlen || readlen == 0)
    {
        Set_UartRecFlag(1);
        printf("uart readlen error,NeedDataLen = %d readlen = %d\r\n",NeedDataLen,readlen);
        return ret;
    }
    Set_UartRecFlag(2);
    ret = Submit_DevdResult(p_uartcommCb.computeOrder);
    if(ret < 0)
    {
        printf("error:Submit_DevdResult  ret %d \r\n",ret);
    }
    if(callbackfun!=NULL)
        callbackfun(id,ret);

    return ret;
}


int Start_DevResult(Start_DevResultCallback callback,int id)
{
    int  ret;
//    char *ch;
    char buf[10];
    size_t len ;
 
    printf("Start_DevResult \r\n");
#if CLOSE_UART_DEBUG
//    char* ComputeOrder = Get_DeviceComputeOrder(0);
//    if(ComputeOrder == NULL)
//    {
//        return -5;
//    }
#else
    ch = "\r\nTM00001,{SKAFCKAFJDBHAHAJLKAFACCBLBAFVJHDLCAFVJHDAFNCAFKFAFIBAFHBDR},TM00002,{SKAFCKAFFKBHAHAJLKAFACCBLBAFVJHDLCAFVJHDAFNCAFKFAFIBAFHBDR}";
    char* ComputeOrder = ch;
#endif
//    size_t len = strlen(ComputeOrder);
    if(0 > fcntl(p_uartcommCb.uartfd,F_SETTIMEOUT,100*mS))
    {
        return -2;
    }
    int readzerocnt = 0;
    while(1) //清空上一次的数据
    {
        len = read(p_uartcommCb.uartfd,buf,sizeof(buf));
        if(len == 0)
        {
            if(readzerocnt++ > 3)
                break;
            DJY_EventDelay(100*mS);
        }else
        {
            readzerocnt = 0;
        }
    }
//    ch = ComputeOrder;
//    UartSend_cnt= Get_CmdCount(ch);
//    printf("UartSend_cnt = %d\r\n",UartSend_cnt);
    ret = (int)DJY_EventPop(p_uartcommCb.evtt, NULL, 0, (ptu32_t)callback, (ptu32_t)id, 0);
//    do{
//        ch = Get_NextCmd(ch,&len);
//        if((ch != NULL)&&(len != 0))
//        {
//            if((s32)len != Uart_Send(p_uartcommCb.uartfd,ch,len))
//            {
//                printf("error: uart start Get Result !!!\n\r");
//                return -1;
//            }
//        }else{
//            break;
//        }
//    }while(ch != NULL);
    return ret;
}



char * Get_DevResult()
{
    char * ret = NULL;
    if(p_uartcommCb.computeOrder == NULL)
        return NULL;
    else
        ret = p_uartcommCb.computeOrder;
    return ret;
}


int Uart_GetDevid(char *idbuf ,int maxlen)
{
    if(p_uartcommCb.uartfd <= 0)
        return -2;
    if(maxlen < p_uartcommCb.idlen)
        return -3;

    memcpy(idbuf,p_uartcommCb.devid, p_uartcommCb.idlen);
    return  p_uartcommCb.idlen;
}

int Dev_UarCommInit(const char * devname,size_t result)
{
    int ret = 0;
    s32 len = 0;
    unsigned char Devidbuf[8];
//    p_uartcommCb = New_UartCommHandle();
//    if(p_uartcommCb == NULL)
//        return -1;

    p_uartcommCb.computeOrder =  malloc(result);
    if( p_uartcommCb.computeOrder == NULL)
    {
        ret = -101;
        goto Error_Del;
    }

    p_uartcommCb.maxsize = result;
    p_uartcommCb.evtt = DJY_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS+1, 0, 1,
            Uart_Event,NULL, 0x1000, "Uart Event");

    if(p_uartcommCb.evtt == CN_EVTT_ID_INVALID)
    {
        printf("error : Dev_UarCommInit Init evtt: NULL    \n\r");
        ret = -100;
        goto Error_Del;
    }

    p_uartcommCb.uartfd = open(devname,O_RDWR,0);
    if(p_uartcommCb.uartfd <= 0)
    {
        ret = -2;
        goto Error_Del;
    }
    if(0 > fcntl(p_uartcommCb.uartfd,F_SETTIMEOUT,100*mS))
    {
        ret = -3;
        goto Error_Del;
    }

//    if(GetSamlpingState() == Samlping_off)
//        EnableSampling();

    char  *pcmmd= "{SFAHTIDR}";
//    char ch = '\0';
//    Uart_Send(p_uartcommCb.uartfd,&ch,1);//todo:第一个字节没有发出去'
    s32 readlen = 0;
    p_uartcommCb.idlen = 0;
    while(1)
    {
        if(GetSamlpingState() == Samlping_off)
            EnableSampling();

        len = strlen(pcmmd);
        if(len != Uart_Send(p_uartcommCb.uartfd,pcmmd,len))
        {
            ret = -4;
            goto Error_Del;
        }

        DJY_EventDelay(10*mS);
        len = read(p_uartcommCb.uartfd,&p_uartcommCb.devid[readlen],sizeof(p_uartcommCb.devid)-readlen);

        if((ret++ >= 5)||((len != 0) && (p_uartcommCb.devid[0]) == 0x04))
        {
            p_uartcommCb.idlen = len;
            break;
        }

        if(len == 0)
        {
            printf("error : read dev id error \n\r");
            DisableSampling();
        }
    }

#if 1
    if(p_uartcommCb.idlen > 0)
    {
        printf("p_uartcommCb.idlen = %d\r\n",p_uartcommCb.idlen);
        if(0!=memcmp(Devidbuf,p_uartcommCb.devid,8))
        {
            DevidSave(p_uartcommCb.devid,p_uartcommCb.idlen);
        }
    }
#endif

    DevidLoad(Devidbuf,8);
    printf("Load devid = %s\r\n",Devidbuf);

    if(Devidbuf != NULL)
    {
        memcpy(p_uartcommCb.devid,Devidbuf,8);
        printf("p_uartcommCb.devid = %s\r\n",p_uartcommCb.devid);
    }

    return ret;

Error_Del:
//    if(p_uartcommCb.computeOrder ) free(p_uartcommCb.computeOrder);
//   p_uartcommCb = NULL;
    DevidLoad(Devidbuf,8);
    printf("Load devid = %s\r\n",Devidbuf);

    if(Devidbuf != NULL)
    {
        memcpy(p_uartcommCb.devid,Devidbuf,8);
        printf("p_uartcommCb.devid = %s\r\n",p_uartcommCb.devid);
    }
    return ret;
}
static int _DevResultCallback(int id,int ret)
{
    printf("Result id %d ret :%d \r\n",id,ret);
    return 0;
}

int Factort_UartTest(void)
{
    size_t readlen = 0;
    int  ret = 0;
    s32 len = 0;

    printf("Factort_UartTset\r\n");
//    s32 Rcvlenofcnt = 0;

    if(0 > fcntl(p_uartcommCb.uartfd,F_SETTIMEOUT,100*mS))
    {
        return -2;
    }
    char  *pcmmd= "{SFAHTIDR}";
    while(1)
    {
        len = strlen(pcmmd);
        if(len != Uart_Send(p_uartcommCb.uartfd,pcmmd,len))
        {
            ret = -4;
//            goto Error_Del;
        }

        DJY_EventDelay(10*mS);
        len = read(p_uartcommCb.uartfd,&p_uartcommCb.devid[readlen],sizeof(p_uartcommCb.devid)-readlen);

        if((ret++ >= 5)||(len == 8))
        {
            p_uartcommCb.idlen = len;
            break;
        }

        if(len == 0)
            printf("error : read dev id error \n\r");
    }

    printf("--------ret = %d,len = %d------\n\r",ret,len);
    if(ret < 5)
    {
        Set_TestUart_flag(2);
//        Refresh_GuiWin();
    }else{
        Set_TestUart_flag(1);
    }

//    Refresh_GuiWin();

    return ret;
}


#include "shell.h"

bool_t devtestuart(char *param)
{
    char *word_addr,*next_param;
    word_addr = shell_inputs(param,&next_param);
    int key = strtoul(word_addr, (char **)NULL, 0);
    switch (key) {
        case 1:
            printf("[==============id\n\r");
            for(int i=0;i<p_uartcommCb.idlen;i++)
            {
                int aa = p_uartcommCb.devid[i];
                printf("0x%2x ", aa);
                if(i%10==0)
                    printf("\r\n");
            }
            printf("\n\r==============]\n\r");
            break;
        case 2:
            Start_DevResult(_DevResultCallback,0);
            break;
        default:            break;
    }
    return true;
}

bool_t getuartdevid(char *param)
{
    size_t i,j;
    i = strlen(p_uartcommCb.devid);
    printf("devid = ");
    for(j=0;j < i; j++)
    {
        printf("%02x ",p_uartcommCb.devid[j]);
    }
    printf("\r\n");
    return true;
}

ADD_TO_ROUTINE_SHELL(testu,devtestuart,"devtestuart :COMMAND:init_jtag+enter");
ADD_TO_ROUTINE_SHELL(getdevid,getuartdevid,"Get :Uart Devid");


