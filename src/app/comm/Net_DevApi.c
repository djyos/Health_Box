//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * Net_DevApi.c
 *
 *  Created on: 2020年1月4日
 *      Author: czz
 */
#include "stdlib.h"
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <cJSON.h>
#include "Net_DevComm.h"
#include "Uart_DevComm.h"
#include "Net_DevApi.h"
#include "../GuiWin/inc/GuiInfo.h"
#include "app_flash.h"

struct NetCommHandle{
    char deviceID[100];
    char deviceKey[100];
    u32 uid;
    struct computeOrder *p_computeOrder;
    u32 ComputeOrderNum;
};
#ifndef free
#define free(x) M_Free(x)
#endif
static struct NetCommHandle p_NetCommHandle = {0};

#define LOCA_WEB 0

#if  LOCA_WEB
#define NET_HOST "192.168.1.111:8188"
#else
#define NET_HOST "47.97.254.199/user-app"
//#define NET_HOST "121.43.107.215/user-app"
#endif

extern URL_CFG_T  LoadUrl;

//编码一个url
static int Net_UrlEncode(const char* buf,int len,char*outbuf,int outlen)
{
    unsigned char highByte, lowByte;
    int res_len = 0;

    if(outlen < (len*5+1))
        return -1;

    for (int i = 0; i < len; ++i)
    {
        highByte = (buf[i] >> 4) & 0x0f;
        lowByte = buf[i] & 0x0f;

        highByte += 0x30;
        if (highByte > 0x39)
            outbuf[res_len++] = highByte + 0x07;
        else
            outbuf[res_len++] = highByte;

        lowByte += 0x30;
        if (lowByte > 0x39)
            outbuf[res_len++] = lowByte + 0x07;
        else
            outbuf[res_len++] = lowByte;

        outbuf[res_len++] = '%';
        outbuf[res_len++] = '2';
        outbuf[res_len++] = '0';
    }
    outbuf[res_len++] = '\0';
    return res_len;

}

int  Update_DeviceKey()
{
    int tmp,ret = 1;
    NetAccess Nacb;
    cJSON * json  = NULL;
    memset(&Nacb,0,sizeof(Nacb));
    char *path = (char*)malloc(200);
    if (path == 0)
    {
        ret = -1;
        goto end_return;
    }
    sprintf(path, "/healthCabin/authorizeKey?deviceID=%s",p_NetCommHandle.deviceID);

    Nacb.out_json = malloc(1024);
    if( Nacb.out_json == NULL)
    {
        ret = -2;
        goto end_return;
    }
    Nacb.buflen = 1024;
//    Nacb.net_host = NET_HOST;
    Nacb.net_host = LoadUrl.Weburl;
    Nacb.path = path;
    Nacb.post_data = "123";
    Nacb.Type = "json";

    tmp = DevGetCommon(&Nacb);
    if(tmp < 0)
    {
        ret = -3;
        goto end_return;
    }
    json = cJSON_Parse(Nacb.out_json);
    if(json == NULL)
    {
        ret = -4;
        goto end_return;
    }
    cJSON*  jsondeviceKey = cJSON_GetObjectItem(json, "deviceKey");
    if(jsondeviceKey == NULL)
    {
        printf("123456\r\n");
        ret = 0;
        goto end_return;
    }
    if(strlen(jsondeviceKey->valuestring) >= sizeof(p_NetCommHandle.deviceKey))
    {
        ret = -5;
        goto end_return;
    }
    strcpy(p_NetCommHandle.deviceKey,jsondeviceKey->valuestring);

end_return:

    if (json) cJSON_Delete(json);
    if(path) free(path);
    if(Nacb.out_json!=NULL) free(Nacb.out_json);

    return ret;
}
int  Update_DeviceUid()
{
    int tmp,ret = 1;
    NetAccess Nacb;
    cJSON * json  = NULL;
    memset(&Nacb,0,sizeof(Nacb));
    char *path = (char*)malloc(200);
    if (path == 0)
    {
        ret = -1;
        goto end_return;
    }
    sprintf(path, "/healthCabin/publicUserAuthorize?deviceKey=%s",p_NetCommHandle.deviceKey);

    Nacb.out_json = malloc(1024);
    if( Nacb.out_json == NULL)
    {
        ret = -2;
        goto end_return;
    }
    memset(Nacb.out_json,0,1024);
    Nacb.buflen = 1024;
    //    Nacb.net_host = NET_HOST;
    Nacb.net_host = LoadUrl.Weburl;
    Nacb.path = path;
    Nacb.post_data = "123";
    Nacb.Type = "json";

    tmp = DevGetCommon(&Nacb);
    if(tmp < 0)
    {
        ret = -3;
        goto end_return;
    }
    json = cJSON_Parse(Nacb.out_json);
    if(json == NULL)
    {
        ret = -4;
        goto end_return;
    }
    cJSON*  cjsonuid = cJSON_GetObjectItem(json, "uid");
    if(cjsonuid == NULL)
    {
//        ret = -5;
        ret = 0;
        goto end_return;
    }
    p_NetCommHandle.uid = cjsonuid->valueint;
    ret = cjsonuid->valueint;
    cJSON*  cjsonuname = cJSON_GetObjectItem(json, "name");
    if(cjsonuname == NULL)
    {
        ret = -5;
        goto end_return;
    }
    SetUserName(cjsonuname->valuestring);

end_return:
    if (json) cJSON_Delete(json);
    if(path) free(path);
    if(Nacb.out_json) free(Nacb.out_json);

    return ret;
}
static int Add_NewComputeOrder(struct computeOrder*order)
{
    int ret = 0;
    int cnt = 0;
    struct computeOrder*p_order = p_NetCommHandle.p_computeOrder;
    if(p_order == NULL)
    {
        p_NetCommHandle.p_computeOrder = order;
        cnt++;
    }
    else
    {
        cnt++;
        while(p_order->next != NULL)
        {
            cnt++;
            p_order = p_order->next;
        }
        p_order->next = order;
        cnt++;
    }
    p_NetCommHandle.ComputeOrderNum = cnt;
    return ret;
}

int Del_AllComputeOrder()
{
    int ret = 0;
    struct computeOrder*p_order = p_NetCommHandle.p_computeOrder;
    struct computeOrder*p_orderbak = p_NetCommHandle.p_computeOrder;
    if (p_order==NULL)
        return true;

    while(1)
    {
        p_order = p_NetCommHandle.p_computeOrder;
        p_orderbak = p_order;
        while(p_order->next != NULL)
        {
            p_orderbak = p_order;
            p_order = p_order->next;
        }
        free(p_order);
        p_orderbak->next = NULL;
        if(p_orderbak == p_order)
        {
            p_NetCommHandle.p_computeOrder = NULL;
            break;
        }
    }
    p_NetCommHandle.ComputeOrderNum = 0;
    return ret;
}

struct computeOrder* Get_ComputeOrderOfcnt(int cnt)
{
    struct computeOrder*ret = NULL;
    if((cnt>p_NetCommHandle.ComputeOrderNum)||(cnt<=0))
        return NULL;
    ret = p_NetCommHandle.p_computeOrder;

    while(cnt--)
    {
        if(cnt > 0)
            ret = ret->next;
    }
    return ret;
}
int Get_ComputeOrderRcvlen()
{
    int ret = 0;
    struct computeOrder*p_order;
    p_order = p_NetCommHandle.p_computeOrder;
    while(p_order!=NULL)
    {
        ret += p_order->rcvlen;
        p_order = p_order->next;
    }
    return ret;
}
int Get_ComputeOrderNum()
{
    return p_NetCommHandle.ComputeOrderNum;
}

int jsonstrtojsonobj(char *str)
{
    int cnt = 0;
    char *strto = str;
    while(*str!='\0')
    {
        switch (*str) {
        case '"': if(cnt!=0)*strto++ = *str;  break;
        case '{': cnt++;  *strto++ = *str; break;
        case '}': cnt--; *strto++ = *str; break;
        case '\\':  *str = ' '; break;
        default:
            *strto++ = *str;
            break;
        }
        str++;
    }
    *strto++ = *str;
    return 0;
}

int Update_DeviceComputeOrder()
{
    int tmp,ret = 0;
    NetAccess Nacb;
    cJSON * json  = NULL;
    memset(&Nacb,0,sizeof(Nacb));

    char *path = (char*)malloc(200);
    if (path == 0)
    {
        ret = -1;
        goto end_return;
    }
    sprintf(path, "/healthCabin/getComputeOrder?deviceKey=%s",p_NetCommHandle.deviceKey);

    Nacb.out_json = malloc(1024);
    if( Nacb.out_json == NULL)
    {
        ret = -2;
        goto end_return;
    }
    Nacb.buflen = 1024;
//    Nacb.net_host = NET_HOST;
    Nacb.net_host = LoadUrl.Weburl;
    Nacb.path = path;
    Nacb.post_data = NULL;
    Nacb.Type = "json";

    tmp = DevGetCommon(&Nacb);
    if(tmp < 0)
    {
        ret = -3;
        goto end_return;
    }
    jsonstrtojsonobj(Nacb.out_json);
//    printf("out_json %s \r\n[%s,line %d] \r\n",Nacb.out_json,__FILE__,__LINE__);

    json = cJSON_Parse(Nacb.out_json);
    if(json == NULL)
    {
        ret = -4;
        goto end_return;
    }
    cJSON*  cjson = cJSON_GetObjectItem(json, "computeOrder");
    if(cjson == NULL)
    {
        ret = -5;
        goto end_return;
    }
    Del_AllComputeOrder();
    int size2 = cJSON_GetArraySize(cjson);
//    printf("size %d [%s,line %d] \r\n",size2,__FILE__,__LINE__);
    if(size2!=0)
    for(int j=0;j<size2;j++)
    {
        cJSON*   JsoncomputeOrder = cJSON_GetArrayItem(cjson,j);
        if(JsoncomputeOrder == NULL)
            continue;
        cJSON*  jsonrcvlen = cJSON_GetObjectItem(JsoncomputeOrder, "rcvlen");
        cJSON*  jsondata = cJSON_GetObjectItem(JsoncomputeOrder, "data");
        if(jsonrcvlen==NULL||jsondata == NULL)
        {
            printf("computeOrder 格式错误\r\n");
            continue;
        }
        size_t size_len = sizeof(struct computeOrder)+strlen(jsondata->valuestring)+1;
        struct computeOrder* p_NewcomputeOrder = malloc(size_len);
        p_NewcomputeOrder->next = NULL;
        p_NewcomputeOrder->rcvlen = jsonrcvlen->valueint;
        p_NewcomputeOrder->data = (char*)(p_NewcomputeOrder+1);
        strcpy(p_NewcomputeOrder->data,jsondata->valuestring);
        Add_NewComputeOrder(p_NewcomputeOrder);
    }

end_return:
    if (json) cJSON_Delete(json);
    if(path) free(path);
    if(Nacb.out_json) free(Nacb.out_json);
//    printf("fun %s ret %d \r\n",__func__,ret);
    return ret;
}

int Get_UserDeviceKey(char *buf,int buflen,int update)
{
    int tmp,ret = 1;
    if((update) ||(p_NetCommHandle.deviceKey[0] == 0))
    {
        tmp = Update_DeviceKey();
        if(tmp < 0)
        {
            printf("error: Update_DeviceKey ret %d \r\n",tmp);
            ret = -1;
            goto End_return;
        }else if(tmp == 0)
        {
            printf("invalid: Update_DeviceKey ret %d \r\n",tmp);
            ret = 0;
            goto End_return;
        }
    }
    if(buflen <= (int)strlen(p_NetCommHandle.deviceKey))
    {
        ret = -2;
        goto End_return;
    }
    strcpy(buf,p_NetCommHandle.deviceKey);


End_return:
    return ret;
}


int  Get_Deviceuid(int update)
{
    int tmp,ret = 0;

    if((update)||(p_NetCommHandle.uid <= 0))
    {
        tmp = Update_DeviceUid();
        if(tmp < 0)
        {
            ret = -1;
            goto End_return;
        }
    }
    ret = p_NetCommHandle.uid;

    return ret;
End_return:
    return ret;
}
//
//char * Get_DeviceComputeOrder(int update)
//{
//    char* ret = NULL;
//
//    if((p_NetCommHandle.computeOrder == NULL)||(update))
//    {
//      Update_DeviceComputeOrder();
//    }
//    ret = p_NetCommHandle.computeOrder;
//    return ret;
//}

static char dec2hex(short int c)
{
    if (0 <= c && c <= 9)
    {
        return c + '0';
    }
    else if (10 <= c && c <= 15)
    {
        return c + 'A' - 10;
    }
    else
    {
        return -1;
    }
}
#if 0
//编码一个url
static int urlencode(char *url ,int urllen,char *outstr,int outbuflen)
{
    int i = 0;
    int len = strlen(url);
    if(urllen!=0)
        len = urllen;
    int res_len = 0;
    char *res = outstr;
    for (i = 0; i < len; ++i)
    {
        if(res_len +4 > outbuflen)
        {
            return -1;
        }
        char c = url[i];
        if (    ('0' <= c && c <= '9') ||
                ('a' <= c && c <= 'z') ||
                ('A' <= c && c <= 'Z') ||
                c == '/' || c == '.'|| c == '#' || c == '~' || c == '&'
                || c == ':' || c == ';' || c == '=' || c == '?' || c == '@' ||
                c == '$' || c == '&' || c == '*'||c == '(' || c == ')'||c == ','
                )
        {
            res[res_len++] = c;
        }
        else
        {
            int j = (short int)c;
            if (j < 0)
                j += 256;
            int i1, i0;
            i1 = j / 16;
            i0 = j - i1 * 16;
            res[res_len++] = '%';
            res[res_len++] = dec2hex(i1);
            res[res_len++] = dec2hex(i0);
        }
    }
    res[res_len++] = '\0';

    return res_len;
}
//#else
static int urlencode(char *url ,int urllen,char *outstr,int outbuflen)
{
    int i = 0;
    int len = strlen(url);
    if(urllen!=0)
        len = urllen;
    int res_len = 0;
    char *res = outstr;
    int i1, i0;
    short int j;
    for (i = 0; i < len; ++i)
    {
        if(res_len +7 > outbuflen)
        {
            return -1;
        }
        if(url[i]<127){
            if(url[i]==0x20){//空格变+符号
                res[res_len++]='+';
                continue;
            }
            if((url[i]>='0' && url[i]<='9')
                    || (url[i]>='a' && url[i]<='z')
                    || (url[i]>='A' && url[i]<='Z')
                    || url[i]=='.' || url[i]=='-' || url[i]=='_' )
            {
                res[res_len++]=url[i];
            }else{
                j = (short int)url[i];
                if (j < 0)
                    j += 256;
                i1 = j / 16;
                i0 = j - i1 * 16;
                res[res_len++] = '%';
                res[res_len++] = dec2hex(i1);
                res[res_len++] = dec2hex(i0);
            }
        }
        else
        {
            j = (short int)url[i];
            if (j < 0)
                j += 256;
            i1 = j / 16;
            i0 = j - i1 * 16;
            res[res_len++] = '%';
            res[res_len++] = dec2hex(i1);
            res[res_len++] = dec2hex(i0);

            i++;
            j = (short int)url[i];
            if (j < 0)
                j += 256;
            i1 = j / 16;
            i0 = j - i1 * 16;
            res[res_len++] = '%';
            res[res_len++] = dec2hex(i1);
            res[res_len++] = dec2hex(i0);
        }
    }
    res[res_len++] = '\0';

    return res_len;
}

#endif

#if 0
static char *  pack_Result(char* sendbuf)
{
    char tmpbuf[20];
    int len = 0;
    int encodelen = 0;
    int tmp = 0;
    char *chstart = NULL;
    char *chend = NULL;
    char *urlsendbuf = NULL;
    char * param = "deviceKey=%s&uid=%d&result=";
    size_t urllen = (strlen(sendbuf)+strlen(param))*3;
    printf("pack_Result urllen = %d\r\n",urllen);
    urlsendbuf = (char*)malloc(urllen);
    if (urlsendbuf == 0)
    {
        printf("urlsendbuf psram_malloc error\r\n");
       goto error_return;
    }
    char *pstart = "OK\r\nBEGIN";
    memset(urlsendbuf,0,urllen);
    sprintf(&urlsendbuf[len],param,p_NetCommHandle.deviceKey,p_NetCommHandle.uid);
    chend = sendbuf;
    for(int i=1;chend!=NULL;i++)
    {
        len = strlen(urlsendbuf);
        chstart = strstr(chend,pstart);
        if(chstart == NULL)
            break;
        chend = strstr(chstart+strlen(pstart),pstart);
        if(chend ==NULL)
            encodelen = strlen(chstart);
        else
            encodelen = (int)(chend-chstart);

        memset(tmpbuf,0,sizeof(tmpbuf));
        sprintf(tmpbuf,"begin_%d,",i);
        tmp = urlencode(tmpbuf,0,&urlsendbuf[len],urllen-len);
        if(tmp < 0)
        {
            goto error_return;
        }

        len += (tmp-1);
        tmp = urlencode(chstart,encodelen,&urlsendbuf[len],urllen-len);
        if(tmp < 0)
        {
            goto error_return;
        }
        memset(tmpbuf,0,sizeof(tmpbuf));
        sprintf(tmpbuf,",end_%d;",i);
        len += (tmp-1);
        tmp = urlencode(tmpbuf,0,&urlsendbuf[len],urllen-len);
        if(tmp < 0)
        {
            goto error_return;
        }
        len +=(tmp-1);
    }
    urlsendbuf[len-3] = '\0';
    return urlsendbuf;

error_return:
    if(urlsendbuf) free(urlsendbuf);
    return NULL;
}
#else
static char *  pack_Result(char* sendbuf)
{
    char tmpbuf[20];
    int len = 0;
    int encodelen = 0;
    int tmp = 0;
    char *chstart = NULL;
    char *chend = NULL;
    char *urlsendbuf = NULL;
    char * param = "deviceKey=%s&uid=%d&result=";
    size_t urllen = (strlen(sendbuf)+strlen(param))+100;
    printf("pack_Result urllen = %d\r\n",urllen);
    urlsendbuf = (char*)malloc(urllen);
    if (urlsendbuf == 0)
    {
        printf("urlsendbuf psram_malloc error\r\n");
       goto error_return;
    }
    char *pstart = "OK\r\nBEGIN";
    memset(urlsendbuf,0,urllen);
    sprintf(&urlsendbuf[len],param,p_NetCommHandle.deviceKey,p_NetCommHandle.uid);
    chend = sendbuf;
    for(int i=1;chend!=NULL;i++)
    {
        len = strlen(urlsendbuf);
        chstart = strstr(chend,pstart);
        if(chstart == NULL)
            break;
        chend = strstr(chstart+strlen(pstart),pstart);
        if(chend ==NULL)
            encodelen = strlen(chstart);
        else
            encodelen = (int)(chend-chstart);

        memset(tmpbuf,0,sizeof(tmpbuf));
        sprintf(tmpbuf,"begin_%d,",i);
//        tmp = urlencode(tmpbuf,0,&urlsendbuf[len],urllen-len);
        tmp = strlen(tmpbuf);
        memcpy(&urlsendbuf[len],tmpbuf,tmp);
        if(tmp < 0)
        {
            goto error_return;
        }

        len += (tmp);

        memcpy(&urlsendbuf[len],chstart,encodelen);
//        tmp = urlencode(chstart,encodelen,&urlsendbuf[len],urllen-len);
        tmp = encodelen;
        if(tmp < 0)
        {
            goto error_return;
        }
        len += (tmp);
        sprintf(tmpbuf,",end_%d;",i);
        tmp = strlen(tmpbuf);
        memcpy(&urlsendbuf[len],tmpbuf,tmp);
//        tmp = urlencode(tmpbuf,0,&urlsendbuf[len],urllen-len);
        if(tmp < 0)
        {
            goto error_return;
        }
        len +=(tmp);
    }
    urlsendbuf[len-1] = '\0';
    return urlsendbuf;

error_return:
    if(urlsendbuf) free(urlsendbuf);
    return NULL;



}


#endif
int filter_SendBuf(char * sendbuf)
{
    int ret = 0;
    char *begen,*end;
    char *strnow = sendbuf;
    char *sendbufto = sendbuf;

    while(1)
    {
        begen = strstr(strnow,"BEGIN{");
        end = strstr(strnow,"}END\r\n");
        if(end!=NULL)
            end+=strlen("}END\r\n");
        if((begen==NULL)||(end==NULL))
            break;
        strcpy(sendbufto,"OK\r\n");
        sendbufto+=strlen("OK\r\n");
        size_t len = end-begen;
        memcpy(sendbufto,begen,len);
        sendbufto+=len;
        strnow = end;
    }
    *sendbufto = '\0';
    return ret;
}

#define SUBMIT_DEVDRESULT   3
int Submit_DevdResult(char *sendbuf)
{
    int tmp,ret = 0;
    NetAccess Nacb;
    cJSON * json  = NULL;
    char *path = NULL;
    char *urlsendbuf = NULL;
    cJSON*  cjson = NULL;

    char *p = NULL;
    size_t bufLen = 0;
    char Net_host[32];
    memset(&Nacb,0,sizeof(Nacb));

#if (SUBMIT_DEVDRESULT==1)
    const char multi_part_req_fmt[] =
            "--%s\r\n"
            "Content-Disposition: form-data; name=\"deviceKey\"\r\n"
            "\r\n"
            "%s"
            "\r\n--%s\r\n"
            "Content-Disposition: form-data; name=\"uid\"\r\n"
            "\r\n"
            "%d"
            "\r\n--%s\r\n"
            "Content-Disposition: form-data; name=\"result\"\r\n"
            "\r\n"
            "%s"
            "\r\n--%s--\r\n"
            "\r\n";
    Nacb.boundary = "-----000000123456789087654321";
    size_t datelen = strlen(sendbuf)+ strlen(Nacb.boundary)*4 + strlen(multi_part_req_fmt) + 10;
    Nacb.post_data = (char*)malloc(datelen);
    if (Nacb.post_data == 0)
    {
        ret = -10;
        goto end_return;
    }
    Nacb.out_json = malloc(1024);
    if( Nacb.out_json == NULL)
    {
        ret = -2;
        goto end_return;
    }
    Nacb.buflen = 1024;
    Nacb.net_host = NET_HOST;
    Nacb.path = "/healthCabin/sendResult";

    snprintf(Nacb.post_data, datelen, multi_part_req_fmt,  Nacb.boundary,\
            p_NetCommHandle.deviceKey,  Nacb.boundary, p_NetCommHandle.uid, \
            Nacb.boundary, sendbuf,  Nacb.boundary);
#elif (SUBMIT_DEVDRESULT==2)
    size_t urllen = strlen(sendbuf)*3; 
    urlsendbuf = (char*)malloc(urllen);
    if (urlsendbuf == 0)
    {
        ret = -10;
        goto end_return;
    }
    memset(urlsendbuf,0,urllen);
    if(false == urlencode(sendbuf,urlsendbuf,urllen))
    {
        ret = -11;
        goto end_return;
    }

    char * pathtmp =  "/healthCabin/sendResult?deviceKey=%s&uid=%d&result=";
    size_t pathlen = strlen(urlsendbuf) +  strlen(pathtmp) + strlen(p_NetCommHandle.deviceKey)+10;
    free(urlsendbuf);
    urlsendbuf = NULL;
    path = (char*)malloc(pathlen);
    if (path == 0)
    {
        ret = -1;
        goto end_return;
    }
    sprintf(path, pathtmp,p_NetCommHandle.deviceKey,p_NetCommHandle.uid);
    int str_len = strlen(path);
    if(false == urlencode(sendbuf,&path[str_len],urllen))
    {
        ret = -11;
        goto end_return;
    }

    Nacb.out_json = malloc(1024);
    if( Nacb.out_json == NULL)
    {
        ret = -2;
        goto end_return;
    }
    Nacb.buflen = 1024;
    Nacb.net_host = NET_HOST;
    Nacb.path = path;
    Nacb.boundary = NULL;
    Nacb.post_data = "123";
#elif (SUBMIT_DEVDRESULT==3)

    printf("sendbuf len= %d\r\n",strlen(sendbuf));
#if 0
    if(0 < filter_SendBuf(sendbuf))
    {
        ret = -11;
        goto end_return;
    }
#endif
//    printf("%s",sendbuf);
    Nacb.post_data = pack_Result(sendbuf);

    if (Nacb.post_data == 0)
    {
        ret = -10;
        goto end_return;
    }

    Nacb.out_json = malloc(1024);
    if( Nacb.out_json == NULL)
    {
        ret = -2;
        goto end_return;
    }
    Nacb.buflen = 1024;
//    Nacb.net_host = NET_HOST;
    Nacb.path = "/healthCabin/sendResult";
    Nacb.Type = "x-www-form-urlencoded";

#endif

    p = strchr(LoadUrl.Weburl, ':');
    if(p == NULL)
    {
        p = strchr(LoadUrl.Weburl, '/');
        if(p != NULL)
        {
            bufLen = p - LoadUrl.Weburl;//strlen(LoadUrl.Weburl)-strlen(p1);
            if(bufLen+1 > sizeof(Net_host))
            {
                printf("error : file %s line %d \r\n",__FILE__,__LINE__);
            }
            memcpy(Net_host, (char*)LoadUrl.Weburl, bufLen);
            Net_host[bufLen] = '\0';
            Nacb.path = "/user-app/healthCabin/sendResult";
        }
    }else{
        bufLen = p - LoadUrl.Weburl;//strlen(LoadUrl.Weburl)-strlen(p1);
        if(bufLen+1 > sizeof(Net_host))
        {
            printf("error : file %s line %d \r\n",__FILE__,__LINE__);
        }
        memcpy(Net_host, (char*)LoadUrl.Weburl, bufLen);
        Net_host[bufLen] = '\0';
    }
    Nacb.net_host = Net_host;

    printf("Nacb.path = %s  Nacb.net_host = %s\r\n",Nacb.path,Nacb.net_host);
    int DevGetCommonXXX(NetAccess *Nacb);
    tmp = DevGetCommonXXX(&Nacb);

#if LOCA_WEB
    Nacb.net_host = "192.168.1.111";
    int DevGetCommonXXX(NetAccess *Nacb);
    tmp = DevGetCommonXXX(&Nacb);
//#else
    Nacb.path = "/user-app/healthCabin/sendResult";
    Nacb.net_host = "47.97.254.199";
//    Nacb.net_host = "121.43.107.215";
    int DevGetCommonXXX(NetAccess *Nacb);
    tmp = DevGetCommonXXX(&Nacb);
#endif
    if(tmp < 0)
    {
        ret = -3;
        goto end_return;
    }
    json = cJSON_Parse(Nacb.out_json);
    if(json == NULL)
    {
        ret = -4;
        goto end_return;
    }
    cjson = cJSON_GetObjectItem(json, "result");
    if(cjson == NULL)
    {
        ret = -5;
        goto end_return;
    }
    if(0 != strcmp("SUCCESS",cjson->valuestring) )
    {
        ret = -6;
        if(0 == strcmp("FAIL",cjson->valuestring))
        {
            SetUpdateError(Error_FAIL);
        }
        else if(0 == strcmp("PARAM_ERROR",cjson->valuestring))
        {
            SetUpdateError(Error_PARAM_ERROR);
        }
        else if(0 == strcmp("ILLEGAL_ACCESS",cjson->valuestring))
        {
            SetUpdateError(Error_ILLEGAL_ACCESS);
        }
        else if(0 == strcmp("EXCEPTION",cjson->valuestring))
        {
            SetUpdateError(Error_EXCEPTION);
        }
        printf("\r\nerror: body \n\r%s \r\n",Nacb.out_json);
        goto end_return;
    }

end_return:
    if(urlsendbuf) free(urlsendbuf);
    if (json) cJSON_Delete(json);
    if(path) free(path);
    if(Nacb.post_data) free(Nacb.post_data);
    if(Nacb.out_json) free(Nacb.out_json);
    return ret;
}


int Net_DevApiInit()
{
    char tmpbuf[20];
    int tmp = 0;
    int ret = 0;

    memset(&p_NetCommHandle,0,sizeof(p_NetCommHandle));
    tmp = Uart_GetDevid(tmpbuf ,sizeof(tmpbuf));
    if(tmp < 0)
    {
        ret = -2;
        goto end_free;
    }
    int len = tmp;

    tmp = Net_UrlEncode(tmpbuf ,len,p_NetCommHandle.deviceID,sizeof(p_NetCommHandle.deviceID));
    if(tmp < 0)
    {
        ret = -3;
        goto end_free;
    }

end_free:
    return ret;
}

#if 0
void Test_Uart(void)
{
    Del_AllComputeOrder();

    size_t size_len = sizeof(struct computeOrder)+strlen("{SKAFCKAHAFHKAHAJLKAFBBTCLBAFUFCQLCAFUFCQAFASAFVCAFTDAFSEDR}")+1;
    struct computeOrder* p_NewcomputeOrder = malloc(size_len);
    p_NewcomputeOrder->next = NULL;
    p_NewcomputeOrder->rcvlen = 196620;
    p_NewcomputeOrder->data = (char*)(p_NewcomputeOrder+1);
    strcpy(p_NewcomputeOrder->data,"{SKAFCKAHAFHKAHAJLKAFBBTCLBAFUFCQLCAFUFCQAFASAFVCAFTDAFSEDR}");
    Add_NewComputeOrder(p_NewcomputeOrder);
}
#endif

#include "shell.h"

bool_t devtest(char *param)
{
    int id;
    int tmp;
    char *word_addr,*next_param;
    word_addr = shell_inputs(param,&next_param);
    int key = strtoul(word_addr, (char **)NULL, 0);
    switch (key) {
        case 1:
            tmp = Update_DeviceKey();
            if(tmp<0)
                printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
            char buf[100];
            tmp = Get_UserDeviceKey(buf,sizeof(buf),0);
            if(tmp<0)
                printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
            else
                printf("%s \n\r",buf);
            break;
        case 2:
            tmp = Update_DeviceComputeOrder();
            if(tmp<0)
                printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
//            char * str = Get_DeviceComputeOrder(0);
//            if(str != NULL)
//                printf("get str : %s \n\r",str);
            break;
        case 3:
            id  = Update_DeviceUid();
            printf("uid = %d  \n\r",id);
            id = Get_Deviceuid(0);
            printf("uid = %d  \n\r",id);
            break;
        case 4:
//            str =  Get_DeviceComputeOrder(0);
//            printf("%s \r\n",str);
            break;
        case 5:
            Submit_DevdResult("123");
            break;
        case 6:
            printf("deviceID:%s \n\r",p_NetCommHandle.deviceID);
            printf("deviceKey:%s \n\r",p_NetCommHandle.deviceKey);
            printf("uid:%d \n\r",p_NetCommHandle.uid);
//            printf("computeOrder:%s \n\r",p_NetCommHandle.computeOrder);



            break;

        default:            break;
    }
    return true;
}

ADD_TO_ROUTINE_SHELL(test,devtest,"test :COMMAND:init_jtag+enter");



