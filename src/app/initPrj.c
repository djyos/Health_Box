/****************************************************
 *  Automatically-generated file. Do not edit!	*
 ****************************************************/

#include "project_config.h"
#include "djyos.h"
#include "stdint.h"
#include "stddef.h"
#include "cpu_peri.h"
#include <Iboot_info.h>

#include <djyfs/filesystems.h>

extern ptu32_t djy_main(void);
//如果这个CN_MANUFACTURER_NAME名字要改，那djysrc里的Iboot_info.c中相应的名字也要改
const char CN_MANUFACTURER_NAME[] = PRODUCT_MANUFACTURER_NAME;


const struct ProductInfo Djy_Product_Info __attribute__ ((section(".DjyProductInfo"))) =
{
    .VersionNumber[0] = PRODUCT_VERSION_LARGE,
    .VersionNumber[1] = PRODUCT_VERSION_MEDIUM,
    .VersionNumber[2] = PRODUCT_VERSION_SMALL,
#if(CN_PTR_BITS < 64)
    .ManufacturerNameAddr      = (u32)(&CN_MANUFACTURER_NAME),
    .ManufacturerNamereserved32    = 0xffffffff,
#else
    .ManufacturerNameAddr      = (u64)(&CN_MANUFACTURER_NAME),
#endif
    .ProductClassify = PRODUCT_PRODUCT_CLASSIFY,
    .ProductType = PRODUCT_PRODUCT_MODEL,

    .TypeCode = PRODUCT_PRODUCT_MODEL_CODE,

    .ProductionTime = {0xff,0xff,0xff,0xff},
    .ProductionNumber = {0xff,0xff,0xff,0xff,0xff},
    .reserved8 = 0,
    .BoardType = PRODUCT_BOARD_TYPE,
    .CPU_Type = PRODUCT_CPU_TYPE,
    .Reserved ={
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff
            },
};


ptu32_t __djy_main(void)
{
    //cpu onchip iwdt
    extern void ModuleInstall_BrdWdt(void);
    ModuleInstall_BrdWdt();
    //end cpu onchip iwdt

    //cpu onchip spi
    extern int ModuleInstall_SPI(void);
    ModuleInstall_SPI();
    //end cpu onchip spi

    //LCD driver ST7789V
    extern ptu32_t ModuleInstall_ST7789V(const char *DisplayName,const char* HeapName);
    ModuleInstall_ST7789V(CFG_ST7789V_DISPLAY_NAME,CFG_ST7789V_HEAP_NAME);
    //end LCD driver ST7789V

    //graphical decorate development
    extern void ModuleInstall_Gdd_AND_Desktop(void);
    ModuleInstall_Gdd_AND_Desktop();
    //end graphical decorate development

    //touch
    extern bool_t ModuleInstall_Touch(void);
    ModuleInstall_Touch();    //初始化人机界面输入模块
    //end touch

    //touchscreen FT6236
    extern bool_t ModuleInstall_FT6236(void);
    ModuleInstall_FT6236( );
    #if(CFG_MODULE_ENABLE_GRAPHICAL_DECORATE_DEVELOPMENT == true)
    extern bool_t GDD_AddInputDev(const char *InputDevName);
    GDD_AddInputDev(CFG_FT6236_TOUCH_NAME);
    #endif
    //end touchscreen FT6236

    djy_main();

	return 0;
}

void Sys_ModuleInit(void)
{
	uint16_t evtt_main;

    //shell
    extern void Stdio_KnlInOutInit(char * StdioIn, char *StdioOut);
    Stdio_KnlInOutInit(CFG_STDIO_IN_NAME,CFG_STDIO_OUT_NAME);
    extern s32 ModuleInstall_Shell(ptu32_t para);
    ModuleInstall_Shell(0);
    //end shell

    //----------------------------early----------------------------//
    //black box
    extern void ModuleInstall_BlackBox(void);
    ModuleInstall_BlackBox( );
    //end black box

    //cpu onchip lowpower control
    void ModuleInstall_LowPower(void);
    ModuleInstall_LowPower();
    //end cpu onchip lowpower control

    //cpu onchip random
    extern void ModuleInstall_random(void);
    ModuleInstall_random();
    //end cpu onchip random

    //device file system
    extern s32 ModuleInstall_dev(void);
    ModuleInstall_dev();    // 安装设备文件系统；
    //end device file system

    //message queue
    extern bool_t ModuleInstall_MsgQ(void);
    ModuleInstall_MsgQ ( );
    //end message queue

    //multiplex
    extern bool_t ModuleInstall_Multiplex(void);
    ModuleInstall_Multiplex ();
    //end multiplex

    //watch dog
    extern bool_t ModuleInstall_Wdt(void);
    ModuleInstall_Wdt();
    //end watch dog

    //xip iboot file system
    extern s32 ModuleInstall_XIP_IBOOT_FS(u32 opt, void *data);
    ModuleInstall_XIP_IBOOT_FS(0,NULL);
    //end xip iboot file system

    //djybus
    extern bool_t ModuleInstall_DjyBus(void);
    ModuleInstall_DjyBus ( );
    //end djybus

    //spi bus
    extern bool_t ModuleInstall_SPIBus(void);
    ModuleInstall_SPIBus();
    //end spi bus

    //iicbus
    extern bool_t ModuleInstall_IICBus(void);
    ModuleInstall_IICBus ( );
    //end iicbus

    //ioiicconfig
    bool_t ModuleInstall_init_ioiic(const char * busname);
    ModuleInstall_init_ioiic(CFG_IO_IIC_BUS_NAME);
    //end ioiicconfig

    //easy file system
    extern s32 ModuleInstall_EFS(const char *target, u32 opt, u32 config);
    ModuleInstall_EFS(CFG_EFS_MOUNT_POINT, CFG_EFS_INSTALL_OPTION, 0);
    //end easy file system

    //----------------------------medium----------------------------//
    //cpu onchip audio
    extern s32 ModuleInstall_Audio(void);
    ModuleInstall_Audio( );
    //end cpu onchip audio

    //cpu onchip flash
    extern int ModuleInstall_Flash(void);
    ModuleInstall_Flash();
    //end cpu onchip flash

    //emflash insatall xip
    extern bool_t ModuleInstall_FlashInstallXIP(const char *TargetFs,s32 bstart, s32 bend, u32 doformat);
    ModuleInstall_FlashInstallXIP(CFG_EFLASH_XIPFSMOUNT_NAME,CFG_EFLASH_XIP_PART_START,
    CFG_EFLASH_XIP_PART_END, CFG_EFLASH_XIP_PART_FORMAT);
    //end emflash insatall xip

    //embflash install efs
    extern bool_t ModuleInstall_EmbFlashInstallEfs(const char *TargetFs, s32 bstart, s32 bend, u32 doformat);
    ModuleInstall_EmbFlashInstallEfs(CFG_EMBFLASH_EFS_MOUNT_NAME, CFG_EMBFLASH_EFS_PART_START,
    CFG_EMBFLASH_EFS_PART_END, CFG_EMBFLASH_EFS_PART_FORMAT);
    //end embflash install efs

    //font
    extern bool_t ModuleInstall_Font(void);
    ModuleInstall_Font ( );
    //end font

    //kernel
    #if(CFG_OS_TINY == flase)
    extern s32 kernel_command(void);
    kernel_command();
    #endif
    //end kernel

    //Nls Charset
    extern ptu32_t ModuleInstall_Charset(ptu32_t para);
    ModuleInstall_Charset(0);
    extern void ModuleInstall_CharsetNls(const char * DefaultCharset);
    ModuleInstall_CharsetNls("C");
    //end Nls Charset

    //gb2312 charset
    extern bool_t ModuleInstall_CharsetGb2312(void);
    ModuleInstall_CharsetGb2312 ( );
    //end gb2312 charset

    //gb2312 dot
    extern void ModuleInstall_FontGB2312(void);
    ModuleInstall_FontGB2312();
    //end gb2312 dot

    //fat file system
    extern s32 ModuleInstall_FAT(const char *dir, u32 opt, void *data);
    ModuleInstall_FAT(CFG_FAT_MOUNT_POINT, CFG_FAT_MS_INSTALLUSE, CFG_FAT_MEDIA_KIND);
    //end fat file system

    //utf8 charset
    extern bool_t ModuleInstall_CharsetUtf8(void);
    ModuleInstall_CharsetUtf8 ( );
    //end utf8 charset

    //cpu onchip adc
    extern void ModuleInstall_ADC(void);
    ModuleInstall_ADC( );
    //end cpu onchip adc

    //graphical kernel
    extern bool_t ModuleInstall_GK(void);
    ModuleInstall_GK();
    //end graphical kernel

    //cpu onchip qspi
    #if CFG_INSTALL_QSPI_WHEN_SYSINIT == true
    extern int ModuleInstall_QSPI_PSRAM(void);
    ModuleInstall_QSPI_PSRAM();
    #endif
    //end cpu onchip qspi

    //cpu onchip uart
    extern ptu32_t ModuleInstall_UART(ptu32_t SerialNo);
    #if CFG_UART1_ENABLE ==1
    ModuleInstall_UART(CN_UART1);
    #endif
    #if CFG_UART2_ENABLE ==1
    ModuleInstall_UART(CN_UART2);
    #endif
    //end cpu onchip uart

    //tcpip
    extern bool_t ModuleInstall_TcpIp(void);
    ModuleInstall_TcpIp( );
    //end tcpip

    //cpu onchip MAC
    extern bool_t ModuleInstall_Wifi(void);
    ModuleInstall_Wifi( );
    //end cpu onchip MAC

    //loader
    #if !defined (CFG_RUNMODE_BAREAPP)
    extern bool_t ModuleInstall_UpdateIboot(void);
    ModuleInstall_UpdateIboot( );
    #endif
    //end loader

    //human machine interface
    extern bool_t ModuleInstall_HmiIn(void);
    ModuleInstall_HmiIn();      //初始化人机界面输入模块
    //end human machine interface

    //----------------------------later----------------------------//
    //stdio
    #if(CFG_STDIO_STDIOFILE == true)
    extern s32 ModuleInstall_STDIO(const char *in,const char *out, const char *err);
    ModuleInstall_STDIO(CFG_STDIO_IN_NAME,CFG_STDIO_OUT_NAME,CFG_STDIO_ERR_NAME);
    #endif
    //end stdio

	evtt_main = DJY_EvttRegist(EN_CORRELATIVE,CN_PRIO_RRS,0,0,
		__djy_main,NULL,CFG_MAINSTACK_LIMIT, "main function");
	//事件的两个参数暂设为0,如果用shell启动,可用来采集shell命令行参数
	DJY_EventPop(evtt_main,NULL,0,0,0,0);

    //heap
    #if ((CFG_DYNAMIC_MEM == true))
    extern bool_t Heap_DynamicModuleInit(void);
    Heap_DynamicModuleInit ( );
    #endif
    //end heap



	return ;
}
