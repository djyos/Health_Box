#ifndef __LOOP_BYTES_H__
#define __LOOP_BYTES_H__
#ifdef   __cplusplus
extern "C" {
#endif

#include "stdlib.h"


//#include "LoopObj.h"

//extern struct StLoopObjMgr;

#define __DJYOS__

#define LOOP_BYTES_MAX (120*1024)

#ifdef __DJYOS__

#include <osarch.h>

#include "mp3Common.h"

#define LOCK_TYPE mutex_t
#define LOCK_INIT(x)  x = Lock_MutexCreate(NULL);
#define LOCK(x) Lock_MutexPend(x,CN_TIMEOUT_FOREVER)
#define UNLOCK(x) Lock_MutexPost(x)
#define LOCK_DEINIT(x) Lock_MutexDelete(x)

typedef struct SemaphoreLCB* sem_pt;
#define SEM_TYPE  sem_pt
#define SEM_INIT(s,v) s = Lock_SempCreate((1),(v),CN_BLOCK_FIFO,0)
#define SEM_PEND(s,t) Lock_SempPend((s),(t))
#define SEM_POST(s) Lock_SempPost(s)
#define SEM_DEINIT(s) Lock_SempDelete(s)
#else
#define LOCK_TYPE int
#define LOCK_INIT(x)
#define LOCK(x)
#define UNLOCK(x)
#define LOCK_DEINIT(x)

typedef struct SemaphoreLCB* sem_pt;
#define SEM_TYPE int
#define SEM_INIT(s,v)
#define SEM_PEND(s,t) 1
#define SEM_POST(s)
#define SEM_DEINIT(s)
#endif



typedef enum {
    OPT_LOOPBYTES_GET_MAX = 0,
    OPT_LOOPBYTES_SET_THRESHOLDADD,
    OPT_LOOPBYTES_SET_THRESHOLDGET,
    OPT_LOOPBYTES_SET_PUSH_MODE,
    OPT_LOOPBYTES_SET_PULL_MODE,
}ELOOPBYTES_OPT;

typedef struct StLoopBytesMgr {
    volatile int flag_init;
    volatile int bytes_max;
    volatile int bytes_total;
    volatile int threshold_add;
    volatile int threshold_get;
    // mode==0 ==> return until buf more than len or timeout
    // mode==1 ==> return until buf more than one or timeout
    volatile int push_mode;
    volatile int pull_mode;
    LOCK_TYPE    lock;
    SEM_TYPE     sem_add;
    SEM_TYPE     sem_get;

    struct StLoopObjMgr *pLoopObjMgr;
}StLoopBytesMgr;

struct StLoopBytesMgr *LoopBytesMgrInit(int max_bytes, int threshold_add, int threshold_get);
int LoopBytesMgrCtrl(struct StLoopBytesMgr *pMgr, ELOOPBYTES_OPT opt, void *popt, int optlen);
int LoopBytesMgrPushPend(struct StLoopBytesMgr *pMgr, unsigned char *buf, int len, unsigned int timeout, int *err);
int LoopBytesMgrPullPend(struct StLoopBytesMgr *pMgr, unsigned char *buf, int len, unsigned int timeout, int *err);
int LoopBytesMgrDeInit(struct StLoopBytesMgr *pMgr);
int LoopBytesMgrReset(struct StLoopBytesMgr *pMgr);

int LoopBytesMgrTotals(struct StLoopBytesMgr *pMgr);
int LoopBytesMgrMax(struct StLoopBytesMgr *pMgr);


//int LoopBytesMgrTest();

#ifdef   __cplusplus
}
#endif

#endif

