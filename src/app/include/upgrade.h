/*
 * airkiss.h
 *
 *  Created on: 2015-1-26
 *      Author: peterfan
 */

#ifndef UPGRADE_H_
#define UPGRADE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

enum UpdateResult
{
    UpdateResult_no,                      //暂无结果
    UpdateResult_success,                 //升级成功
    UpdateResult_check_fail,              //校验失败
    UpdateResult_network_download_fail,   //网络下载失败
};

enum UpdateFlag
{
    NoUpdate,   //不需要升级
    ReadyUpdate,//准备升级
    UpgradInProgress,  //正在升级
};

enum UpgradeMode
{
    CMD_NoUpgrade,      //不需要升级
    CMD_AdviseUpgrade,  //建议升级
    CMD_CoerceUpgrade,  //强制升级
};

enum CheckVerify
{
    VerifyNo,           //还未得出校验结果
    VerifyFail,         //校验失败
    VerifySuccess,      //校验成功
};

void set_update_result_flag(enum UpdateResult flag);
enum UpdateResult get_update_result_flag(void);
void Set_update_flag (enum UpdateFlag flag);
enum UpdateFlag Get_update_flag (void);
void Set_check_verify (enum CheckVerify flag);
enum CheckVerify Get_check_verify (void);
void network_update_exe(void);
char *Get_Upgrade_version(void);
int web_upgrade_firmware(int check);
enum UpdateFlag Get_update_flag (void);
enum UpgradeMode GetUpgradeMode(void);
#ifdef __cplusplus
}
#endif

#endif /* AIRKISS_H_ */
