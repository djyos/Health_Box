
//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * app_main.c
 *
 *  Created on: 2014-5-28
 *      Author: Administrator
 */
#include "stdint.h"
#include "stddef.h"
#include "cpu_peri.h"
#include "cpu_peri_adc.h"
#include "project_config.h"
#include <sys/socket.h>
#include <app_flash.h>
#include "wlan_ui_pub.h"
#include "GuiWin/inc/GuiInfo.h"
#include "GuiWin/inc/WinSwitch.h"
#include <upgrade.h>
#include "shell.h"
#include <wdt_soft.h>
#include <wdt_pub.h>
#include <qspi_pub.h>
#include "VoicePrompt.h"
#include "board.h"
#include "Net_DevApi.h"
#include "wifi_app.h"
#include "Uart_DevComm.h"
#include "Net_DevApi.h"

int login_ok = 0;
volatile int flag_connected = 0;
char power_percentage = 0;

static int flag_ScamWifi = 0;
static int Start_ConnectWifi = 0;

extern  WIFI_CFG_T  LoadWifi;

extern char vol_to_percentage(int assign);
extern int SetClock(int hour, int min);
extern int GetTimeHourMinute(int *hour, int *min);
extern int GetWifi_SaveStatue(char *ssid);

void Set_flag_ScamWifi(int statue)
{
     flag_ScamWifi = statue;
}

void Set_Start_ConnectWifi(int statue)
{
    Start_ConnectWifi = statue;
}

void Set_login_ok(int statue)
{
    login_ok = statue;
}

int is_wifi_connected()
{
    return flag_connected;
}

char current_power(void)
{
    return power_percentage;
}

bool_t UsbIsCharge = false;
bool_t IsCharge(void)
{
    return UsbIsCharge;
}
void SetChargeFlag(enum ChargeFlag flag)
{
    if(flag == NoCharge)
        UsbIsCharge = false;
    else if(flag == OnCharge)
        UsbIsCharge = true;
}

int Power_on_control(void)
{
    int vol = 0;
    int ret = 0;

    if(djy_gpio_read(GPIO34))
    {
        SetChargeFlag(OnCharge);
        vol = vbat_voltage_get();
        vol -= 150;
    }
    else
        SetChargeFlag(NoCharge);
    power_percentage = vol_to_percentage(vol);
    printf("========= percent = %d %%  \r\n", power_percentage);    //显示当前的电池电压

    if(power_percentage == 0){
        ret = 1;
    }else{
        if(!djy_gpio_read(GPIO34))
        {
            vol = vbat_voltage_get() ;
            printf("vol = %d\r\n",vol);
            if(vol < 3340)
            {
                ret = 1;
                printf("电量不足，准备关机\r\n");
//                DJY_EventDelay(3000*1000);
                Set_ShoutDown();
            }
        }
    }

    return ret;
}

void set_cell_power(void)
{
    char i = 0, per = -1;

    while(i++ < 3)
    {
        per = vol_to_percentage(0);
        if(per != -1)
            break;
    }

    if(i <= 3)
        power_percentage = per;

//    printf(" ====== 拔充电器是的电量   = %d ======\r\n", power_percentage);
}

ptu32_t timer_EventMain(void)
{
    u8 count = 0;
    int local_hour = 0;
    int local_min = 0;
    int hour, min;
    char local_Percentage = 0;
    char Percentage = 0;
    char local_Power_flag = 0;
    char Power_flag = 0;
    while (1) {
//        DelayCloseWiFi(1000*15);
        if (flag_connected && !login_ok) {
            printf("==user_login==\r\n");
//            int user_login();
//            ret = user_login();
            Update_DeviceKey();
//            testAAA();
//            printf("login message: ret = %d!\r\n", ret);
//            if (ret == 1) {
//            login_ok = 1;
//            }
        }

//        if (login_ok==1){
//                Update_ServerInfo();
//        }

        DJY_EventDelay(1000*1000);

//        /************更新当前显示时间*******************/
//        if (flag_connected) {
            GetTimeHourMinute(&hour, &min);
            if(Get_SelectionWinType()==WIN_Main_WIN)
            {
                if (local_hour != hour || local_min != min) {
                    local_hour = hour;
                    local_min = min;
                    SetClock(hour, min);
                }
            }
//        }

            /************更新当前显示电量*******************/
            if(Get_PowerFlag()==true)
            {
                Set_PowerFlag(false);
                PowerLower_Win();
            }
            if(IsCharge() == false)
                Power_flag =0;
            else
                Power_flag = 1;
            if(count==0 || count >= 10 ||Power_flag != local_Power_flag)
            {
                count = 1;
                Percentage = current_power();
                if(Percentage != local_Percentage || Power_flag != local_Power_flag)
                {
                    local_Power_flag = Power_flag;
                    local_Percentage = Percentage;
                    Set_MainWinPower(local_Percentage);
                }
            }
            count++;

        /************更新当前wifi显示图标*******************/
        switch(mhdr_get_station_status()) {
#if(CN_BEKEN_SDK_V3 == 1)
            case RW_EVT_STA_GOT_IP:
            if (flag_connected != 1)
            {
                flag_connected = 1;
                Wifi_Connectedflag(1);
                VoicePrompt(wifi_connect_sucess);
                DjyWifi_StaConnectDone();
            }
            break;
            case RW_EVT_STA_IDLE:
            case RW_EVT_STA_CONNECTING:
            case RW_EVT_STA_BEACON_LOSE:
            case RW_EVT_STA_PASSWORD_WRONG:
            case RW_EVT_STA_NO_AP_FOUND:
            case RW_EVT_STA_ASSOC_FULL:
            case RW_EVT_STA_DISCONNECTED:
            case RW_EVT_STA_CONNECT_FAILED:
            case RW_EVT_STA_CONNECTED:
#else
        case MSG_GOT_IP:
            if (flag_connected != 1) {
                flag_connected = 1;
                Wifi_Connectedflag(1);
                VoicePrompt(wifi_connect_sucess);
                DjyWifi_StaConnectDone();
            }
            break;
        case MSG_PASSWD_WRONG:
//            printf("wifi passwd error!\r\n");
//            break;
        case MSG_IDLE:
        case MSG_CONNECTING:
        case MSG_NO_AP_FOUND:
        case MSG_CONN_FAIL:
        case MSG_CONN_SUCCESS:
#endif
        default:
            if (flag_connected != 0) {
                flag_connected = 0;
                Wifi_Connectedflag(0);
            }
            break;
        }
        if(flag_connected)
        {
            if((Get_update_flag() == UpgradInProgress) || (GetUpgradeMode() == CMD_CoerceUpgrade))
            {
//                printf("-------456-----\r\n");
                Set_CtrlUpdateWin_Ctrl();
                if(web_upgrade_firmware(0) > 0)
                    network_update_exe();
            }
        }

    }

    return 0;
}

bool_t timer_event_init(void)
{
    u16 evtt_timer;
    evtt_timer = DJY_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS + 1, 0, 0, timer_EventMain, 0, 0x1000, "timer_event");
    if(evtt_timer != CN_EVENT_ID_INVALID)
    {
        DJY_EventPop(evtt_timer, NULL, 0, 0, 0, 0);
    }else
    {
        printf("\r\evtt_timer Event Start Fail!\r\n");
    }

    return true;
}

ptu32_t Key_EventMain(void)
{
    int count = 0 ;
    int fresh_ui = 0;
    bool_t lock_long=false;
//    gpio_config(13, GMODE_INPUT_PULLUP);
//    LP_BSP_ResigerGpioToWakeUpL4(PS_DEEP_WAKEUP_GPIO,13,0,0);
//    gpio_config(13, GMODE_INPUT_PULLDOWN);
//    LP_BSP_ResigerGpioToWakeUpL4(0x2000,0);
    while(1)
    {
#if 1
        if(!djy_gpio_read(GPIO13))
        {
            if(500 > count && count >= 300 && lock_long)
            {
                if(Get_CloseScreen_Statue())
                {
                    Set_CloseScreen_Statue(false);
                    OpenScreen_Speaker();
                    Set_ShutDown_TimerCnt(0);
                }
                else
                {
                    ShutPower_Win();
                }
                count = 600;
                printf("22222222\r\n");

            }else if(300 > count && lock_long){
                count ++;
            }
        }else{
            if(100 > count  &&  count > 1 && lock_long)
            {
                if(!Get_CloseScreen_Statue())
                {
                    Set_CloseScreen_Statue(true);
                    CloseScreen_Speaker();
                    Set_ShutDown_TimerCnt(0);
                }

                printf("333333\r\n");
            }
            if(!lock_long)
            {
                lock_long=true;
            }
            count = 0 ;
        }

        if(Get_Test_Mode())
        {
            if(Get_Start_UartTest())
            {
                fresh_ui = 0;
                Set_Start_UartTest(0);
                Factort_UartTest();
            }else{
                fresh_ui ++ ;
                if(fresh_ui >= 100)
                {
                    fresh_ui = 0;
                    Refresh_GuiWin();
                }
            }
        }
        DJY_EventDelay(10*1000);
#else
        count++;
        if(count >= 500)
            ShutPower_Win();
        DJY_EventDelay(10*1000);
#endif
    }
    return 0;
}

bool_t Key_event_init(void)
{
    u16 evtt_timer;
    evtt_timer = DJY_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS + 1, 0, 0, Key_EventMain, 0, 0x1000, "Key_event");
    if(evtt_timer != CN_EVENT_ID_INVALID)
    {
        DJY_EventPop(evtt_timer, NULL, 0, 0, 0, 0);
    }else
    {
        printf("\r\evtt_timer Event Start Fail!\r\n");
    }

    return true;
}

ptu32_t Wifi_EventMain(void)
{
    int Connect_cnt = 0;
    int Connect_time = 40;
    while(1)
    {
        if(flag_ScamWifi == 1)
        {
            flag_ScamWifi = 2;
            Connect_cnt = 0;
            Start_ConnectWifi = 0;
            Get_ScanRouter();
            printf("Scam_Wifi finish\r\n");
        }

        if(flag_ScamWifi == 0)
        {
            if(LoadWifi.statue == 0x55)
            {
                if(flag_connected != 1)
                {
                    if((Connect_time == 40) && (Connect_cnt == 0))
                    {
                        if(GetWifi_SaveStatue(LoadWifi.WifiSsid))
                        {
                            printf("GetWifi_SaveStatue\r\n");
                            Connect_time = 20;
                        }
                    }
                    Connect_cnt ++;
                    if(Connect_cnt > Connect_time)
                    {
                        printf("Connect_cnt = %d\r\n",Connect_cnt);
                        Connect_cnt = 0;
                        Start_ConnectWifi = 2;
                    }
                }else{
                    Connect_cnt = 0;
                }
            }
        }

        if(Start_ConnectWifi)
        {
            printf("Start_ConnectWifi = %d\r\n",Start_ConnectWifi);
            if(Start_ConnectWifi == 1)
            {
                VoicePrompt(wifi_connecting);
                WifiSave(LoadWifi.WifiSsid, LoadWifi.WifiPassWd);
                /*必须加入延时，不然切换太快，wifi底层固件没处理好，偶尔发送数据不成功*/
    //                    DJY_EventDelay(300*1000);
            }
            Connect_time = 40;
            Start_ConnectWifi = 0;
            Connect_cnt = 0;

            printf("LoadWifi.WifiSsid = %s,LoadWifi.WifiPassWd = %s\r\n",LoadWifi.WifiSsid,LoadWifi.WifiPassWd);
            DjyWifi_StaDisConnect();
            DjyWifi_StaAdvancedConnect(LoadWifi.WifiSsid, LoadWifi.WifiPassWd);
        }

        DJY_EventDelay(500*1000);
    }
    return true;
}

bool_t Wifi_event_init(void)
{
    u16 evtt_timer;
    evtt_timer = DJY_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS + 1, 0, 0, Wifi_EventMain, 0, 0x1000, "wifi_event");
    if(evtt_timer != CN_EVENT_ID_INVALID)
    {
        DJY_EventPop(evtt_timer, NULL, 0, 0, 0, 0);
    }else
    {
        printf("\r\evtt_timer Event Start Fail!\r\n");
    }

    return true;
}

void CheckUpdate(void)
{
    Set_update_flag(NoUpdate);
    if(flag_connected)
    {
        if(web_upgrade_firmware(1) == 1)
        {
            Set_update_flag(ReadyUpdate);
        }
        printf("info: download firmware from the network done!\r\n");
    }
}

void DevicewakeUp(void)
{
    int longPressCounter = 0;
    int shutdown=0;

    do
    {
        if (0 == djy_gpio_read(GPIO13))
        {
            longPressCounter++;
            shutdown=0;
        }
        else
        {
            longPressCounter=0;
            if(shutdown==0)
            {
                shutdown=(int)(DJY_GetSysTime()/1000000);
            }
            else if((int)(DJY_GetSysTime()/1000000)-shutdown>=10)
            {
                Set_ShoutDown();
            }
        }
        DJY_EventDelay(100*1000);
    }
    while(longPressCounter<10);
}

ptu32_t djy_main(void)
{
    s32 j;

    LP_BSP_ResigerGpioToWakeUpL4(PS_DEEP_WAKEUP_GPIO,13,1,0);

    gpio_config(13, GMODE_INPUT_PULLUP);

    DevicewakeUp();

    if(GetSamlpingState() == Samlping_off)
        EnableSampling();

    Heap_Add(QSPI_DCACHE_BASE, 0x7fff80, 64, 0, false, "PSRAM");       //把QSPI接的RAM添加到heap当中

    UINT32 manual_cal_load_default_txpwr_tab(UINT32 is_ready_flash);
    int manual_cal_txpwr_tab_is_fitted(void);
    manual_cal_load_default_txpwr_tab(manual_cal_txpwr_tab_is_fitted);

    DHCP_ClientInit( );//dhcp client

    while(Power_on_control()){DJY_EventDelay(100);};

    Key_event_init();
    timer_event_init();
    Wifi_event_init();

    MainInterface();
    OpenBackLight();
    DJY_EventDelay(2000*1000);
    void Main_GuiWin(void);
    Main_GuiWin();

//    OpenSpeaker();
    mp3_module_init();

    UrlLoad();
#if 1
//    if(!WifiLoad())
//    {
//        strcpy(LoadWifi.WifiSsid,"ddc");
//        strcpy(LoadWifi.WifiPassWd,"12345678");
//
//        WifiSave(LoadWifi.WifiSsid, LoadWifi.WifiPassWd);
//    }
//    VoicePrompt(wifi_connecting);
    if(WifiLoad())
        Start_ConnectWifi = 2 ;

    printf("\n\r========================================================\r\n");
    int tmp = Dev_UarCommInit("/dev/UART1",1*1024*1024);
    if(tmp<0)
        printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
    else
        printf("[ok]:Dev_UarCommInit\r\n");
    tmp = Net_DevApiInit();
    if(tmp<0)
        printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
    else
        printf("[OK]:Net_DevApiInit\n\r");
    printf("========================================================\r\n");
#if 0
    tmp = Update_DeviceKey();
    if(tmp<0)
        printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
    char buf[100];
    tmp = Get_UserDeviceKey(buf,sizeof(buf));
    if(tmp<0)
        printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
    else
        printf("%s \n\r",buf);

    int uid  = Update_DeviceUid();
    printf("uid = %d  \n\r",uid);
    uid = Get_Deviceuid();
    printf("uid = %d  \n\r",uid);
    tmp = Update_DeviceComputeOrder();
    if(tmp<0)
        printf("error : file:%s  line:%d tmp:%d \r\n",__FILE__,__LINE__,tmp);
    char * str = Get_DeviceComputeOrder();
    if(str != NULL)
        printf("get str : %s \n\r");

#endif

//    DjyWifi_StaConnectDone();
#endif

//    CheckUpdate();
    j = 450;
    while(1)
    {
        if(j++ > 450)
        {
            power_percentage = vol_to_percentage(0);
            j = 0;
        }
        DJY_EventDelay(100*1000);
    }
    return 0;
}

