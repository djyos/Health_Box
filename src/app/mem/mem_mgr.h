#ifndef __MEM_MGR_H__
#define __MEM_MGR_H__

#define SHARE_BUF_SIZE 4*1024*1024

//int MemMgrInit();
//void *psram_malloc (unsigned int size);
//void psram_free (void *p);
//void *GetMp3BufAddr();
//int GetMp3BufSize();
//void *GetWavBufAddr();
//int GetWavBufSize();
void *GetUpdateBufAddr();
int GetAudioLoopBufSize();
void *GetAudioLoopBufAddr();
#endif
