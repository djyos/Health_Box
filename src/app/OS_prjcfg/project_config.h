/****************************************************
 *  Automatically-generated file. Do not edit!	*
 ****************************************************/

#ifndef __PROJECT_CONFFIG_H__
#define __PROJECT_CONFFIG_H__

#include <stdint.h>
#include <stddef.h>
//manual config start
//此处填写手动配置，DIDE生成配置文件时，不会被修改
//manual config end

#define    CN_RUNMODE_IBOOT                0                //IBOOT模式运行
#define    CN_RUNMODE_APP                  2                //由IBOOT加载的APP
#define    CFG_RUNMODE                     CN_RUNMODE_APP           //由IBOOT加载的APP
#define    CN_RUNMODE_BOOTSELF             1                       //无须IBOOT，自启动模式APP
//*******************************  Configure arp  ******************************************//
#define     CFG_ARP_HASHLEN             32      //占用一个指针
#define    CFG_MODULE_ENABLE_ARP           true
//*******************************  Configure black box  ******************************************//
#define    CFG_MODULE_ENABLE_BLACK_BOX     true
//*******************************  Configure board config  ******************************************//
#define    CFG_MODULE_ENABLE_BOARD_CONFIG  true
//*******************************  Configure cpu onchip adc  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_ADC  true
//*******************************  Configure cpu onchip audio  ******************************************//
#define CFG_RX_DMA_BUF_SIZE     8192        //
#define CFG_AUDIO_SAMPLE_RATE   8000    //
#define CFG_USE_AUD_DAC         true
#define CFG_USE_AUD_ADC         true
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_AUDIO  true
//*******************************  Configure cpu onchip boot  ******************************************//
#define CFG_POWER_ON_RESET_TO_BOOT    true //控制上电复位后是否强制运行iboot
#define    CFG_MODULE_ENABLE_BOOT          true
//*******************************  Configure cpu onchip flash  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_FLASH  true
//*******************************  Configure cpu onchip gpio  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_GPIO  true
//*******************************  Configure cpu onchip iwdt  ******************************************//
#define CFG_WDT_FEED_CYCLE          12000000       //单位us
#define CFG_BOOT_TIME_LIMIT         30000000       //允许保护启动加载过程才需要配置此项
#define CFG_DEFEND_ON_BOOT          false          //启动加载过程如果出现死机看门狗将复位，BK7251不支持，只能选false
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_IWDT  true
//*******************************  Configure cpu onchip lowpower control  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_LOWPOWER_CONTROL  true
//*******************************  Configure cpu onchip MAC  ******************************************//
#define CFG_WIFI_DEV_NAME        "BK7251_WiFi"//
#define CFG_FAST_DATA_FILE_NAME "/efs/fast_data.dat"    //
#define CFG_MAC_DATA_FILE_NAME "/efs/mac.dat"    //
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_MAC  true
//*******************************  Configure cpu onchip pwm  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_PWM  true
//*******************************  Configure cpu onchip qspi  ******************************************//
#define CFG_INSTALL_QSPI_WHEN_SYSINIT    true        //
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_QSPI  true
//*******************************  Configure cpu onchip random  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_RANDOM  true
//*******************************  Configure cpu onchip spi  ******************************************//
#define CFG_SPI_CLK                    30000000       //SPI 时钟。
#define CFG_SPI_CPOL                     1           //spi时钟极性（1：SCK在空闲状态处于高电平。0：反之）。
#define CFG_SPI_CPHA                     1           //spi时钟相位（1：在SCK周期的第二个边沿采样数据。0：在SCK周期的第一个边沿采样数据）
#define CFG_SPI_FLASH_RAM_POWER         true         //是否打开flash和ram的电源。
#define CFG_SPI_WORK_MODE_INTE          true        //设置SPI的工作模式，true为中断模式通信，false为普通模式。
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_SPI  true
//*******************************  Configure cpu onchip uart  ******************************************//
#define CFG_UART1_SENDBUF_LEN       256      //
#define CFG_UART1_RECVBUF_LEN       2048      //
#define CFG_UART2_SENDBUF_LEN       256      //
#define CFG_UART2_RECVBUF_LEN       256      //
#define CFG_UART1_ENABLE           true        //
#define CFG_UART2_ENABLE           true       //
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_UART  true
//*******************************  Configure debug information  ******************************************//
#define    CFG_MODULE_ENABLE_DEBUG_INFORMATION  true
//*******************************  Configure device file system  ******************************************//
#define CFG_DEVFILE_LIMIT       10      //定义设备数量
#define    CFG_MODULE_ENABLE_DEVICE_FILE_SYSTEM  true
//*******************************  Configure dhcp  ******************************************//
#define     CFG_DHCPD_ENABLE            false   //
#define     CFG_DHCPC_ENABLE            true    //
#define     CFG_DHCP_RENEWTIME          3600    //秒数
#define     CFG_DHCP_REBINDTIME         3600    //秒数
#define     CFG_DHCP_LEASETIME          3600    //秒数
#define     CFG_DHCPD_IPNUM             0x40    //64
#define     CFG_DHCPD_IPSTART           "192.168.0.2"    //
#define     CFG_DHCPD_SERVERIP          "192.168.0.253"  //
#define     CFG_DHCPD_ROUTERIP          "192.168.0.253"  //
#define     CFG_DHCPD_NETIP             "255.255.255.0"  //
#define     CFG_DHCPD_DNS               "192.168.0.253"  //
#define     CFG_DHCPD_DNSBAK            "192.168.0.253"  //
#define     CFG_DHCPD_DOMAINNAME       "domain"        //
#define    CFG_MODULE_ENABLE_DHCP          true
//*******************************  Configure djybus  ******************************************//
#define    CFG_MODULE_ENABLE_DJYBUS        true
//*******************************  Configure easy file system  ******************************************//
#define CFG_EFS_FILE_SIZE_LIMIT           3840                 //
#define CFG_EFS_MAX_CREATE_FILE_NUM       14                   //
#define CFG_EFS_MAX_OPEN_FILE_NUM         10                   //
#define CFG_EFS_MOUNT_POINT               "efs"      //
#define CFG_EFS_INSTALL_OPTION            MS_INSTALLCREAT      //MS_INSTALLFORMATMS_INSTALLCREAT,MS_INSTALLUSE中一个或多个的“或”
#define    CFG_MODULE_ENABLE_EASY_FILE_SYSTEM  true
//*******************************  Configure embflash install efs  ******************************************//
#define CFG_EMBFLASH_EFS_MOUNT_NAME            "efs"      //需要挂载的efs文件系统mount点名字
#define CFG_EMBFLASH_EFS_PART_START                  1008        //填写块号，块号从1008开始计算
#define CFG_EMBFLASH_EFS_PART_END                   1024        //1024是最后一块
#define CFG_EMBFLASH_EFS_PART_FORMAT               false      //是否需要格式化该分区。
#define    CFG_MODULE_ENABLE_EMBFLASH_INSTALL_EFS  true
//*******************************  Configure emflash insatall xip  ******************************************//
#define CFG_EFLASH_XIP_PART_START      0          //"分区起始块号，含"，填写块号，块号从0开始计算
#define CFG_EFLASH_XIP_PART_END        80         //"分区结束块号，不含"，80表示最后一块，如果结束块是6，起始块是0，则该分区使用的块为0，1，2，3，4，5块
#define CFG_EFLASH_XIP_PART_FORMAT     false      //是否需要格式化该分区。
#define CFG_EFLASH_XIPFSMOUNT_NAME   "xip-iboot"    //
#define    CFG_MODULE_ENABLE_EMFLASH_INSATALL_XIP  true
//*******************************  Configure file system  ******************************************//
#define CFG_CLIB_BUFFERSIZE            512      //
#define    CFG_MODULE_ENABLE_FILE_SYSTEM   true
//*******************************  Configure flash  ******************************************//
#define    CFG_MODULE_ENABLE_FLASH         true
//*******************************  Configure font  ******************************************//
#define CFG_FONT_DEFAULT  "gb2312_song_16"      //字体名在include/font目录中找
#define    CFG_MODULE_ENABLE_FONT          true
//*******************************  Configure ftp  ******************************************//
#define     CFG_FTPD_ENABLE            false   //暂未实现
#define     CFG_FTPC_ENABLE            true    //
#define    CFG_MODULE_ENABLE_FTP           true
//*******************************  Configure gb2312 charset  ******************************************//
#define    CFG_MODULE_ENABLE_GB2312_CHARSET  true
//*******************************  Configure gb2312 dot  ******************************************//
#define CFG_GB2312_12_SONG              zk_disable              //GB2312字体,zk_disable：不需要from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_12_SONG_FILENAME "zk_gb2316_12song.bin"  //若从文件读取则配置文件名
#define CFG_GB2312_16_SONG              from_array              //GB2312字体,zk_disable：不需要from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_16_SONG_FILENAME "zk_gb2316_16song.bin"  //若从文件读取则配置文件名
#define CFG_GB2312_16_YAHEI             zk_disable              //GB2312字体,zk_disable：不需要from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_16_YAHEI_FILENAME "zk_gb2316_16yahei.bin"  //若从文件读取则配置文件名
#define CFG_GB2312_16_YUAN              zk_disable              //GB2312字体,zk_disable：不需要from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_16_YUAN_FILENAME "zk_gb2316_16yuan.bin"  //若从文件读取则配置文件名
#define CFG_GB2312_16_KAI              zk_disable               //GB2312字体,zk_disable：不需要from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_16_KAI_FILENAME  "zk_gb2316_16kai.bin"   //若从文件读取则配置文件名
#define CFG_GB2312_16_HEI              zk_disable               //GB2312字体,zk_disable：不需要from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_16_HEI_FILENAME  "zk_gb2316_16hei.bin"   //若从文件读取则配置文件名
#define CFG_GB2312_16_FANG              zk_disable              //GB2312字体,zk_disable：不需要from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_16_FANG_FILENAME "zk_gb2316_16fang.bin"  //若从文件读取则配置文件名
#define CFG_GB2312_24_SONG              zk_disable              //GB2312字体,zk_disable：不需要from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_24_SONG_FILENAME "zk_gb2316_24song.bin"  //若从文件读取则配置文件名
#define CFG_GB2312_24_YAHEI              zk_disable             // "24点阵微软雅黑",GB2312字体,zk_disable：不需要，from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_24_YAHEI_FILENAME "zk_gb2316_24yahei.bin"// "字库文件名",若从文件读取，则配置文件名
#define CFG_GB2312_24_YUAN              zk_disable              //GB2312字体,zk_disable：不需要from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_24_YUAN_FILENAME "zk_gb2316_24yuan.bin"  //若从文件读取则配置文件名
#define CFG_GB2312_24_KAI              zk_disable               //GB2312字体,zk_disable：不需要from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_24_KAI_FILENAME  "zk_gb2316_24kai.bin"   //若从文件读取则配置文件名
#define CFG_GB2312_24_HEI              zk_disable               //GB2312字体,zk_disable：不需要from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_24_HEI_FILENAME  "zk_gb2316_24hei.bin"   //若从文件读取则配置文件名
#define CFG_GB2312_24_FANG              zk_disable              //GB2312字体,zk_disable：不需要from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_24_FANG_FILENAME "zk_gb2316_24fang.bin"  //若从文件读取则配置文件名
#define CFG_GB2312_32_SONG              zk_disable              //GB2312字体,zk_disable：不需要from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_32_SONG_FILENAME "zk_gb2316_32song.bin"  //若从文件读取则配置文件名
#define CFG_GB2312_32_YUAN              zk_disable              //GB2312字体,zk_disable：不需要from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_32_YUAN_FILENAME "zk_gb2316_32yuan.bin"  //若从文件读取则配置文件名
#define CFG_GB2312_32_KAI              zk_disable               //GB2312字体,zk_disable：不需要from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_32_KAI_FILENAME  "zk_gb2316_32kai.bin"   //若从文件读取则配置文件名
#define CFG_GB2312_32_HEI              zk_disable               //GB2312字体,zk_disable：不需要from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_32_HEI_FILENAME  "zk_gb2316_32hei.bin"   //若从文件读取则配置文件名
#define CFG_GB2312_32_FANG              zk_disable              //GB2312字体,zk_disable：不需要from_array：从数组读取，from_file：从文件读
    #define CFG_GB2312_32_FANG_FILENAME "zk_gb2316_32fang.bin"  //若从文件读取则配置文件名
#define    CFG_MODULE_ENABLE_GB2312_DOT    true
//*******************************  Configure graphical decorate development  ******************************************//
#define CFG_DESKTOP_WIDTH       0           //桌面尺寸（像素数）宽度0=显示器宽度
#define CFG_DESKTOP_HEIGHT      0           //桌面尺寸（像素数）高度0=显示器高度
#define CFG_DESKTOP_BUF         false        //内存稀少的硬件可不带缓存
#define CFG_DISPLAY_NAME        "lcdst7789v"         //须与bsp中显示器驱动模块配置的显示器名字相同
#define CFG_DESKTOP_NAME        "desktop"         //
#define CFG_DESKTOP_FORMAT      CN_SYS_PF_RGB565    //桌面窗口像素格式常数在gkernel.h中定义，一般使用与显示器相同颜色
#define CFG_GRAY_BASE_COLOR     CN_COLOR_WHITE      //像素格式设为灰度时才需要设置的“最亮”色可在gkernel.h中找到常用颜色定义
#define CFG_FILL_COLOR          CN_COLOR_BLUE       //创建桌面时的填充色用888格式，可在gkernel.h中找到常用颜色定义
#define    CFG_MODULE_ENABLE_GRAPHICAL_DECORATE_DEVELOPMENT  true
//*******************************  Configure graphical kernel  ******************************************//
#define CFG_GKERNEL_CMD_DEEP        1024    //上层应用（例如gdd）向gkernel传递命令的缓冲区长度（字节数）
#define CFG_USER_REQUEST_DEEP       128     //gkernel向上层请求命令的缓冲区长度（字节数）
#define    CFG_MODULE_ENABLE_GRAPHICAL_KERNEL  true
//*******************************  Configure heap  ******************************************//
#define CFG_DYNAMIC_MEM true  //即使选false也允许使用malloc-free分配内存，但使用有差别，详见 《……用户手册》内存分配章节
#define    CFG_MODULE_ENABLE_HEAP          true
//*******************************  Configure human machine interface  ******************************************//
#define CFG_HMIIN_DEV_LIMIT     2       //人机交互输入设备数量如键盘、鼠标等
#define    CFG_MODULE_ENABLE_HUMAN_MACHINE_INTERFACE  true
//*******************************  Configure icmp  ******************************************//
#define    CFG_MODULE_ENABLE_ICMP          true
//*******************************  Configure iicbus  ******************************************//
#define    CFG_MODULE_ENABLE_IICBUS        true
//*******************************  Configure int  ******************************************//
#define    CFG_MODULE_ENABLE_INT           true
//*******************************  Configure io analog iic bus  ******************************************//
#define    CFG_MODULE_ENABLE_IO_ANALOG_IIC_BUS  true
//*******************************  Configure ioiicconfig  ******************************************//
#define CFG_IO_IIC_BUS_NAME   "IoIic"   //
#define    CFG_MODULE_ENABLE_IOIICCONFIG   true
//*******************************  Configure kernel  ******************************************//
#define CFG_INIT_STACK_SIZE     4096    //定义初始化过程使用的栈空间一般按默认值就可以了
#define CFG_EVENT_LIMIT         30      //事件数量
#define CFG_EVENT_TYPE_LIMIT    30      //事件类型数
#define CFG_IDLESTACK_LIMIT     2048    //IDLE事件处理函数运行的栈尺寸一般按默认值就可以了
#define CFG_IDLE_MONITOR_CYCLE  30      //监视IDLE事件持续低于1/16 CPU占比的时间秒数，0=不监视
#define CFG_MAINSTACK_LIMIT     4096    //main函数运行所需的栈尺寸
#define CFG_OS_TINY             false   //true=用于资源紧缺的场合内核会裁剪掉：事件类型名字，事件处理时间统计。
#define    CFG_MODULE_ENABLE_KERNEL        true
//*******************************  Configure kernel object system  ******************************************//
#define CFG_OBJECT_LIMIT        8   //用完会自动扩充
#define CFG_HANDLE_LIMIT        8   //用完会自动扩充
#define    CFG_MODULE_ENABLE_KERNEL_OBJECT_SYSTEM  true
//*******************************  Configure loader  ******************************************//
#define CFG_UPDATEIBOOT_EN      false       //
#define CFG_START_APP_IS_VERIFICATION      false       //
#define  CFG_APP_RUNMODE  EN_DIRECT_RUN     //EN_DIRECT_RUN=直接从flash中运行；EN_FORM_FILE=从文件系统加载到内存运行
#define  CFG_APP_VERIFICATION   VERIFICATION_MD5   //
#define CFG_IBOOT_VERSION_SMALL       00        //xx.xx.__APP忽略
#define CFG_IBOOT_VERSION_MEDIUM      00        //xx.__.xxAPP忽略
#define CFG_IBOOT_VERSION_LARGE       01        //__.xx.xxAPP忽略
#define CFG_IBOOT_UPDATE_NAME      "/efs/iboot.bin"           //
#define CFG_APP_UPDATE_NAME        "/SD/app.bin"            //
#define CFG_FORCED_UPDATE_PATH     "/SD/djyapp.bin"           //
#define    CFG_MODULE_ENABLE_LOADER        true
//*******************************  Configure lock  ******************************************//
#define CFG_LOCK_LIMIT          40      //定义锁的数量包含信号量和互斥量
#define    CFG_MODULE_ENABLE_LOCK          true
//*******************************  Configure memory pool  ******************************************//
#define CFG_MEMPOOL_LIMIT       20      //
#define    CFG_MODULE_ENABLE_MEMORY_POOL   true
//*******************************  Configure message queue  ******************************************//
#define    CFG_MODULE_ENABLE_MESSAGE_QUEUE true
//*******************************  Configure misc  ******************************************//
#define    CFG_MODULE_ENABLE_MISC          true
//*******************************  Configure multiplex  ******************************************//
#define    CFG_MODULE_ENABLE_MULTIPLEX     true
//*******************************  Configure Nls Charset  ******************************************//
#define CFG_LOCAL_CHARSET       "gb2312"        //
#define    CFG_MODULE_ENABLE_NLS_CHARSET   true
//*******************************  Configure ppp  ******************************************//
#define    CFG_MODULE_ENABLE_PPP           true
//*******************************  Configure ring buffer and line buffer  ******************************************//
#define    CFG_MODULE_ENABLE_RING_BUFFER_AND_LINE_BUFFER  true
//*******************************  Configure router  ******************************************//
#define    CFG_MODULE_ENABLE_ROUTER        true
//*******************************  Configure sdcard power  ******************************************//
#define    CFG_MODULE_ENABLE_SDCARD_POWER  true
//*******************************  Configure shell  ******************************************//
#define CFG_SHELL_STACK            0x1000      //
#define CFG_ADD_ROUTINE_SHELL      true        //
#define CFG_ADD_EXPAND_SHELL       true        //
#define CFG_ADD_GLOBAL_FUN         false       //
#define CFG_SHOW_ADD_SHEELL        true        //
#define    CFG_MODULE_ENABLE_SHELL         true
//*******************************  Configure sock  ******************************************//
#define     CFG_SOCKET_NUM              10      //占一个 tagItem 结构
#define    CFG_MODULE_ENABLE_SOCK          true
//*******************************  Configure spi bus  ******************************************//
#define    CFG_MODULE_ENABLE_SPI_BUS       true
//*******************************  Configure stdio  ******************************************//
#define CFG_STDIO_STDIN_MULTI      true         //
#define CFG_STDIO_STDOUT_FOLLOW    true         //
#define CFG_STDIO_STDERR_FOLLOW    true         //
#define CFG_STDIO_FLOAT_PRINT      true         //
#define CFG_STDIO_STDIOFILE        true         //
#define CFG_STDIO_IN_NAME              "/dev/UART2"    //
#define CFG_STDIO_OUT_NAME             "/dev/UART2"    //
#define CFG_STDIO_ERR_NAME             "/dev/UART2"    //
#define    CFG_MODULE_ENABLE_STDIO         true
//*******************************  Configure tcp  ******************************************//
#define     CFG_TCP_REORDER             true    //资源充足才打开
#define     CFG_TCP_CCBNUM              15      //占一个 指针 和 struct ClientCB
#define     CFG_TCP_SCBNUM              5       //占一个 指针 和 struct ServerCB
#define     CFG_TCP_SOCKET_HASH_LEN     10      //用于通过“IP+port”四元组检索socket
#define    CFG_MODULE_ENABLE_TCP           true
//*******************************  Configure tcpip  ******************************************//
#define     CFG_NETPKG_MEMSIZE          0x6000  //
#define     CFG_TPL_PROTONUM            5       //占用一个 tagTplProtoItem 结构
#define    CFG_MODULE_ENABLE_TCPIP         true
//*******************************  Configure telnet  ******************************************//
#define     CFG_TELNETD_ENABLE          true    //
#define     CFG_TELNETC_ENABLE          false   //
#define    CFG_MODULE_ENABLE_TELNET        true
//*******************************  Configure tftp  ******************************************//
#define     CFG_TFTP_PATHDEFAULT       "/"   //TFTP默认工作目录
#define    CFG_MODULE_ENABLE_TFTP          true
//*******************************  Configure time  ******************************************//
#define CFG_LOCAL_TIMEZONE      8      //北京时间是东8区
#define    CFG_MODULE_ENABLE_TIME          true
//*******************************  Configure touch  ******************************************//
#define    CFG_MODULE_ENABLE_TOUCH         true
//*******************************  Configure uart device file  ******************************************//
#define    CFG_MODULE_ENABLE_UART_DEVICE_FILE  true
//*******************************  Configure udp  ******************************************//
#define     CFG_UDP_CBNUM               10      //占用一个 tagUdpCB 结构
#define     CFG_UDP_HASHLEN             4       //占用一个指针
#define     CFG_UDP_PKGMSGLEN        1472       //udp最大包长度
#define     CFG_UDP_BUFLENDEFAULT    0x2000     //8KB
#define    CFG_MODULE_ENABLE_UDP           true
//*******************************  Configure utf8 charset  ******************************************//
#define    CFG_MODULE_ENABLE_UTF8_CHARSET  true
//*******************************  Configure watch dog  ******************************************//
#define CFG_WDT_LIMIT           10      //允许养狗数量
#define    CFG_MODULE_ENABLE_WATCH_DOG     true
//*******************************  Configure xip iboot file system  ******************************************//
#define    CFG_MODULE_ENABLE_XIP_IBOOT_FILE_SYSTEM  true
//*******************************  Configure touchscreen FT6236  ******************************************//
#define  CFG_TOUCH_SIZE  32     //
#define CT_MAX_TOUCH  5                         //支持最多5点触摸
#define CFG_TOUCH_ADJUST_FILE   "/efs/touch_init.dat"  //保存触摸屏矫正参数的文件
#define CFG_FT6236_BUS_NAME     "IoIic"        //触摸芯片使用的IIC总线名称
#define CFG_FT6236_TOUCH_NAME   "FT6236"       //配置触摸屏名称
#define CFG_TARGET_DISPLAY_NAME "lcdst7789v"      //配置触摸屏所在显示器的名称
#define    CFG_MODULE_ENABLE_TOUCHSCREEN_FT6236  true
//*******************************  Configure LCD driver ST7789V  ******************************************//
#define  CFG_LCD_SIZE  32     //
#define CFG_LCD_XSIZE   320             //
#define CFG_LCD_YSIZE   240             //
#define CFG_LCD_XSIZE_UM   64800            //
#define CFG_LCD_YSIZE_UM   48600            //
#define CFG_ST7789V_DISPLAY_NAME        "lcdst7789v"    //配置液晶显示的名称
#define CFG_ST7789V_HEAP_NAME           "PSRAM"        //配置液晶驱动所使用的堆名称
#define    CFG_MODULE_ENABLE_LCD_DRIVER_ST7789V  true
//*******************************  Core Clock  ******************************************//
#define    CFG_CORE_MCLK                   (180.0*Mhz)       //主频，内核要用，必须定义
//*******************************  DjyosProduct Configuration  ******************************************//
#define    PRODUCT_MANUFACTURER_NAME       "深圳市创联时代科技有限公司"   //厂商名称
#define    PRODUCT_PRODUCT_CLASSIFY        "健康盒子"            //产品分类
#define    PRODUCT_PRODUCT_MODEL           "JK-1"            //产品型号
#define    PRODUCT_VERSION_LARGE           0                 //版本号,__.xx.xx
#define    PRODUCT_VERSION_MEDIUM          4                 //版本号,xx.__.xx
#define    PRODUCT_VERSION_SMALL           1                 //版本号,xx.xx.__
#define    PRODUCT_PRODUCT_MODEL_CODE      "JIUP2P"          //产品型号编码
#define    PRODUCT_BRANCH                  ""                //产品分支
#define    PRODUCT_PASSWORD                "002"             //产品秘钥
#define    PRODUCT_OTA_ADDRESS             "factory.cpolar.cn"    //OTA服务器地址
#define    PRODUCT_BOARD_TYPE              "HealthBox"       //板件类型
#define    PRODUCT_CPU_TYPE                "bk7251"          //CPU类型


#endif
