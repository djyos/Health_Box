#include "playmp3.h"

#include "mad.h"
#include "file.h"
#include "LoopBytes.h"
#include "cpu_peri.h"
#include "typedef.h"
#include "audio_pub.h"
#include "LoopRec.h"
#include "../mem/mem_mgr.h"
#include <board.h>

#define MP3_CHANGE_2_CHANNEL


#define debug printf
#define assert(x) if (!(x)) {printf("assert error: %s!\r\n", __FUNCTION__); while (1) {};}

struct StLoopBytesMgr *GetMgrOfLoopMgr(); //wav and mp3 share loop buffer.
int InitLoopBytesBuffer();


static struct StMp3Stream gMp3Stream;
static int local_samplerate=0;//16000;
static int local_channel = 1;
u32 u32g_Volume = 90;

#define BUFFER_MAX (4*1024)

struct SemaphoreLCB* media_stop_sem = 0;
volatile int media_stop_flag = 0;
int media_init()
{
    if (media_stop_sem == 0) {
        media_stop_flag = 0;
        media_stop_sem = Lock_SempCreate(1,0,CN_BLOCK_FIFO,"media_stop");
        if (media_stop_sem==0) {
            printf("error: media sem null!\r\n");
            return -1;
        }
    }
    return 0;
}
void media_stop()
{
    printf("media_stop!\r\n");
    media_stop_flag = 1;
}

int media_is_stop()
{
    return media_stop_flag == 1;
}

void media_play()
{
    if (media_stop_flag==1) {
        media_stop_flag = 0;
        if (media_stop_sem) {
            printf("media play!\r\n");
            Lock_SempPost(media_stop_sem);
        }
    }
}

uint32_t media_data_send(char *buf, uint32_t len)
{
    if (media_stop_flag == 1 && media_stop_sem) {
        printf("info: media stop!\r\n");
        Lock_SempPend(media_stop_sem, 0xFFFFFFFF);
    }
    return djy_audio_dac_write(buf, len);
}



typedef struct StMp3Stream {
#if MP3_DEBUG
    unsigned int time_mark;
    volatile unsigned int db_mark_decode;
    volatile unsigned int db_mark_decode_max;
#endif
    volatile unsigned int end_flag;
    volatile unsigned int end_done;
    volatile unsigned int mp3_duration; //ms
    volatile unsigned int mp3_samplerate;
    volatile unsigned int mp3_bitrate;
    int nReaded;
    int nPos;
    unsigned char buf[BUFFER_MAX];
    int (*getdata)(unsigned char *buf, int len, int timeout);
}StMp3Stream;

void AudioDevReset()
{
    if(GetSpeakerState() == Speaker_on)
        CloseSpeaker();
    djy_audio_dac_close();
    djy_audio_dac_open(AUDIO_DMA_BUFF_SIZE, local_channel, local_samplerate);
    djy_audio_dac_ctrl(AUD_DAC_CMD_SET_VOLUME, &u32g_Volume);
    if(GetSpeakerState() == Speaker_off)
        OpenSpeaker();
}


static int  AdjustDelay4DMAPlayEx (unsigned char *frm_data, int frm_len)
{
    int n = 0;
    int writed = 0;
    while (1) {
         n = media_data_send((char*)&frm_data[writed], frm_len-writed);
         if (n > 0) {
             writed += n;
         }
         if (writed < frm_len) {
             DJY_EventDelay(10*1000);
         }
         else {
             break;
         }
     }
    return 0;
}

void SetAudioOptions(unsigned int channnel, unsigned int samplerate)
{


    if (local_channel != (int)channnel) {
          local_channel = channnel;
          local_samplerate = samplerate;
          djy_audio_dac_close();
          djy_audio_dac_open(AUDIO_DMA_BUFF_SIZE, local_channel, local_samplerate);
          if (u32g_Volume > 100) {
              u32g_Volume = 100;
          }
          djy_audio_dac_ctrl(AUD_DAC_CMD_SET_VOLUME, &u32g_Volume);
          debug("====set u32g_Volume= %d===\r\n",u32g_Volume);
      }
    if (local_samplerate != (int)samplerate) {
        local_samplerate = samplerate;
        switch (samplerate) {
            case    audio_sample_rate_11025:
            case    audio_sample_rate_22050:
            case    audio_sample_rate_44100:
            case    audio_sample_rate_12000:
            case    audio_sample_rate_24000:
            case    audio_sample_rate_48000:
            case    audio_sample_rate_8000:
            case    audio_sample_rate_16000:
            case    audio_sample_rate_32000:
                local_samplerate = samplerate;
                break;
            default:
                local_samplerate = audio_sample_rate_44100;
                break;
        }
        djy_audio_dac_ctrl(AUD_DAC_CMD_SET_SAMPLE_RATE,&local_samplerate);
        debug("====set sample rate: %d!===\r\n", local_samplerate);
    }
}

void Mp3DataStartPlay ()
{
    struct StMp3Stream *pMgr = &gMp3Stream;
    pMgr->end_flag = 0;
    pMgr->end_done = 0;
}

int Mp3PlayIsEnded()
{
    struct StMp3Stream *pMgr = &gMp3Stream;
    return pMgr->end_done;
}


void Mp3DataEndPlay ()
{
    struct StMp3Stream *pMgr = &gMp3Stream;
    pMgr->end_flag = 1; //启动结束通知，还没结束，需要缓冲区播放完成才能结束。
}

int Mp3EndDataIsSet()
{
    struct StMp3Stream *pMgr = &gMp3Stream;
    return pMgr->end_flag == 1;
}

int Mp3IsBusy()
{
    struct StMp3Stream *pMgr = &gMp3Stream;
    return !pMgr->end_done;
}



CBFUN_PLAYER_DONE gMp3NotifyCB = 0;
void SetMp3DoneMsgCB(int (*event_cb) ())
{
    gMp3NotifyCB = event_cb;
}


static int NetGetData(unsigned char *buffer, int len, int timeout)
{
    struct StMp3Stream *pMgr = &gMp3Stream;
    int ret = 0;
    static int flag_notify = 0;
    if (GetMgrOfLoopMgr()) {
        while (media_is_stop()){
            DJY_EventDelay(100*1000);
        }
        while (pMgr->end_done == 1) {
            if (flag_notify==1) {
                if (gMp3NotifyCB) {
                    gMp3NotifyCB();
                }
                flag_notify = 0;
            }
            DJY_EventDelay(20*1000);
            //printf("info: pMgr->end_done==1, delay 1S!\r\n");
        }
        flag_notify = 1;
        ret = LoopBytesMgrPullPend(GetMgrOfLoopMgr(), buffer,  len, timeout, 0);
        if (ret == 0 && Mp3EndDataIsSet()) { //获取数据结束，可以停止。
            pMgr->end_done = 1;
            //printf("info: Mp3EndDataIsSet!\r\n");
        }
    }
    return ret;
}



static enum mad_flow header(void *data, struct mad_header const *stream)
{
    struct StMp3Stream *priv = (struct StMp3Stream *)data;
    if (stream) {
        priv->mp3_bitrate = stream->bitrate;
        priv->mp3_samplerate = stream->samplerate;
        //priv->mp3_duration = stream->duration.fraction*1000/352800000UL+stream->duration.seconds*1000;
        priv->mp3_duration = stream->duration.fraction/352800UL+stream->duration.seconds*1000;

        //printf("mp3_duration play(ms)=%d(ms)!\r\n", priv->mp3_duration);
    }
#if MP3_DEBUG
    gMp3Stream.db_mark_decode = GET_TIME_MS();
#endif
    return MAD_FLOW_CONTINUE;
}

static enum mad_flow input(void *data, struct mad_stream *stream)
{
//    enum mad_flow op = MAD_FLOW_CONTINUE;
    int last = 0;
    struct StMp3Stream *priv = (struct StMp3Stream *)data;
    if (!priv->getdata) return MAD_FLOW_STOP;

    if ( stream->buffer == NULL )
    {
        priv->nReaded = priv->getdata(priv->buf, BUFFER_MAX, 1000);
//        debug("info: %s(%d), getdata, ret=%d!\r\n", __FUNCTION__, __LINE__, priv->nReaded);
        if (priv->nReaded > 0) {
            mad_stream_buffer(stream, priv->buf, priv->nReaded);
        }
        return MAD_FLOW_CONTINUE;
    }

    priv->nPos = stream->next_frame - priv->buf;
    if (priv->nReaded > priv->nPos) {
        last = priv->nReaded - priv->nPos;
        memcpy(priv->buf, priv->buf+priv->nPos, last);
    }
#if 1
    priv->nReaded = priv->getdata(priv->buf+last, BUFFER_MAX-last, 1000);
    if (priv->nReaded <= 0) {
        return MAD_FLOW_CONTINUE;
    }
#else
//    debug("info: %s(%d), getdata, ret=%d!\r\n", __FUNCTION__, __LINE__, priv->nReaded);
    priv->nReaded = priv->getdata(priv->buf+last, BUFFER_MAX-last, 0xffffffff);
    if (priv->nReaded <= 0) {
        return MAD_FLOW_STOP;
    }
#endif
    priv->nPos = 0;
    priv->nReaded += last;
    mad_stream_buffer(stream, priv->buf, priv->nReaded);

    return MAD_FLOW_CONTINUE;
}


static inline signed int scale(mad_fixed_t sample)
{
    sample += (1L << (MAD_F_FRACBITS - 16));
    if (sample >= MAD_F_ONE)
        sample = MAD_F_ONE - 1;
    else if (sample < -MAD_F_ONE)
        sample = -MAD_F_ONE;
    return sample >> (MAD_F_FRACBITS + 1 - 16);
}


static enum mad_flow output(void *data, struct mad_header const *header, struct mad_pcm *pcm)
{
    (void)data;
    (void)header;
    struct StMp3Stream *priv= &gMp3Stream;
    unsigned int nchannels;
    unsigned int nsamples;
    unsigned int n;
    mad_fixed_t const *left_ch;
    mad_fixed_t const *right_ch;

    unsigned char *ptmp = NULL;
#if 1
    static unsigned char buffer[6912];
#else
    static unsigned char *buffer = 0;
    if (buffer==0) {
        buffer = (unsigned char*)malloc(6912);
    }
    if (buffer==0) {
        return MAD_FLOW_IGNORE;
    }
#endif



    nchannels = pcm->channels;
    n = nsamples = pcm->length;
    left_ch = pcm->samples[0];
#ifdef MP3_CHANGE_2_CHANNEL
    if (nchannels == 1) {
        right_ch = pcm->samples[0];
        nchannels = 2;
    }
    else {
        right_ch = pcm->samples[1];
    }
#else
    right_ch = pcm->samples[1];
#endif
    ptmp = buffer;
    while (nsamples--) {
        signed int sample;
        sample = scale(*left_ch++);
        *(ptmp++) = sample >> 0;
        *(ptmp++) = sample >> 8;
        if (nchannels == 2) {
            sample = scale(*right_ch++);
            *(ptmp++) = sample >> 0;
            *(ptmp++) = sample >> 8;
        }
    }
    if (nchannels == 2)
        n *= 4;
    else
        n *= 2;


    ptmp = buffer;

    //audio_adc_ctrl(AUD_ADC_CMD_SET_CHANNEL, &nchannels);
    SetAudioOptions(nchannels, priv->mp3_samplerate);


#if MP3_DEBUG
    if (gMp3Stream.db_mark_decode_max < GET_TIME_MS()-gMp3Stream.db_mark_decode) {
        gMp3Stream.db_mark_decode_max = GET_TIME_MS()-gMp3Stream.db_mark_decode;
    }
    //printf("mp3 frame decode spend %d(ms), max=%d(ms)!\r\n", GET_TIME_MS()-gMp3Stream.db_mark_decode, gMp3Stream.db_mark_decode_max);
#endif


#if 0
    while (n) {
        wrote = djy_audio_dac_write(ptmp, n);
        ptmp += wrote;
        n -= wrote;
    }
#else

    AdjustDelay4DMAPlayEx (ptmp, n);
#endif
    return MAD_FLOW_CONTINUE;
}


static enum mad_flow error(void *data, struct mad_stream *stream, struct mad_frame *frame)
{
    (void)data;
    (void)stream;
    (void)frame;

    //printf("warning: frame decode return error, and continue flow ...!\r\n");
    return MAD_FLOW_CONTINUE;
}

unsigned int Mp3Framebitrate()
{
    struct StMp3Stream *priv = &gMp3Stream;
    return priv->mp3_bitrate;
}


int Mp3PlayData(unsigned char *buf, int len, unsigned int timeout)
{
//    struct StMp3Stream *pMgr = &gMp3Stream;
    int opt = 0;
    int ret = 0;
    if (len<=0) return -1;
    opt = 1; //More than one byte will notify to the mp3 play process.
    if (GetMgrOfLoopMgr()) {
        Mp3DataStartPlay ();
        LoopBytesMgrCtrl(GetMgrOfLoopMgr(), OPT_LOOPBYTES_SET_THRESHOLDADD, &opt, 4);
        opt = 1;// pull_mode == 1 , get it instantly
        LoopBytesMgrCtrl(GetMgrOfLoopMgr(), OPT_LOOPBYTES_SET_PULL_MODE, &opt, 4);
        ret = LoopBytesMgrPushPend(GetMgrOfLoopMgr(), buf, len, timeout, 0);

    }
    return ret;
}

//int mp3_loopbytes_is_empty()
//{
//    if (GetMgrOfLoopMgr()) {
//        return LoopBytesMgrTotals(GetMgrOfLoopMgr())==0;
//    }
//    return 0;
//}


void clear_mp3_stream() {
    struct StMp3Stream *pMgr = &gMp3Stream;
#if MP3_DEBUG
    pMgr->time_mark = 0;
    pMgr->db_mark_decode = 0;
    pMgr->db_mark_decode_max = 0;
#endif
    pMgr->mp3_duration = 0; //ms
    pMgr->mp3_samplerate = 0;
    pMgr->mp3_bitrate = 0;
    pMgr->nReaded = 0;
    pMgr->nPos = 0;
    memset(pMgr->buf, 0, BUFFER_MAX);
    //pMgr->getdata = 0;
}

ptu32_t decode_mp3(void)
{
    int ret = 0;
    struct mad_decoder decoder;
    while (1) {
        if (GetMgrOfLoopMgr()) LoopBytesMgrReset(GetMgrOfLoopMgr());
        clear_mp3_stream();
        gMp3Stream.getdata = NetGetData;


        mad_decoder_init(&decoder, &gMp3Stream, input, header, 0, output, error, 0);
        mad_decoder_options(&decoder, 0);
        ret = mad_decoder_run(&decoder, MAD_DECODER_MODE_SYNC);

        mad_decoder_finish(&decoder);
        DJY_EventDelay(1000*1000);
        printf("--------mp3--------loop!!!\r\n");
    }
    return ret;
}

int mp3_stream_reset()
{
    int cnt = 0;
    struct StMp3Stream *pMgr = &gMp3Stream;
    pMgr->end_flag = 1;//开始就禁止播放。
    while (pMgr->end_done != 1) {
        DJY_EventDelay(100*1000);
        if (cnt++ > 10) break;
    }
    pMgr->end_done = 1;// end_flag只是告诉结束，真正结束需要判断end_done==1
    if (GetMgrOfLoopMgr()) {
        LoopBytesMgrReset(GetMgrOfLoopMgr());
    }
    return 0;
}

int mp3_play_from_file(char *path)
{
    int ret = 0;
    int length = 1024;

    void AudioDevReset();
    int mp3_stream_reset();
    int wav_stream_reset();
    AudioDevReset();
    mp3_stream_reset();
    wav_stream_reset();

    FILE *f = fopen(path, "rb");
    if (f == 0) {
        printf("error: mp3_play_from_file, path=%s!\r\n", path);
        return -1;
    }
    fseek(f, 0L, SEEK_END);
    int file_size = ftell(f);
    fseek(f, 0L, SEEK_SET);
    printf ("info: file size: %d!\r\n", file_size);

    unsigned char *p = (unsigned char*)malloc(length);
    if (p == 0) goto ERROR_RET;

    int n = 0;
    int pos = 0;
    int offset = 0;
    while (1) {
        if (offset >= file_size) break;
        n = fread(p, 1, length, f);
        if (n > 0) {
            offset += n;
            pos = 0;
            while (1) {
                if (pos >= n) break;
                ret = Mp3PlayData(p+pos, n-pos, 5000);
                if (ret > 0){
                    pos += ret;
                }
                else {
                    printf("mp3_play_from_file->Mp3PlayData, ret=%d!\r\n", ret);
                    DJY_EventDelay(300*1000);
                }
            }
        }
        else {
            printf("mp3_play_from_file->fread, n=%d!\r\n", n);
            DJY_EventDelay(300*1000);
        }
    }

ERROR_RET:
    Mp3DataEndPlay ();
    if (f) fclose(f);
    if (p) free(p);
    return 0;
}


int mp3_module_init()
{
    struct StMp3Stream *pMgr = &gMp3Stream;
    memset(pMgr, 0, sizeof(struct StMp3Stream));
    pMgr->end_flag = 1;//开始就禁止播放。
    pMgr->end_done = 1;// end_flag只是告诉结束，真正结束需要判断end_done==1
    media_init();

    static unsigned char mp3_stack[0x2200];

    djy_audio_dac_open(AUDIO_DMA_BUFF_SIZE, 1, audio_sample_rate_44100);

    InitLoopBytesBuffer();
    u16 task_playmp3 = DJY_EvttRegist(EN_CORRELATIVE, CN_PRIO_REAL, 0, 1, decode_mp3, /*NULL*/mp3_stack, sizeof(mp3_stack), "playmp3");
    if(task_playmp3 != CN_EVTT_ID_INVALID)
    {
        DJY_EventPop(task_playmp3,NULL,0,0,0,0);
    }
    return 0;
}


