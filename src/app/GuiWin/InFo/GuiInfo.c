//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * BmpInfo.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *
 *  功能:对要显示的Bmp文件集中管理，提供获取获取相应Bmp文件的接口
 *
 */
#define  GUI_INFO_FILE_C_
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include <gdd_widget.h>
#include <stddef.h>
#include "stdio.h"
#include "cJSON.h"
#include "string.h"
#include "stdlib.h"
#include "board.h"
#include <app_flash.h>
#include <dbug.h>
#include <time.h>
#include <lowpower.h>
//============================所有压缩图片的文件数组======================================
static const char MainInterface_bmp[] = {//开机界面图
#include "../PictureMaterial/MainInterface_bmp"
};
//static const char Background_bmp[] = {//背景Bmp格式文件
//#include "../PictureMaterial/Background_bmp"
//};
static const char Setting_bmp[] = {
#include "../PictureMaterial/Setting_bmp"
};
static const char ClickMedical_bmp[] = {
#include "../PictureMaterial/ClickMedical_bmp"
};
static const char Quantum_bmp[] = {
#include "../PictureMaterial/Quantum_bmp"
};
static const char Djyos_bmp[] = {
#include "../PictureMaterial/Djyos_bmp"
};
static const char Click_bmp[] = {
#include "../PictureMaterial/Click_bmp"
};
static const char LoginFail_bmp[] = {
#include "../PictureMaterial/LoginFail_bmp"
};
static const char LoginFailText_bmp[] = {
#include "../PictureMaterial/LoginFailText_bmp"
};
static const char ResetLogin_bmp[] = {
#include "../PictureMaterial/ResetLogin_bmp"
};
static const char Detection1_bmp[] = {
#include "../PictureMaterial/Detection1_bmp"
};
static const char Detection2_bmp[] = {
#include "../PictureMaterial/Detection2_bmp"
};
static const char Detection3_bmp[] = {
#include "../PictureMaterial/Detection3_bmp"
};
static const char ExitLogin_bmp[] = {
#include "../PictureMaterial/ExitLogin_bmp"
};
static const char Begin_bmp[] = {
#include "../PictureMaterial/Begin_bmp"
};
static const char Stop_bmp[] = {
#include "../PictureMaterial/Stop_bmp"
};
static const char Wifi01_bmp[] = {
#include "../PictureMaterial/Wifi01_bmp"
};
static const char Wifi02_bmp[] = {
#include "../PictureMaterial/Wifi02_bmp"
};
static const char Dis0_bmp[] = {
#include "../PictureMaterial/Dis0_bmp"
};
static const char Dis1_bmp[] = {
#include "../PictureMaterial/Dis1_bmp"
};
static const char Dis2_bmp[] = {
#include "../PictureMaterial/Dis2_bmp"
};
static const char Dis3_bmp[] = {
#include "../PictureMaterial/Dis3_bmp"
};
static const char Dis4_bmp[] = {
#include "../PictureMaterial/Dis4_bmp"
};
static const char Dis5_bmp[] = {
#include "../PictureMaterial/Dis5_bmp"
};
static const char Dis6_bmp[] = {
#include "../PictureMaterial/Dis6_bmp"
};
static const char Dis7_bmp[] = {
#include "../PictureMaterial/Dis7_bmp"
};
static const char Dis8_bmp[] = {
#include "../PictureMaterial/Dis8_bmp"
};
static const char Dis9_bmp[] = {
#include "../PictureMaterial/Dis9_bmp"
};
static const char Dis10_bmp[] = {
#include "../PictureMaterial/Dis10_bmp"
};
static const char NetSetting_bmp[] = {
#include "../PictureMaterial/NetSetting_bmp"
};
static const char FirmwareUpdate_bmp[] = {
#include "../PictureMaterial/FirmwareUpdate_bmp"
};
static const char back_bmp[] = {
#include "../PictureMaterial/back_bmp"
};
static const char Success_bmp[] = {
#include "../PictureMaterial/Success_bmp"
};
static const char SuccessText_bmp[] = {
#include "../PictureMaterial/SuccessText_bmp"
};
static const char Fail_bmp[] = {
#include "../PictureMaterial/Fail_bmp"
};
static const char FailText_bmp[] = {
#include "../PictureMaterial/FailText_bmp"
};
static const char Lock_bmp[] = {
#include "../PictureMaterial/Lock_bmp"
};
static const char WifFinish_bmp[] = {
#include "../PictureMaterial/WifFinish_bmp"
};
static const char Wifi1_bmp[] = {
#include "../PictureMaterial/Wifi1_bmp"
};
static const char Wifi2_bmp[] = {
#include "../PictureMaterial/Wifi2_bmp"
};
static const char Wifi3_bmp[] = {
#include "../PictureMaterial/Wifi3_bmp"
};
static const char Wifi4_bmp[] = {
#include "../PictureMaterial/Wifi4_bmp"
};
static const char ResetConfig_bmp[] = {
#include "../PictureMaterial/ResetConfig_bmp"
};
static const char NetConnecting1_bmp[] = {
#include "../PictureMaterial/NetConnecting1_bmp"
};
static const char NetConnecting2_bmp[] = {
#include "../PictureMaterial/NetConnecting2_bmp"
};
static const char NetConnectingText_bmp[] = {
#include "../PictureMaterial/NetConnectingText_bmp"
};
static const char NetSuccess_bmp[] = {
#include "../PictureMaterial/NetSuccess_bmp"
};
static const char NetkFail_bmp[] = {
#include "../PictureMaterial/NetFail_bmp"
};
static const char NetFailText_bmp[] = {
#include "../PictureMaterial/NetFailText_bmp"
};
static const char Finish_bmp[] = {
#include "../PictureMaterial/Finish_bmp"
};
//static const char UploadFail_bmp[] = {
//#include "../PictureMaterial/UploadFail_bmp"
//};
//static const char UploadFinish_bmp[] = {
//#include "../PictureMaterial/UploadFinish_bmp"
//};
//static const char UploadFinishText_bmp[] = {
//#include "../PictureMaterial/UploadFinishText_bmp"
//};
static const char Uploading1_bmp[] = {
#include "../PictureMaterial/Uploading1_bmp"
};
static const char Uploading2_bmp[] = {
#include "../PictureMaterial/Uploading2_bmp"
};
static const char UploadingText_bmp[] = {
#include "../PictureMaterial/UploadingText_bmp"
};
static const char Prompt_bmp[] = {
#include "../PictureMaterial/Prompt_bmp"
};
static const char PromptText_bmp[] = {
#include "../PictureMaterial/PromptText_bmp"
};
static const char Confirm_bmp[] = {
#include "../PictureMaterial/Confirm_bmp"
};
static const char Again_bmp[] = {
#include "../PictureMaterial/Again_bmp"
};
//static const char AgainUpload_bmp[] = {
//#include "../PictureMaterial/AgainUpload_bmp"
//};
static const char MedicalText_bmp[] = {
#include "../PictureMaterial/MedicalText_bmp"
};
static const char NetloadFail_bmp[] = {
#include "../PictureMaterial/NetloadFail_bmp"
};
static const char NetloadFailText_bmp[] = {
#include "../PictureMaterial/NetloadFailText_bmp"
};
static const char Netloading_bmp[] = {
#include "../PictureMaterial/Netloading_bmp"
};
static const char NetloadingText_bmp[] = {
#include "../PictureMaterial/NetloadingText_bmp"
};

static const char Ok_bmp[] = {
#include "../PictureMaterial/Ok_bmp"
};
static const char Cancel_bmp[] = {
#include "../PictureMaterial/Cancel_bmp"
};
static const char Powering_bmp[] = {
#include "../PictureMaterial/Powering_bmp"
};
static const char PowerLogo_1_bmp[] = {
#include "../PictureMaterial/PowerLogo_1_bmp"
};
static const char PowerLogo_25_bmp[] = {
#include "../PictureMaterial/PowerLogo_25_bmp"
};
static const char PowerLogo_50_bmp[] = {
#include "../PictureMaterial/PowerLogo_50_bmp"
};
static const char PowerLogo_75_bmp[] = {
#include "../PictureMaterial/PowerLogo_75_bmp"
};
static const char PowerLogo_100_bmp[] = {
#include "../PictureMaterial/PowerLogo_100_bmp"
};
static const char PowerLow_bmp[] = {
#include "../PictureMaterial/PowerLow_bmp"
};
static const char Shutdown_bmp[] = {
#include "../PictureMaterial/Shutdown_bmp"
};

static const char AgainCheck_bmp[] = {
#include "../PictureMaterial/AgainCheck_bmp"
};
static const char HandLeft_bmp[] = {
#include "../PictureMaterial/HandLeft_bmp"
};
static const char UpdateParamError_bmp[] = {
#include "../PictureMaterial/UpdateParamError_bmp"
};
static const char UpdateIllegalAccess_bmp[] = {
#include "../PictureMaterial/UpdateIllegalAccess_bmp"
};
static const char UpdateException_bmp[] = {
#include "../PictureMaterial/UpdateException_bmp"
};
static const char UpdateFail_bmp[] = {
#include "../PictureMaterial/UpdateFail_bmp"
};
static const char BackHome_bmp[] = {
#include "../PictureMaterial/BackHome_bmp"
};

static bool_t WifiConnectedflag = false;   //wifi链接标志
static bool_t PowerLowerFlag = false;
static char GuiInfoPower = 25;//电量占比 0~100

static unsigned int gRtcTickSecOffset = 0;
static unsigned int gRtcTickSecTimestamp = 0;

static enum UpdateError update_error=Error_FAIL;
void SetUpdateError(enum UpdateError inf)
{
    update_error=inf;
}
struct WinTime GuiInfoTime = {
        .year = 2019,
        .mon = 10,
        .mday = 10,
        .weeks = 4,
        .hour = 12,
        .minute = 20,
        .sec = 30,
};

//extern UINT32 usb_is_pluged(void);
//extern void deep_sleep(void);

unsigned int GetRtcTickSecTimestamp(void)
{
    return gRtcTickSecTimestamp;
}

/**
 * 设置电量占比
 */
bool_t Set_MainWinPower(char Percentage)
{
    if(Percentage>100)
        return false;
    GuiInfoPower = Percentage;
//    PowerProgressbar.Pos = GuiInfoPower;
    printf("IsCharge()= %d, Percentage = %d\r\n",IsCharge(),Percentage);
    if(GuiInfoPower == 1 && PowerLowerFlag==false && IsCharge() == false)
    {
        PowerLowerFlag = true;
//        Power_Lower();
    }
    if(Get_SelectionWinType() != Win_WifiPassInput)
        Refresh_GuiWin();

    return true;
}

bool_t Get_PowerFlag(void)
{
    return PowerLowerFlag;
}
void Set_PowerFlag(bool_t statue)
{
    PowerLowerFlag = statue;
}

/*****************************************************************************
 *  功能：更新wifi状态
 *  参数：flag ：false wifi未连接  true wifi已连接
 *  返回：
 *****************************************************************************/
bool_t Wifi_Connectedflag(u8 flag)
{
    WifiConnectedflag = flag;
    if(Get_SelectionWinType() != Win_WifiPassInput)
        Refresh_GuiWin();
    return true;
}

bool_t Get_Wifi_Connectedflag()
{
    return WifiConnectedflag;
}

const char * Get_BmpBuf(enum Bmptype type)
{
    switch (type)
    {
       case BMP_MainInterface_bmp   :
          return MainInterface_bmp;
//        case BMP_Background_bmp   :
//            return Background_bmp;
        case BMP_WIFI             :
            if(WifiConnectedflag)
            {
                return Wifi01_bmp;
            }else{
                return Wifi02_bmp;
            }
            break;
        case BMP_WIFI_SIGNAL1 :
             return Wifi1_bmp;
        case BMP_WIFI_SIGNAL2 :
             return Wifi2_bmp;
        case BMP_WIFI_SIGNAL3 :
             return Wifi3_bmp;
        case BMP_WIFI_SIGNAL4 :
             return Wifi4_bmp;
        case BMP_Setting_bmp :
             return Setting_bmp;
        case BMP_FirmwareUpdate_bmp :
             return FirmwareUpdate_bmp;
             break;
        case BMP_NetSetting_bmp  :
             return NetSetting_bmp;
             break;

        case BMP_ClickMedical_bmp :
             return ClickMedical_bmp;
             break;
        case BMP_Quantum_bmp   :
             return Quantum_bmp;
        case BMP_Djyos_bmp   :
           return Djyos_bmp;
        case BMP_Click_bmp   :
           return Click_bmp;
        case BMP_LoginFail_bmp   :
           return LoginFail_bmp;
        case BMP_LoginFailText_bmp   :
           return LoginFailText_bmp;
        case BMP_ResetLogin_bmp   :
           return ResetLogin_bmp;
        case BMP_Detection1_bmp   :
           return Detection1_bmp;
        case BMP_Detection2_bmp   :
           return Detection2_bmp;
        case BMP_Detection3_bmp   :
           return Detection3_bmp;
        case BMP_ExitLogin_bmp   :
           return ExitLogin_bmp;
        case BMP_Begin_bmp   :
           return Begin_bmp;
        case BMP_Stop_bmp   :
           return Stop_bmp;
        case BMP_0_bmp   :
           return Dis0_bmp;
        case BMP_1_bmp   :
           return Dis1_bmp;
        case BMP_2_bmp   :
           return Dis2_bmp;
        case BMP_3_bmp   :
           return Dis3_bmp;
        case BMP_4_bmp   :
           return Dis4_bmp;
        case BMP_5_bmp   :
           return Dis5_bmp;
        case BMP_6_bmp   :
           return Dis6_bmp;
        case BMP_7_bmp   :
           return Dis7_bmp;
        case BMP_8_bmp   :
           return Dis8_bmp;
        case BMP_9_bmp   :
           return Dis9_bmp;
        case BMP_10_bmp   :
           return Dis10_bmp;
        case BMP_back_bmp   :
           return back_bmp;
        case BMP_Success_bmp   :
           return Success_bmp;
        case BMP_SuccessText_bmp   :
           return SuccessText_bmp;
        case BMP_Fail_bmp   :
           return Fail_bmp;
        case BMP_FailText_bmp   :
           return FailText_bmp;
        case BMP_Lock_bmp   :
           return Lock_bmp;
        case BMP_WifFinish_bmp   :
           return WifFinish_bmp;
        case BMP_ResetConfig_bmp   :
           return ResetConfig_bmp;
        case BMP_NetConnecting1_bmp   :
           return NetConnecting1_bmp;
        case BMP_NetConnecting2_bmp   :
           return NetConnecting2_bmp;
        case BMP_NetConnectingText_bmp   :
           return NetConnectingText_bmp;
        case BMP_NetSuccess_bmp   :
           return NetSuccess_bmp;
        case BMP_NetFail_bmp   :
           return NetkFail_bmp;
        case BMP_NetFailText_bmp   :
           return NetFailText_bmp;
        case BMP_Finish_bmp   :
           return Finish_bmp;
//        case BMP_UploadFail_bmp   :
//           return UploadFail_bmp;
//        case BMP_UploadFinish_bmp   :
//           return UploadFinish_bmp;
//        case BMP_UploadFinishText_bmp   :
//           return UploadFinishText_bmp;
        case BMP_Uploading1_bmp   :
           return Uploading1_bmp;
        case BMP_Uploading2_bmp   :
           return Uploading2_bmp;
        case BMP_UploadingText_bmp   :
           return UploadingText_bmp;
        case BMP_Prompt_bmp   :
           return Prompt_bmp;
        case BMP_PromptText_bmp   :
           return PromptText_bmp;
        case BMP_Confirm_bmp   :
           return Confirm_bmp;
        case BMP_Again_bmp   :
           return Again_bmp;
//        case BMP_AgainUpload_bmp   :
//           return AgainUpload_bmp;
        case BMP_MedicalText_bmp   :
           return MedicalText_bmp;
        case BMP_NetloadFail_bmp   :
           return NetloadFail_bmp;
        case BMP_NetloadFailText_bmp   :
           return NetloadFailText_bmp;
        case BMP_Netloading_bmp   :
           return Netloading_bmp;
        case BMP_NetloadingText_bmp   :
           return NetloadingText_bmp;

        case BMP_Ok_bmp:
            return Ok_bmp;
        case BMP_Cancel_bmp:
            return Cancel_bmp;
        case BMP_Powering_bmp:
            return Powering_bmp;
        case BMP_PowerLogo_bmp   :
            if(current_power() >= 100)
                return PowerLogo_100_bmp;
            else if(current_power() >= 75)
                return PowerLogo_75_bmp;
            else if(current_power() >= 50)
                return PowerLogo_50_bmp;
            else if(current_power() >= 25)
                return PowerLogo_25_bmp;
            else
                return PowerLogo_1_bmp;
        case BMP_PowerLow_bmp:
            return PowerLow_bmp;
        case BMP_Shutdown_bmp:
            return Shutdown_bmp;
        case BMP_AgainCheck_bmp:
            return AgainCheck_bmp;
        case BMP_HandLeft_bmp:
            return HandLeft_bmp;
        case BMP_ErrorInformation_bmp:
            if(update_error == Error_FAIL)
                return UpdateFail_bmp;
            else if(update_error == Error_PARAM_ERROR)
                return UpdateParamError_bmp;
            else if(update_error == Error_ILLEGAL_ACCESS)
                return UpdateIllegalAccess_bmp;
            else
                return UpdateException_bmp;
        case BMP_BackHome_bmp:
            return BackHome_bmp;
        default:      break;
    }
    return NULL;
}

void DispMainInterface(unsigned char *pic)
{
    struct RopGroup RopCode = (struct RopGroup){ 0, 0, 0, CN_R2_COPYPEN, 0, 0, 0  };
   struct GkWinObj *desktop;
   struct RectBitmap   bitmap;
   desktop = GK_GetDesktop(CFG_DISPLAY_NAME);
   bitmap.bm_bits = pic+70;
   bitmap.linebytes = CFG_LCD_XSIZE*2;
   bitmap.PixelFormat = CN_SYS_PF_RGB565;
   bitmap.ExColor = 0;
   bitmap.height=CFG_LCD_YSIZE;
   bitmap.width=CFG_LCD_XSIZE;
   bitmap.reversal = true;
   GK_DrawBitMap(desktop,&bitmap,0,0,0,RopCode,100000);
   GK_SyncShow(100000);
}

void MainInterface(void)
{
//    void DispMainInterface(unsigned char *pic);
   DispMainInterface((unsigned char *)Get_BmpBuf(BMP_MainInterface_bmp));
}

//获取窗口界面时间
struct WinTime Win_GetTime()
{
    return GuiInfoTime;
}
/**
 * 设置界面时间
 */
bool_t Win_SetTime(struct WinTime time)
{
    if((time.hour<24)&&(time.minute<60))
    {
//        GuiInfoTime = time;
//        sprintf(Timetxtbuf,"%02d:%02d",time.hour,time.minute);
        GuiInfoTime.hour = time.hour;
        GuiInfoTime.minute = time.minute;
//        if(Get_SelectionWinType() != Win_WifiPassInput)
        Refresh_GuiWin();
        return true;
    }
    return false;
}

int SetClock(int hour, int min)
{
//    struct WinTime t;
    GuiInfoTime.hour = hour;
    GuiInfoTime.minute = min;
    return Win_SetTime(GuiInfoTime);
}

static u8 is_leap_year(int year)
{
    if(
            (((year%4) == 0) && ((year%100)!=0))
        ||  ((year%400)==0)
      )
    {
        return 1;
    }

    return 0;
}



unsigned int calc_sec1970(int Y, u8 M, u8 D, u8 h, u8 m, u8 s)
{
    int i = 0;
    unsigned int sec = 0;
    int days[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    for(i = 1970; i < Y; i++)
    {
        if(is_leap_year(i))
            sec += 366 * 24 * 60 * 60;
        else
            sec += 365 * 24 * 60 * 60;
    }
    for(i = 1; i < M; i++)
    {
        if(i == 2 && is_leap_year(Y))
        {
            sec += 29 * 24 * 60 * 60;
        }else{
            sec += days[i] * 24 * 60 * 60;
        }
    }
    sec += (D-1) * 24 * 60 * 60;
    sec += h * 60 * 60 + m * 60 + s;
    sec = sec - (8 * 60 * 60);
    return sec;
}

void synDateWithHttpPkg(char* buf)
{
    char *strMonth[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    char *strMon[7] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
    struct WinTime time;
    char str[5] = {0};
    char strMonthBuf[5] = {0};
    char strMonBuf[5] = {0};

    if(buf == NULL)
    {
        return;
    }
    printf("value = %s\r\n", buf);

    memcpy(strMonBuf, buf, 3);
    printf("str = %s\r\n",strMonBuf);

    memset(str, 0, strlen(str));
    memcpy(str, buf+12, 4);
    time.year = atoi(str);
    printf("time.year = %d\r\n",time.year);

    memcpy(strMonthBuf, buf+8, 3);
    printf("strMon = %s\r\n",strMonthBuf);

    memset(str, 0, strlen(str));
    memcpy(str, buf+5, 2);
    time.mday = atoi(str);
    printf("time.mday = %d\r\n",time.mday);

    memset(str, 0, strlen(str));
    memcpy(str, buf+17, 2);
    time.hour = atoi(str)+8;
    printf("time.hour = %d\r\n",time.hour);

    memset(str, 0, strlen(str));
    memcpy(str, buf+20, 2);
    time.minute = atoi(str);
    printf("time.minute = %d\r\n",time.minute);

    memset(str, 0, strlen(str));
    memcpy(str, buf+23, 2);
    time.sec = atoi(str);
    printf("time.sec = %d\r\n",time.sec);

    for(time.mon = 1; time.mon <= 12; time.mon++)
    {
        if(strcmp(strMonth[time.mon - 1], strMonthBuf) == 0)
            break;
    }
    for(time.weeks = 1; time.weeks <= 7; time.weeks++)
    {
        if(strcmp(strMon[time.weeks - 1], strMonBuf) == 0)
            break;
    }
    GuiInfoTime.year = time.year;
    GuiInfoTime.mon = time.mon;
    GuiInfoTime.mday = time.mday;
    GuiInfoTime.weeks = time.weeks;
    GuiInfoTime.hour = time.hour;
    GuiInfoTime.minute = time.minute;

    gRtcTickSecOffset = DJY_GetSysTime()/1000000;
    gRtcTickSecTimestamp=calc_sec1970(time.year,time.mon,time.mday,time.hour,time.minute,time.sec);

//    Win_SetTime(time);
}

struct tm *oss_gmtime(const time_t *time, int time_zone)
{
    static struct  tm result;
    s64 temp_time;
    if(time == NULL)
    {
        extern s64 __Rtc_Time(s64 *rtctime);
        temp_time = __Rtc_Time(NULL);
        temp_time += ((s64)8*3600);
    }
    else
    {
        temp_time = *time + ((s64)time_zone*3600);
    }
    return Time_LocalTime_r(&temp_time, &result);
}

int GetTimeHourMinute(int *hour, int *min)
{
    unsigned int timestamp_out = 0;
    unsigned int cur_tick_sec  = 0;
    struct tm *ptm = 0;
    cur_tick_sec = DJY_GetSysTime()/1000000;
    timestamp_out  = gRtcTickSecTimestamp + cur_tick_sec - gRtcTickSecOffset;
    time_t t = timestamp_out;
    ptm = oss_gmtime(&t, 8);
    *hour = ptm->tm_hour;
    *min = ptm->tm_min;
    //printf("---->hour=%d, min=%d!\r\n", *hour, *min);
    //strftime(GMT, sizeof(GMT), "%a, %d %b %Y %H:%M:%S GMT", oss_gmtime(&t, 0));
    //printf("GetTimeHourMinute: %d, TimeStamp: %d, RTC TIME: %d!\r\n", gRtcTickSecOffset, gRtcTickSecTimestamp, timestamp_out);
    return 0;
}

void sdcard_power_off(void)
{
    u32 param;

//    sdcard_uninitialize();

    param = BLK_BIT_MIC_QSPI_RAM_OR_FLASH;
    sddev_control(SCTRL_DEV_NAME, CMD_SCTRL_BLK_DISABLE, &param);
//    param = QSPI_IO_1_8V;
//    sddev_control(SCTRL_DEV_NAME, CMD_QSPI_IO_VOLTAGE, &param);
    param = PSRAM_VDD_1_8V;
    sddev_control(SCTRL_DEV_NAME, CMD_QSPI_VDDRAM_VOLTAGE, &param);
}

//关机
void Set_ShoutDown()
{
    printf("%s ,%d\r\n",__func__,__LINE__);
    sdcard_power_off();
    CloseScreen_Speaker();
    DJY_DelayUs(100000);
    LP_SetSleepLevel(CN_SLEEP_L4);
}



