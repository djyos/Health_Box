//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include <upgrade.h>
#include "project_config.h"
//extern char start_user_loading;
//控件ID编号
enum UpdateId{
    Update_BACKGROUND, //背景
    Update_LOGO,      //检测到跟新
    Update_Text,
    Update_Text2,
    Update_Immediately,      //立即更新
    Update_Later,            //以后再说

    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO UpdateCfgTab[ENUM_MAXNUM] =
{
    [Update_BACKGROUND] = {
        .position = {50,33,50+220,33+189},
        .name = "MainWin",
        .type = type_background,
        .userParam = RGB(255,254,168),
    },

    [Update_LOGO] = {
        .position = {50,50,50+220,50+16},
        .name = "升级设备",
        .type = widget_type_job,
        .userParam = RGB(28,32,42),
    },

    [Update_Text] = {
        .position = {50,90,50+220,90+16},
        .name = "当前版本号",
        .type = widget_type_text,
        .userParam = 1 ,// RGB(115,115,115),
    },
    [Update_Text2] = {
        .position = {50,140,50+220,140+16},
        .name = "可升级版本号",
        .type = widget_type_text,
        .userParam = 2,//RGB(115,115,115),
    },
    [Update_Immediately] = {
        .position ={78,188,78+70,188+25},
        .name = "立即更新",
        .type = widget_type_button,
        .userParam = RGB(0,255,0),
    },

    [Update_Later] = {
        .position ={178,188,178+70,188+25},
        .name = "以后再说",
        .type = widget_type_button,
        .userParam = RGB(206,206,206),
    },
};

//按钮控件创建函数
static bool_t  UpdateButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
//    const char *course;
//    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        const  struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,UpdateCfgTab[Update_Immediately].name))
        {

            GDD_SetFillColor(hdc,buttoninfo->userParam);
            GDD_FillRect(hdc,&rc);

            GDD_SetTextColor(hdc,RGB(28,32,42));
            if(Get_update_flag()==ReadyUpdate){
                GDD_DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
            }else{
                GDD_DrawText(hdc,"确认",-1,&rc,DT_VCENTER|DT_CENTER);
            }
        }
        if(0==strcmp(buttoninfo->name,UpdateCfgTab[Update_Later].name))
        {
            GDD_SetFillColor(hdc,buttoninfo->userParam);
            GDD_FillRect(hdc,&rc);

            if(Get_update_flag()==ReadyUpdate){
                GDD_DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
            }else{
                GDD_DrawText(hdc,"取消",-1,&rc,DT_VCENTER|DT_CENTER);
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}
/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_Update(struct WindowMsg *pMsg)
{
//    RECT rc;
    HDC  hdc;
    HWND hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
//    HWND hwndButton;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, UpdateButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

//    GDD_GetClientRect(hwnd,&rc);

//    for(int i=0;i<ENUM_MAXNUM;i++)
//    {
//        if(UpdateCfgTab[i].type == type_background)
//        {
//            rc = UpdateCfgTab[i].position;
//            GDD_SetFillColor(hdc,UpdateCfgTab[i].userParam);
//            GDD_FillRect(hdc,&rc);
//            GDD_SetDrawColor(hdc,RGB(216,216,216));
//            GDD_DrawRect(hdc,&UpdateCfgTab[i].position);
//            break;
//        }
//    }
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (UpdateCfgTab[i].type)
            {
                case  widget_type_button :
                    {
                     Widget_CreateButton(UpdateCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                             UpdateCfgTab[i].position.left, UpdateCfgTab[i].position.top,\
                             GDD_RectW(&UpdateCfgTab[i].position),GDD_RectH(&UpdateCfgTab[i].position), //按钮位置和大小
                             hwnd,i,(ptu32_t)&UpdateCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                    }
                    break;
                default:    break;
            }
        }
    }
    return true;
}

//绘制消息处函数
static bool_t HmiPaint_Update(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
//    const char * bmp ;
//    char* time;
    RECT rc;

    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (UpdateCfgTab[i].type)
            {
                case  type_background :
                    GDD_SetFillColor(hdc,UpdateCfgTab[i].userParam);
                    GDD_FillRect(hdc,&UpdateCfgTab[i].position);

                    GDD_SetDrawColor(hdc,RGB(115,115,115));
                    GDD_DrawRect(hdc,&UpdateCfgTab[i].position);
                    break;
                case  widget_type_text :
                    GDD_SetRect(&rc,UpdateCfgTab[i].position.left,UpdateCfgTab[i].position.top,\
                            GDD_RectW(&UpdateCfgTab[i].position),GDD_RectH(&UpdateCfgTab[i].position));

                    GDD_SetTextColor(hdc,RGB(115,115,115));
                    GDD_DrawText(hdc,UpdateCfgTab[i].name,-1,&rc,DT_VCENTER|DT_CENTER);
                    GDD_OffsetRect(&rc,0, 22);
                    if(UpdateCfgTab[i].userParam == 1)
                    {
                        char text_buf[20];
                        sprintf(text_buf,"%d.%d.%d",PRODUCT_VERSION_LARGE,PRODUCT_VERSION_MEDIUM,PRODUCT_VERSION_SMALL);
                        GDD_SetTextColor(hdc,RGB(216,216,216));
                        GDD_DrawText(hdc,text_buf,-1,&rc,DT_VCENTER|DT_CENTER);
                    }else{
                        if(Get_update_flag()==ReadyUpdate){
                            GDD_SetTextColor(hdc,RGB(255,143,154));
                            GDD_DrawText(hdc,Get_Upgrade_version(),-1,&rc,DT_VCENTER|DT_CENTER);

                        }else{
                            GDD_SetTextColor(hdc,RGB(216,216,216));
                            GDD_DrawText(hdc,"已是最新版本",-1,&rc,DT_VCENTER|DT_CENTER);
                        }
                    }
                    break;
                case  widget_type_job :
                    GDD_SetTextColor(hdc,UpdateCfgTab[i].userParam);
                    GDD_DrawText(hdc,UpdateCfgTab[i].name,-1,&UpdateCfgTab[i].position,DT_VCENTER|DT_CENTER);
                    break;
                default:    break;
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}
//enum WinType Get_UpdateWinType();
enum WinType HmiNotify_Settings_Update(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);
    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case Update_Immediately:
                if(Get_update_flag()==ReadyUpdate){
                    if(current_power() > 1)
                    {
                        Set_update_flag(UpgradInProgress);
                        NextUi = WIN_Update_Proram;
                    }else{
                        Set_LastWinType(Get_SelectionWinType());
                        NextUi = Win_PowerLow;
                    }
                }else{
                    NextUi = Win_NetFirmware;
                }
                break;
            case Update_Later:
//                start_user_loading = 1;
                NextUi = Win_NetFirmware;
                break;
            default:
                NextUi = WIN_Check_Update;
                break;
        }
    }
    return NextUi;
}


int Register_Check_Update()
{
    return Register_NewWin(WIN_Check_Update,HmiCreate_Update,HmiPaint_Update,HmiNotify_Settings_Update);
}








