//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include <upgrade.h>
//控件ID编号
enum Update_ProramId{
    Update_Proram_BACKGROUND, //背景
    Update_Proram_LOGO,      //检测到跟新
    Update_Proram_file,      //正在下载
    Update_InstallationProram,      //立即更新
    Update_text1,      //
    Update_text2,      //
    Update_Proram_Later,            //以后再说

    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO Update_ProramCfgTab[ENUM_MAXNUM] =
{
    [Update_Proram_BACKGROUND] = {
        .position = {50,33,50+220,33+189},
        .name = "MainWin",
        .type = type_background,
        .userParam = RGB(255,254,168),
    },

    [Update_Proram_LOGO] = {
        .position = {50,45,50+220,45+16},
        .name = "软件更新",
        .type = widget_type_job,
        .userParam = 0,
    },

    [Update_Proram_file] = {
        .position = {28+40,90,220+40-8,90+16},
        .name = "下载",
        .type = widget_type_text,
        .userParam = 0,
    },

    [Update_InstallationProram] = {
        .position ={28+40,130,220+40,130+16},
        .name = "等待安装",
        .type = widget_type_text,
        .userParam = 1,
    },

    [Update_text1] = {
        .position ={28+40,170,220+40,170+16},
        .name = "温馨提示：",
        .type = widget_type_text,
        .userParam = 2,
    },

    [Update_text2] = {
        .position ={28+40,170+22,220+40,170+22+16},
        .name = "升级安装过程中请勿断电!",
        .type = widget_type_text,
        .userParam = 3,
    },
};

int Get_Update_Precent(void);

//按钮控件创建函数
static bool_t  Update_ProramButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
#if 0
        const  struct GUIINFO *buttoninfo = GDD_GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,Update_ProramCfgTab[Update_Proram_Immediately].name))//返回
        {
            GDD_SetFillColor(hdc,buttoninfo->userParam);
//            GDD_FillRect(hdc,&rc);
            GDD_FillCircle(hdc, rc.left+40, rc.top+40,40);
            GDD_SetTextColor(hdc,RGB(255,255,255));
            rc.left-=3;
            rc.top+=20;
            GDD_DrawText(hdc,"立即",-1,&rc,DT_CENTER|DT_TOP);
            rc.top+=22;
            GDD_DrawText(hdc,"升级",-1,&rc,DT_CENTER|DT_TOP);
//            bmp = Get_BmpBuf(buttoninfo->userParam);
//            if(bmp != NULL)
//            {
//                Draw_Icon_2(hdc,8,12,12,18,bmp);
////                GDD_DrawBMP(hdc,8,6,bmp);
//            }
        }
        else if(0==strcmp(buttoninfo->name,Update_ProramCfgTab[Update_Proram_Later].name))//返回
        {
            Draw_Circle_Button(hdc,&rc,15,buttoninfo->userParam);
            GDD_SetTextColor(hdc,RGB(255,255,255));
            GDD_DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);

//            bmp = Get_BmpBuf(buttoninfo->userParam);
//            if(bmp != NULL)
//            {
//                Draw_Icon_2(hdc,8,12,12,18,bmp);
////                GDD_DrawBMP(hdc,8,6,bmp);
//            }
        }
#endif
    //  Update_ProramDisplay(0);
        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}
/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_Update_Proram(struct WindowMsg *pMsg)
{
//    RECT rc;
//    HDC  hdc;
    HWND hwnd =pMsg->hwnd;
//    hdc =GDD_BeginPaint(hwnd);
//    HWND hwndButton;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, Update_ProramButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

//    GDD_GetClientRect(hwnd,&rc);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (Update_ProramCfgTab[i].type)
        {
            case  widget_type_button :
                {
                 Widget_CreateButton(Update_ProramCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                         Update_ProramCfgTab[i].position.left, Update_ProramCfgTab[i].position.top,\
                         GDD_RectW(&Update_ProramCfgTab[i].position),GDD_RectH(&Update_ProramCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&Update_ProramCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                }
                break;
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_Update_Proram(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
//    const char * bmp ;
//    char* time;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (Update_ProramCfgTab[i].type)
            {
                case  type_background :
                        GDD_SetFillColor(hdc,Update_ProramCfgTab[i].userParam);
                        GDD_FillRect(hdc,&Update_ProramCfgTab[i].position);

                        GDD_SetDrawColor(hdc,RGB(115,115,115));
                        GDD_DrawRect(hdc,&Update_ProramCfgTab[i].position);
                        break;

                case  widget_type_text :
                    if(Update_ProramCfgTab[i].userParam == 0)
                    {
                        char text_buf[100];
                        sprintf(text_buf,"下载进度：%d%%",Get_Update_Precent());
                        GDD_SetTextColor(hdc,RGB(28,32,42));
                        GDD_DrawText(hdc,text_buf,-1,&Update_ProramCfgTab[i].position,DT_VCENTER|DT_LEFT);
                        if(Get_Update_Precent()==100)
                        {
                            GDD_SetTextColor(hdc,RGB(3,252,36));
                            GDD_DrawText(hdc,"完成",-1,&Update_ProramCfgTab[i].position,DT_VCENTER|DT_RIGHT);
                        }
                    }else if(Update_ProramCfgTab[i].userParam == 1){
                        if(Get_Update_Precent()==100)
                        {
                            if(get_update_result_flag() == UpdateResult_success)
                            {
                                GDD_SetTextColor(hdc,RGB(255,0,0));
                                GDD_DrawText(hdc,"下载成功。系统即将重启。\r\n请勿操作设备等待几分钟。",-1,&Update_ProramCfgTab[i].position,DT_VCENTER|DT_LEFT);
                            }
                            else if(get_update_result_flag() == UpdateResult_check_fail)
                            {
                                GDD_SetTextColor(hdc,RGB(255,0,0));
                                GDD_DrawText(hdc,"下载文件错误。\r\n系统即将重启。",-1,&Update_ProramCfgTab[i].position,DT_VCENTER|DT_LEFT);
                            }
                        }else{
                            if(get_update_result_flag() == UpdateResult_network_download_fail)
                            {
                                GDD_SetTextColor(hdc,RGB(255,0,0));
                                GDD_DrawText(hdc,"下载失败，系统即将重启。",-1,&Update_ProramCfgTab[i].position,DT_VCENTER|DT_LEFT);
                            }
                            else
                            {
                                GDD_SetTextColor(hdc,RGB(28,32,42));
                                GDD_DrawText(hdc,Update_ProramCfgTab[i].name,-1,&Update_ProramCfgTab[i].position,DT_VCENTER|DT_LEFT);
                            }
                        }
                    }else{
                        GDD_SetTextColor(hdc,RGB(28,32,42));
                        GDD_DrawText(hdc,Update_ProramCfgTab[i].name,-1,&Update_ProramCfgTab[i].position,DT_VCENTER|DT_LEFT);
                    }
                    break;
                case  widget_type_job :
                    GDD_SetTextColor(hdc,RGB(28,32,42));
                    GDD_DrawText(hdc,Update_ProramCfgTab[i].name,-1,&Update_ProramCfgTab[i].position,DT_VCENTER|DT_CENTER);
                    break;
                default:    break;
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

enum WinType HmiNotify_Update_Proram(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);
    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
//            case Update_Proram_Immediately:
//                web_upgrade_firmware(0);
//                NextUi = WIN_Settings;
//                break;
            case Update_Proram_Later:
                NextUi = WIN_Main_WIN;
                break;
            default:
                NextUi = WIN_Update_Proram;
                break;
        }
    }
    return NextUi;
}


int Register_Update_Proram()
{
    return Register_NewWin(WIN_Update_Proram,HmiCreate_Update_Proram,HmiPaint_Update_Proram,HmiNotify_Update_Proram);
}








