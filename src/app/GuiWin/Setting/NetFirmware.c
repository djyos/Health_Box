//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include "../inc/ServerEvent.h"
#include "qrencode.h"
#include <upgrade.h>

//界面元素定义
enum NetFirmware{
    NetFirmware_BACKGROUND, //背景
    NetFirmware_BACK,
    NetFirmware_Wifi,
    NetFirmware_POWER_LOGO,
    NetFirmware_Logo,
    NetFirmware_Line,
    NetFirmware_NetSet,    //网络设置
    NetFirmware_Firmware,  //固件升级
    NetFirmware_devid,
    NetFirmware_FullTest,  //隐形测试按钮
    ENUM_MAXNUM,//总数量
};

//==================================config======================================
static struct GUIINFO NetFirmwareCfgTab[ENUM_MAXNUM] =
{
    [NetFirmware_BACKGROUND] = {
        .position = {0,0,320,240},
        .name = "Background",
        .type = type_background,
        .userParam = RGB(255,253,235),
    },
    [NetFirmware_BACK] = {
        .position ={0,0,80,30},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_back_bmp,
    },
    [NetFirmware_Wifi] = {
        .position = {265,8,265+17,8+19},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },
    [NetFirmware_POWER_LOGO] = {
        .position = {233,12,233+23,12+13},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },
    [NetFirmware_Logo] = {
        .position = {287,5,287+25,5+24},
        .name = "量子",
        .type = widget_type_picture,
        .userParam = BMP_Quantum_bmp,
    },
    [NetFirmware_Line] = {
        .position = {8,31,8+303,31+2},
        .name = "DividingLine",
        .type = widget_type_Line,
        .userParam = RGB(163,162,154),//划线的颜色
    },
    [NetFirmware_NetSet] = {
        .position ={48,80,48+86,80+86},
        .name = "网络设置",
        .type = widget_type_button,
        .userParam = BMP_NetSetting_bmp,
    },
    [NetFirmware_Firmware] = {
        .position ={190,80,190+86,80+86},
        .name = "固件更新",
        .type = widget_type_button,
        .userParam = BMP_FirmwareUpdate_bmp,
    },
    [NetFirmware_devid] = {
        .position = {0,200,320,200+22},
        .name = "设备号:",
        .type = widget_type_text,
        .userParam = RGB(216,216,216),
    },
    [NetFirmware_FullTest] = {
        .position ={280,215,320,240},
        .name = "全功能测试",
        .type = widget_type_button,
        .userParam = RGB(255,253,235),
    },
};

//按钮控件创建函数
static bool_t  NetFirmwareButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;

    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        const struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);

        GDD_SetFillColor(hdc,RGB(255,253,235));
        GDD_FillRect(hdc,&rc);

        if(0==strcmp(buttoninfo->name,NetFirmwareCfgTab[NetFirmware_BACK].name))
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                GDD_DrawBMP(hdc,8,10,bmp);
            }
        }
        else if(0==strcmp(buttoninfo->name,NetFirmwareCfgTab[NetFirmware_NetSet].name) ||
           0==strcmp(buttoninfo->name,NetFirmwareCfgTab[NetFirmware_Firmware].name))
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                GDD_DrawBMP(hdc,1,0,bmp);
            }
        }
        else if(0==strcmp(buttoninfo->name,NetFirmwareCfgTab[NetFirmware_FullTest].name))
        {
            GDD_SetFillColor(hdc,buttoninfo->userParam);
            GDD_FillRect(hdc,&buttoninfo->position);
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}
/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_NetFirmware(struct WindowMsg *pMsg)
{
//    RECT rc0;
    HWND hwnd =pMsg->hwnd;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, NetFirmwareButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

//    GDD_GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (NetFirmwareCfgTab[i].type)
        {
            case  widget_type_button :
                {
                 Widget_CreateButton(NetFirmwareCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                         NetFirmwareCfgTab[i].position.left, NetFirmwareCfgTab[i].position.top,\
                         GDD_RectW(&NetFirmwareCfgTab[i].position),GDD_RectH(&NetFirmwareCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&NetFirmwareCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                }
                break;
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_NetFirmware(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
//    struct WinTime time;
    hwnd =pMsg->hwnd;
    char  buf[100];
    int  Get_DeviceSn(char *buf);

    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (NetFirmwareCfgTab[i].type)
            {
                case  type_background :
                    GDD_SetFillColor(hdc,NetFirmwareCfgTab[i].userParam);
                    GDD_FillRect(hdc,&NetFirmwareCfgTab[i].position);
                    break;
                case  widget_type_picture :
                    bmp = Get_BmpBuf(NetFirmwareCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,NetFirmwareCfgTab[i].position.left,\
                                NetFirmwareCfgTab[i].position.top,bmp);
                    }
                    break;
                case  widget_type_Power :
                    if(IsCharge() == true)
                    {
                        bmp = Get_BmpBuf(BMP_Powering_bmp);
                        if(bmp != NULL)
                        {
                            GDD_DrawBMP(hdc,NetFirmwareCfgTab[i].position.left-7,\
                                    NetFirmwareCfgTab[i].position.top,bmp);
                        }
                    }
                    bmp = Get_BmpBuf(NetFirmwareCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,NetFirmwareCfgTab[i].position.left,\
                                NetFirmwareCfgTab[i].position.top,bmp);
                    }
                break;

                case  widget_type_Line :
                    GDD_SetFillColor(hdc,NetFirmwareCfgTab[i].userParam);
                    GDD_FillRect(hdc,&NetFirmwareCfgTab[i].position);
                    break;
                case  widget_type_text :
                    strcpy(buf,NetFirmwareCfgTab[i].name);
                    unsigned char tmpbuf[10];
                    for(size_t j=0;j < 8; j++)
                    {
                        sprintf(tmpbuf,"%02X ",Get_Devid(j));
                        strcat(buf,tmpbuf);
                    }

                    GDD_SetTextColor(hdc,NetFirmwareCfgTab[i].userParam);
                    GDD_DrawText(hdc,buf,-1,&NetFirmwareCfgTab[i].position,DT_CENTER|DT_VCENTER);
                break;
                default:    break;
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

//子控件发来的通知消息
static enum WinType  HmiNotify_NetFirmware(struct WindowMsg *pMsg)
{
    static s64 timebak;
    static int Test_Cnt = 0;
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);

    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case  NetFirmware_BACK        :
                NextUi = WIN_Main_WIN;
                break;
            case  NetFirmware_NetSet        :
                Set_flag_ScamWifi(1);
                NextUi = Win_SelectWifi;
                break;
            case  NetFirmware_Firmware        :
//                Set_UpdateWinType(Get_SelectionWinType());
//                CheckUpdate();
                Set_update_flag(NoUpdate);
                if(Get_Wifi_Connectedflag())
                {
                    Server_SendComd(cmd_Get_Update);
                    Set_CtrlUpdateWin_Ctrl();
                    NextUi = Win_Netloading;
                }else{
                    NextUi = WIN_Check_Update;
                }
                break;
            case  NetFirmware_FullTest :
                if(Test_Cnt == 0)
                    timebak =  DJY_GetSysTime();

                Test_Cnt ++;
//                printf("Test_Cnt = %d\r\n",Test_Cnt);

                if((DJY_GetSysTime() - timebak) > 3000*mS)
                    Test_Cnt = 0;

                if(Test_Cnt >= 5)
                {
                    Test_Cnt = 0;
                    NextUi = WIN_ScreenTest;
                }else{
                    NextUi = Win_NetFirmware;
                }
                break;
            default: break;
        }
    }
    return NextUi;
}

int Register_NetFirmware()
{
    return Register_NewWin(Win_NetFirmware,HmiCreate_NetFirmware,HmiPaint_NetFirmware,HmiNotify_NetFirmware);
}

