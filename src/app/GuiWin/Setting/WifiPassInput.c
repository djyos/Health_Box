//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include "qrencode.h"
#include <app_flash.h>
#include <wifi_app.h>
#include "VoicePrompt.h"

//界面元素定义
enum WifiPassInput{
    WifiPassInput_BACKGROUND, //背景
    WifiPassInput_Line,
    WifiPassInput_BACK,      //返回
    WifiPassInput_NAME,
    WifiPassInput_WIFI,       //wifi
    WifiPassInput_POWER_LOGO,
    WifiPassInput_Logo,
    WifiPassInput_TIME,       //时间
    WifiPassInput_Text,
    WifiPassInput_InputBox,    //
//    WifiPassInput_Keyboard,    //
    WifiPassInput_Connect,    //连接
    WifiPassInput_Cancel,      //取消
    ENUM_MAXNUM,//总数量
};

//==================================config======================================
static struct GUIINFO WifiPassInputCfgTab[ENUM_MAXNUM] =
{
    [WifiPassInput_BACKGROUND] = {
        .position = {0,0,320,240},
        .name = "MainWin",
        .type = type_background,
        .userParam = RGB(255,253,235),
    },
    [WifiPassInput_Line] = {
        .position = {10,33,12+300,33},
        .name = "DividingLine",
        .type = widget_type_Line,
        .userParam = RGB(163,162,154),//划线的颜色
    },
    [WifiPassInput_BACK] = {
        .position ={0,0,80,30},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_back_bmp,
    },
    [WifiPassInput_NAME] = {
        .position = {80,10,225,33},
        .name = "账号",
        .type = widget_type_job,
        .userParam = RGB(115,115,115),
    },
    [WifiPassInput_WIFI] = {
        .position = {265,8,265+17,8+19},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },
    [WifiPassInput_POWER_LOGO] = {
        .position = {233,12,233+23,12+13},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },
    [WifiPassInput_Logo] = {
        .position = {287,5,287+25,5+24},
        .name = "量子",
        .type = widget_type_picture,
        .userParam = BMP_Quantum_bmp,
    },
    [WifiPassInput_Text] = {
        .position = {10,34,54,34+34},
        .name = "密码:",
        .type = widget_type_text,
        .userParam = RGB(185,251,255),
    },
    [WifiPassInput_InputBox] = {
        .position = {54,34,54+258,34+34},
        .name = "文本框",
        .type = widget_type_TextBox,
        .userParam = RGB(28,32,42),
    },
//    [WifiPassInput_Keyboard] = {
//        .position ={5,77,315,240},
//        .name = "虚拟键盘",
//        .type = widget_type_Keybutton,
//        .userParam = RGB(12,121,255),
//    },
    [WifiPassInput_Connect] = {
        .position ={30,190,30+90,190+40},
        .name = "连接",
        .type = widget_type_button,
        .userParam = RGB(0,255,0),
    },
    [WifiPassInput_Cancel] = {
        .position ={190,190,190+90,190+40},
        .name = "取消",
        .type = widget_type_button,
        .userParam = RGB(206,206,206),
    },
};

static HWND Button_hwnd[2];
static HWND TextBox_hwnd;
static HWND tg_VirKeyBoard;
static u8 KeyBoard_flag = 0;
static u8 KeyBoard_Name = 0;

extern  WIFI_CFG_T  LoadWifi;

extern HWND Get_WindowsHwnd();

HWND Get_TextBox_hwnd(void)
{
    return TextBox_hwnd;
}

////按钮控件创建函数
static bool_t  WifiPassInputButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        const struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,WifiPassInputCfgTab[WifiPassInput_BACK].name))//返回
        {
            GDD_SetFillColor(hdc,RGB(255,253,235));
            GDD_FillRect(hdc,&rc);

            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                GDD_DrawBMP(hdc,8,10,bmp);
            }
        }else if(0==strcmp(buttoninfo->name,WifiPassInputCfgTab[WifiPassInput_Cancel].name) \
                || 0==strcmp(buttoninfo->name,WifiPassInputCfgTab[WifiPassInput_Connect].name))
        {
            GDD_SetFillColor(hdc,buttoninfo->userParam);
            GDD_FillRect(hdc,&rc);

            GDD_SetTextColor(hdc,RGB(28,32,42));
            GDD_DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
        }
        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

static bool_t WifiPassInputTextBox_TouchDown(struct WindowMsg *pMsg)
{
    HWND hwnd;
    u32 recreat;

    if(pMsg==NULL)
        return false;
    hwnd =pMsg->hwnd;
    if(hwnd==NULL)
        return false;
    recreat = pMsg->Param1;

    printf("KeyBoard_flag = %d,recreat = %d\r\n",KeyBoard_flag,recreat);
    if(KeyBoard_flag == 0 || recreat)
    {
        if(recreat)
        {
            GDD_DestroyWindow(tg_VirKeyBoard);
        }else{
            KeyBoard_flag = 1;
        }
        if(KeyBoard_Name)
        {
            KeyBoard_Name = 0;
            tg_VirKeyBoard=Widget_CreateVirKeyBoard("虚拟键盘", WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
             5, 77,310,163,Get_WindowsHwnd(),0,NULL, NULL);   //按钮所属的父窗口，ID,附加数据
        }
        else
        {
            KeyBoard_Name = 1;
            tg_VirKeyBoard=Widget_CreateVirKeyBoard("键盘", WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
             5, 77,310,163,Get_WindowsHwnd(),0,NULL, NULL);   //按钮所属的父窗口，ID,附加数据
        }
    }
    else
    {
        KeyBoard_flag = 0;
        GDD_DestroyWindow(tg_VirKeyBoard);
        GDD_PostMessage(Get_WindowsHwnd(), MSG_REFRESH_UI, Win_WifiPassInput, 1);
        GDD_PostMessage(Button_hwnd[0], MSG_PAINT, 0, 0);
        GDD_PostMessage(Button_hwnd[1], MSG_PAINT, 0, 0);
    }
    return true;
}

/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_WifiPassInput(struct WindowMsg *pMsg)
{
//    RECT rc0;
    HWND hwnd =pMsg->hwnd;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, WifiPassInputButtonPaint},
    };

    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;


    static const struct MsgProcTable s_gMsgTableTextBox[] =
    {
            {MSG_TOUCH_DOWN, WifiPassInputTextBox_TouchDown},
    };

    static struct MsgTableLink  s_gDemoMsgLinkTextBox;

    s_gDemoMsgLinkTextBox.MsgNum = sizeof(s_gMsgTableTextBox) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkTextBox.myTable = (struct MsgProcTable *)&s_gMsgTableTextBox;

//    GDD_GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (WifiPassInputCfgTab[i].type)
        {
            case  widget_type_button :
                {
                    if(i >= WifiPassInput_Connect)
                    {
//                        printf("widget_type_button  i = %d\r\n",i);
                        Button_hwnd[i-WifiPassInput_Connect] = Widget_CreateButton(WifiPassInputCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                                WifiPassInputCfgTab[i].position.left, WifiPassInputCfgTab[i].position.top,\
                                GDD_RectW(&WifiPassInputCfgTab[i].position),GDD_RectH(&WifiPassInputCfgTab[i].position), //按钮位置和大小
                                hwnd,i,(ptu32_t)&WifiPassInputCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                    }else{
                        Widget_CreateButton(WifiPassInputCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                                WifiPassInputCfgTab[i].position.left, WifiPassInputCfgTab[i].position.top,\
                                GDD_RectW(&WifiPassInputCfgTab[i].position),GDD_RectH(&WifiPassInputCfgTab[i].position), //按钮位置和大小
                                hwnd,i,(ptu32_t)&WifiPassInputCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                    }
                }
                break;

            case  widget_type_TextBox :
                TextBox_hwnd = Widget_CreateTextBox(WifiPassInputCfgTab[i].name, WS_CHILD|BS_NORMAL| WS_UNFILL,    //按钮风格
                            WifiPassInputCfgTab[i].position.left, WifiPassInputCfgTab[i].position.top,\
                            GDD_RectW(&WifiPassInputCfgTab[i].position),GDD_RectH(&WifiPassInputCfgTab[i].position), //按钮位置和大小
                            hwnd,i,NULL, &s_gDemoMsgLinkTextBox);   //按钮所属的父窗口，ID,附加数据
                if(TextBox_hwnd)
                    GDD_SetFocusWindow(TextBox_hwnd);
                break;
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_WifiPassInput(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
//    struct WinTime time;
    hwnd =pMsg->hwnd;
//    char  buf[10];
//    int  Get_DeviceSn(char *buf);

    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (WifiPassInputCfgTab[i].type)
            {
                case  type_background :
                    GDD_SetFillColor(hdc,WifiPassInputCfgTab[i].userParam);
                    GDD_FillRect(hdc,&WifiPassInputCfgTab[i].position);
                    break;
                case  widget_type_picture :
                    bmp = Get_BmpBuf(WifiPassInputCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,WifiPassInputCfgTab[i].position.left,\
                                WifiPassInputCfgTab[i].position.top,bmp);
                    }
                    break;
                case  widget_type_Power :
                    if(IsCharge() == true)
                    {
                        bmp = Get_BmpBuf(BMP_Powering_bmp);
                        if(bmp != NULL)
                        {
                            GDD_DrawBMP(hdc,WifiPassInputCfgTab[i].position.left-7,\
                                    WifiPassInputCfgTab[i].position.top,bmp);
                        }
                    }
                    bmp = Get_BmpBuf(WifiPassInputCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,WifiPassInputCfgTab[i].position.left,\
                                WifiPassInputCfgTab[i].position.top,bmp);
                    }
                break;
                case  widget_type_Line :
                    GDD_SetDrawColor(hdc,WifiPassInputCfgTab[i].userParam);
                    GDD_DrawLine(hdc,WifiPassInputCfgTab[i].position.left,WifiPassInputCfgTab[i].position.top,\
                            WifiPassInputCfgTab[i].position.right,WifiPassInputCfgTab[i].position.bottom); //L
                    break;
                case  widget_type_text :
                    GDD_SetFillColor(hdc,WifiPassInputCfgTab[i].userParam);
                    GDD_FillRect(hdc,&WifiPassInputCfgTab[i].position);

                    GDD_SetTextColor(hdc,RGB(115,115,115));
                    GDD_DrawText(hdc,WifiPassInputCfgTab[i].name,-1,&WifiPassInputCfgTab[i].position,DT_VCENTER|DT_LEFT);
                    break;
                case  widget_type_job :
                    GDD_SetTextColor(hdc,WifiPassInputCfgTab[i].userParam);

                    size_t len = strlen(Get_RouteCurrent()) + 20;
                    char * str = malloc(len);
                    if(str != NULL)
                    {
                        Utf8_Typesetting(Get_RouteCurrent(),str,len,30);
                        struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
                        struct Charset* Charsetbak = GDD_SetCharset(hdc,myCharset);
                        GDD_DrawText(hdc,str,-1,&WifiPassInputCfgTab[i].position,DT_VCENTER|DT_LEFT);
                        GDD_SetCharset(hdc,Charsetbak);
                    }
                    if(str) free(str);
                    break;
                default:    break;
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

//子控件发来的通知消息
static enum WinType  HmiNotify_WifiPassInput(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;

    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);

    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case  WifiPassInput_BACK:
                KeyBoard_flag=0;
                TextBox_Close(TextBox_hwnd);
                NextUi = Win_SelectWifi;
                break;
            case  WifiPassInput_Cancel:
                TextBox_Close(TextBox_hwnd);
                NextUi = Win_SelectWifi;
                break;
            case  WifiPassInput_Connect:
                DjyWifi_StaDisConnect();

                strcpy(LoadWifi.WifiSsid,Get_RouteCurrent());
                Widget_TextBoxTextCtrl(TextBox_hwnd,EN_GET_TEXT,LoadWifi.WifiPassWd);
                if(strlen(LoadWifi.WifiSsid))
                {
                    Set_Start_ConnectWifi(1);
                }
                TextBox_Close(TextBox_hwnd);
                StartTimeWifiConnecting();
                NextUi = Win_WifiConnecting;
                break;
            default: break;
        }
    }
    return NextUi;
}

int Register_WifiPassInput()
{
    return Register_NewWin(Win_WifiPassInput,HmiCreate_WifiPassInput,HmiPaint_WifiPassInput,HmiNotify_WifiPassInput);
}

