//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include "qrencode.h"
#include <app_flash.h>
#include <wifi_app.h>

static u32 u32g_Selectedrouter;

extern  WIFI_CFG_T  LoadWifi;
//界面元素定义
enum SelectWifi{
    SelectWifi_BACKGROUND, //背景
    SelectWifi_Line,
    SelectWifi_WIFI,       //wifi
    SelectWifi_POWER_LOGO,
    SelectWifi_Logo,
    SelectWifi_BACK,      //返回
    SelectWifi_List1,
    SelectWifi_List2,
    SelectWifi_List3,
    SelectWifi_List4,
    SelectWifi_List5,
    SelectWifi_List6,
    ENUM_MAXNUM,//总数量
};

//==================================config======================================
static struct GUIINFO SelectWifiCfgTab[ENUM_MAXNUM] =
{
    [SelectWifi_BACKGROUND] = {
        .position = {0,0,320,240},
        .name = "Background",
        .type = type_background,
        .userParam = RGB(255,253,235),
    },
    [SelectWifi_Line] = {
        .position = {10,33,10+300,33},
        .name = "DividingLine",
        .type = widget_type_Line,
        .userParam = RGB(163,162,154),//划线的颜色
    },
    [SelectWifi_BACK] = {
        .position ={0,0,80,32},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_back_bmp,
    },
    [SelectWifi_WIFI] = {
        .position = {265,8,265+17,8+19},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },
    [SelectWifi_POWER_LOGO] = {
        .position = {233,12,233+23,12+13},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },
    [SelectWifi_Logo] = {
        .position = {287,5,287+25,5+24},
        .name = "量子",
        .type = widget_type_picture,
        .userParam = BMP_Quantum_bmp,
    },
    [SelectWifi_List1] = {
        .position ={10,34*1,310,34*2},
        .name = "wifi1",
        .type = widget_type_wifi,
        .userParam = 0,
    },
    [SelectWifi_List2] = {
        .position ={10,34*2,310,34*3},
        .name = "wifi2",
        .type = widget_type_wifi,
        .userParam = 1,
    },
    [SelectWifi_List3] = {
        .position ={10,34*3,310,34*4},
        .name = "wifi3",
        .type = widget_type_wifi,
        .userParam = 2,
    },
    [SelectWifi_List4] = {
        .position ={10,34*4,310,34*5},
        .name = "wifi4",
        .type = widget_type_wifi,
        .userParam = 3,
    },
    [SelectWifi_List5] = {
        .position ={10,34*5,310,34*6},
        .name = "wifi5",
        .type = widget_type_wifi,
        .userParam = 4,
    },
    [SelectWifi_List6] = {
        .position ={10,34*6,310,240},
        .name = "wifi6",
        .type = widget_type_wifi,
        .userParam = 5,
    },
//    [SelectWifi_AddNetwork] = {
//        .position ={90,195,110+119,195+40},
//        .name = "添加其他网络",
//        .type = widget_type_button,
//        .userParam = RGB(12,121,255),
//    },
};

struct routerCB
{
   u32 routerBase;
   char routerCurrent[32 + 1];
   HWND hwnd[6];
   u32 routermax;
};
static struct routerCB routercb = {0,{0},{NULL,NULL,NULL,NULL,NULL,NULL},0xffffffff};

u32 Get_Selectedrouter()
{
   return u32g_Selectedrouter;
}

char *Get_RouteCurrent(void)
{
    return routercb.routerCurrent;
}

void Clear_Routercb(void)
{
    routercb.hwnd[0] = NULL;
    routercb.hwnd[1] = NULL;
    routercb.hwnd[2] = NULL;
    routercb.hwnd[3] = NULL;
    routercb.hwnd[4] = NULL;
    routercb.hwnd[5] = NULL;
    routercb.routerBase = 0;
    routercb.routermax = 0xffffffff;
    strcpy(routercb.routerCurrent,"");
}
enum Bmptype RouteStrength(int para)
{
    enum Bmptype ret;
    if(para == 1){
        ret = BMP_WIFI_SIGNAL1;
    }else if(para == 2){
        ret = BMP_WIFI_SIGNAL2;
    }else if(para == 3){
        ret = BMP_WIFI_SIGNAL3;
    }else{
        ret = BMP_WIFI_SIGNAL4;
    }
    return ret;
}


//按钮控件创建函数
static bool_t  SelectWifiButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    int color_flag=0;

    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        const struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,SelectWifiCfgTab[SelectWifi_BACK].name))//返回
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
//                Draw_Icon_2(hdc,8,12,12,18,bmp);
                GDD_DrawBMP(hdc,8,10,bmp);
            }
        }else if(widget_type_wifi == buttoninfo->type)//
        {

            color_flag = buttoninfo->userParam%2;
            if(color_flag)
            {
//                color_flag = 0;
                GDD_SetFillColor(hdc,RGB(255,198,131));
                GDD_FillRect(hdc,&rc);
            }else{
//                color_flag = 1;
                GDD_SetFillColor(hdc,RGB(255,231,185));
                GDD_FillRect(hdc,&rc);
            }

//            printf("routercb.routerBase = %d,buttoninfo->userParam = %d\r\n",routercb.routerBase,buttoninfo->userParam);
            u32 routerCnt = routercb.routerBase+buttoninfo->userParam;
            if(Get_RouteCnt() > routerCnt && Get_RouteCnt() > 0)
            {
                char *routerText = Get_RouteName(routerCnt);
                if(routerText!=NULL)
                {
                    rc.left+=10;
                    GDD_SetTextColor(hdc,RGB(115,115,115) );
//                    GDD_DrawText(hdc,routerText,-1,&rc,DT_VCENTER|DT_LEFT);

                    size_t len = strlen(routerText) + 20;
                    char * str = malloc(len);
                    if(str != NULL)
                    {
                        Utf8_Typesetting(routerText,str,len,30);
                        struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
                        struct Charset* Charsetbak = GDD_SetCharset(hdc,myCharset);
                        GDD_DrawText(hdc,str,-1,&rc,DT_VCENTER|DT_LEFT);
                        GDD_SetCharset(hdc,Charsetbak);
                    }
                    if(str) free(str);

                    if(0==strcmp(routerText,LoadWifi.WifiSsid))
                    {
                        if(Get_Wifi_Connectedflag())
                        {
                            int ret = Get_RouteStrength(routerCnt);
                            bmp = Get_BmpBuf(RouteStrength(ret));
                            if(bmp != NULL)
                            {
                                Draw_Icon_2(hdc,rc.left+247,rc.top+8,18,19,bmp);
                            }

                            bmp = Get_BmpBuf(BMP_WifFinish_bmp);
                            if(bmp != NULL)
                            {
                                Draw_Icon_2(hdc,rc.left+273,rc.top+10,16,14,bmp);
                            }
                        }
                        else{
                            rc.left+=215;
                            GDD_SetTextColor(hdc,RGB(28,32,42));
                            GDD_DrawText(hdc,"正在连接",-1,&rc,DT_VCENTER|DT_LEFT);
                        }
                    }else{
                        int ret = Get_RouteStrength(routerCnt);
                        bmp = Get_BmpBuf(RouteStrength(ret));
                        if(bmp != NULL)
                        {
                            Draw_Icon_2(hdc,rc.left+247,rc.top+8,18,19,bmp);
                        }
//                        if(!GetWifi_SaveStatue(routerText)){
                            bmp = Get_BmpBuf(BMP_Lock_bmp);
                            if(bmp != NULL)
                            {
                                Draw_Icon_2(hdc,rc.left+273,rc.top+10,13,15,bmp);
                            }
//                        }
                    }
                }
            }else{
                GDD_SetFillColor(hdc,RGB(255,253,235));
                GDD_FillRect(hdc,&rc);
                GDD_SetTextColor(hdc,RGB(28,32,42));
                if(buttoninfo->userParam == 0)
                {
                    if(routercb.routerBase)
                    {
                        GDD_DrawText(hdc,"这是最后一页，请往下滑！",-1,&rc,DT_CENTER|DT_VCENTER);
                    }else{
                        GDD_DrawText(hdc,"正在扫描可用wifi，请稍等！",-1,&rc,DT_CENTER|DT_VCENTER);
                    }
                }
//                if(routercb.routermax == 0xffffffff)
//                    routercb.routermax = routercb.routerBase+buttoninfo->userParam;
            }
        }
        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

static bool_t Update_ListsRouter(void)
{
#if 1
    u8 i;
    for(i=0;i<sizeof(routercb.hwnd)/sizeof(HWND);i++)
    {
        if(routercb.hwnd[i] !=NULL)
            GDD_PostMessage(routercb.hwnd[i],MSG_PAINT,0,0);
    }
#else
    Refresh_GuiWin();
#endif
    return true;
}

void Update_RouterInfo(void)
{
    if(Get_SelectionWinType() == Win_SelectWifi)
    {
        routercb.routermax = Get_RouteCnt();
        Update_ListsRouter();
    }
}

//static bool_t  SelectWifiButton_Up(struct WindowMsg *pMsg)
//{
//    return true;
//}
//
//static bool_t  SelectWifiButton_Down(struct WindowMsg *pMsg)
//{
//    return true;
//}
/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_SelectWifi(struct WindowMsg *pMsg)
{
//    RECT rc0;
    HWND hwnd =pMsg->hwnd;
    HWND hwndButton;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, SelectWifiButtonPaint},
//            {MSG_TOUCH_UP,SelectWifiButton_Up},
//            {MSG_TOUCH_DOWN,SelectWifiButton_Down},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

//    GDD_GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (SelectWifiCfgTab[i].type)
        {
            case  widget_type_wifi :
                hwndButton = Widget_CreateButton(SelectWifiCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                             SelectWifiCfgTab[i].position.left, SelectWifiCfgTab[i].position.top,\
                             GDD_RectW(&SelectWifiCfgTab[i].position),GDD_RectH(&SelectWifiCfgTab[i].position), //按钮位置和大小
                             hwnd,i,(ptu32_t)&SelectWifiCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                if(hwndButton != NULL)
                {
                     routercb.hwnd[SelectWifiCfgTab[i].userParam] = hwndButton;
                }
                break;
            case  widget_type_button :
                {
                 Widget_CreateButton(SelectWifiCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                         SelectWifiCfgTab[i].position.left, SelectWifiCfgTab[i].position.top,\
                         GDD_RectW(&SelectWifiCfgTab[i].position),GDD_RectH(&SelectWifiCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&SelectWifiCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                }
                break;
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_SelectWifi(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
//    struct WinTime time;
    hwnd =pMsg->hwnd;
//    char  buf[10];
//    int  Get_DeviceSn(char *buf);
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (SelectWifiCfgTab[i].type)
            {
                case  type_background :
                    GDD_SetFillColor(hdc,SelectWifiCfgTab[i].userParam);
                    GDD_FillRect(hdc,&SelectWifiCfgTab[i].position);
                    break;
                case  widget_type_picture :
                    bmp = Get_BmpBuf(SelectWifiCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,SelectWifiCfgTab[i].position.left,\
                                SelectWifiCfgTab[i].position.top,bmp);
                    }
                    break;
                case  widget_type_Power :
                    if(IsCharge() == true)
                    {
                        bmp = Get_BmpBuf(BMP_Powering_bmp);
                        if(bmp != NULL)
                        {
                            GDD_DrawBMP(hdc,SelectWifiCfgTab[i].position.left-7,\
                                    SelectWifiCfgTab[i].position.top,bmp);
                        }
                    }
                    bmp = Get_BmpBuf(SelectWifiCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,SelectWifiCfgTab[i].position.left,\
                                SelectWifiCfgTab[i].position.top,bmp);
                    }
                break;

                case  widget_type_Line :
                    GDD_SetDrawColor(hdc,SelectWifiCfgTab[i].userParam);
                    GDD_DrawLine(hdc,SelectWifiCfgTab[i].position.left,SelectWifiCfgTab[i].position.top,\
                            SelectWifiCfgTab[i].position.right,SelectWifiCfgTab[i].position.bottom); //L

                    break;
                default:    break;
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

//子控件发来的通知消息
static enum WinType  HmiNotify_SelectWifi(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    s16 x,y;

    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);

    if(event==MSG_BTN_PEN_MOVE)
    {
        x= LO16(pMsg->Param2);
        y= HI16(pMsg->Param2);

        if(abs(y) > abs(x))
        {
            //printf("MSG_BTN_PEN_MOVE\r\n");
            if(y >0)
            {
                if(routercb.routermax > routercb.routerBase)
                {
                    routercb.routerBase+=sizeof(routercb.hwnd)/sizeof(HWND);
                    //printf("routercb.routerBase = %d\r\n",routercb.routerBase);
                    Update_ListsRouter();
                }
            }else{
                if(routercb.routerBase>sizeof(routercb.hwnd)/sizeof(HWND))
                    routercb.routerBase-=sizeof(routercb.hwnd)/sizeof(HWND);
                else
                    routercb.routerBase=0;
                Update_ListsRouter();
            }
        }
    }
    else if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case  SelectWifi_BACK        :
                Clear_Routercb();
                NextUi = Win_NetFirmware;
                break;
            case  SelectWifi_List1:
            case  SelectWifi_List2:
            case  SelectWifi_List3:
            case  SelectWifi_List4:
            case  SelectWifi_List5:
            case  SelectWifi_List6:
                if(routercb.routermax > (routercb.routerBase + SelectWifiCfgTab[id].userParam))
                {
                    if(Get_RouteCnt() > 0)
                    {
                        u32g_Selectedrouter = routercb.routerBase + SelectWifiCfgTab[id].userParam;
                        strcpy(routercb.routerCurrent,Get_RouteName(u32g_Selectedrouter));
                        if(0==strcmp(routercb.routerCurrent,LoadWifi.WifiSsid))
                        {
                            NextUi = Win_WifiDetail;
//                        }else if(GetWifi_SaveStatue(Get_RouteName(u32g_Selectedrouter))){
//                            NextUi = Win_WifiSave;
                        }
                        else{
                            NextUi = Win_WifiPassInput;
                        }
                    }
                }
                break;
            default: break;
        }
    }
    return NextUi;
}

int Register_SelectWifi()
{
    return Register_NewWin(Win_SelectWifi,HmiCreate_SelectWifi,HmiPaint_SelectWifi,HmiNotify_SelectWifi);
}

