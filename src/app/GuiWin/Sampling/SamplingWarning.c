//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include "project_config.h"
//控件ID编号
enum NoneId{
    SamplingWarning_BACKGROUND, //背景
    SamplingWarning_Promote,
    SamplingWarning_PromoteText,
    SamplingWarning_Confire,            //确认

    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO SamplingWarningCfgTab[ENUM_MAXNUM] =
{
    [SamplingWarning_BACKGROUND] = {
        .position = {38,37,38+244,37+178},
        .name = "background",
        .type = type_background,
        .userParam = RGB(181,255,5),
    },

    [SamplingWarning_Promote] = {
        .position = {117,57,117+87,57+20},
        .name = "温馨提示",
        .type = widget_type_picture,
        .userParam = BMP_Prompt_bmp,
    },

    [SamplingWarning_PromoteText] = {
        .position = {70,111,70+180,111+41},
        .name = "提示内容",
        .type = widget_type_picture,
        .userParam = BMP_PromptText_bmp,
    },
    [SamplingWarning_Confire] = {
        .position ={132,180,132+62,180+20},
        .name = "确定",
        .type = widget_type_button,
        .userParam = BMP_Confirm_bmp,
    },
};

//按钮控件创建函数
static bool_t  HmiNoneButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
//    const char *course;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        const  struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,SamplingWarningCfgTab[SamplingWarning_Confire].name))//返回
        {
            Draw_Circle_Button(hdc,&rc,15,buttoninfo->userParam);
            GDD_SetTextColor(hdc,RGB(255,255,255));
            GDD_DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);

            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                GDD_DrawBMP(hdc,0,0,bmp);
            }
        }

    //  NoneDisplay(0);
        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}
/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_None(struct WindowMsg *pMsg)
{
    RECT rc;
    HDC  hdc;
    HWND hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
//    HWND hwndButton;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, HmiNoneButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

    GDD_GetClientRect(hwnd,&rc);

//    for(int i=0;i<ENUM_MAXNUM;i++)
//    {
//        if(SamplingWarningCfgTab[i].type == type_background)
//        {
//            rc = SamplingWarningCfgTab[i].position;
//            GDD_SetFillColor(hdc,SamplingWarningCfgTab[i].userParam);
//            GDD_FillRect(hdc,&rc);
//            GDD_SetDrawColor(hdc,RGB(216,216,216));
//            GDD_DrawRect(hdc,&SamplingWarningCfgTab[i].position);
//            break;
//        }
//    }
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (SamplingWarningCfgTab[i].type)
            {
                case  widget_type_button :
                    {
                     Widget_CreateButton(SamplingWarningCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                             SamplingWarningCfgTab[i].position.left, SamplingWarningCfgTab[i].position.top,\
                             GDD_RectW(&SamplingWarningCfgTab[i].position),GDD_RectH(&SamplingWarningCfgTab[i].position), //按钮位置和大小
                             hwnd,i,(ptu32_t)&SamplingWarningCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                    }
                    break;
                default:    break;
            }
        }

        return true;
    }
    return false;
}

//绘制消息处函数
static bool_t HmiPaint_None(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
//    char* time;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (SamplingWarningCfgTab[i].type)
            {
                case  type_background :
                        GDD_SetFillColor(hdc,SamplingWarningCfgTab[i].userParam);
                        GDD_FillRect(hdc,&SamplingWarningCfgTab[i].position);

                        GDD_SetDrawColor(hdc,RGB(115,115,115));
                        GDD_DrawRect(hdc,&SamplingWarningCfgTab[i].position);
                        break;
                case  widget_type_picture :
                    bmp = Get_BmpBuf(SamplingWarningCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,SamplingWarningCfgTab[i].position.left,\
                                SamplingWarningCfgTab[i].position.top,bmp);
                    }
                    break;
                default:    break;
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

enum WinType HmiNotify_SamplingWarning(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);
    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case SamplingWarning_Confire:
                Set_Detect_Time();
                NextUi = Win_ReadSampling;
                break;
            default:
                NextUi = WIN_SamplingWarning;
                break;
        }
    }
    return NextUi;
}


int Register_SamplingWarning()
{
    return Register_NewWin(WIN_SamplingWarning,HmiCreate_None,HmiPaint_None,HmiNotify_SamplingWarning);
}








