//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include "qrencode.h"
#include "../inc/ServerEvent.h"

//界面元素定义
enum ErrorInformation{
    ErrorInformation_BACKGROUND, //背景
    ErrorInformation_Line,
    ErrorInformation_Logo,
    ErrorInformation_WIFI,       //wifi
    ErrorInformation_POWER_LOGO,
    ErrorInformation_Picture1,
    ErrorInformation_Line2,
    ErrorInformation_Fail,
    ErrorInformation_BACK,      //返回
    ErrorInformation_BackHome,
    ENUM_MAXNUM,//总数量
};

//==================================config======================================
static struct GUIINFO ErrorInformationCfgTab[ENUM_MAXNUM] =
{
    [ErrorInformation_BACKGROUND] = {
        .position = {0,0,320,240},
        .name = "Background",
        .type = type_background,
        .userParam = RGB(255,253,235),
    },
    [ErrorInformation_Line] = {
        .position = {8,31,8+303,31+2},
        .name = "DividingLine",
        .type = widget_type_Line,
        .userParam = RGB(163,162,154),//划线的颜色
    },
    [ErrorInformation_BACK] = {
        .position ={0,0,80,30},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_back_bmp,
    },
    [ErrorInformation_Logo] = {
        .position = {287,5,287+25,5+24},
        .name = "量子",
        .type = widget_type_picture,
        .userParam = BMP_Quantum_bmp,
    },
    [ErrorInformation_WIFI] = {
        .position = {265,8,265+17,8+19},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },
    [ErrorInformation_POWER_LOGO] = {
        .position = {233,12,233+23,12+13},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },
    [ErrorInformation_Picture1] = {
        .position = {114,50,114+92,50+125},
        .name = "picture",
        .type = widget_type_picture,
        .userParam = BMP_ErrorInformation_bmp,
    },
    [ErrorInformation_Line2] = {
        .position = {84,192,84+152,192+2},
        .name = "DividingLine",
        .type = widget_type_Line,
        .userParam = RGB(115,115,115),//划线的颜色
    },
    [ErrorInformation_Fail] = {
        .position ={54,207,54+102,207+30},
        .name = "重新上传",
        .type = widget_type_button,
        .userParam = BMP_AgainUpload_bmp,
    },
    [ErrorInformation_BackHome] = {
        .position ={164,207,164+102,207+30},
        .name = "返回首页",
        .type = widget_type_button,
        .userParam = BMP_BackHome_bmp,
    },
};

//按钮控件创建函数
static bool_t  ErrorInformationButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        const struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,ErrorInformationCfgTab[ErrorInformation_BACK].name))//返回
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                GDD_DrawBMP(hdc,8,10,bmp);
            }
        }
        else if(0==strcmp(buttoninfo->name,ErrorInformationCfgTab[ErrorInformation_Fail].name))
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                GDD_DrawBMP(hdc,0,0,bmp);
            }
        }
        else if(0==strcmp(buttoninfo->name,ErrorInformationCfgTab[ErrorInformation_BackHome].name))
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                GDD_DrawBMP(hdc,0,0,bmp);
            }
        }
        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}
/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_ErrorInformation(struct WindowMsg *pMsg)
{
//    RECT rc0;
    HWND hwnd =pMsg->hwnd;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, ErrorInformationButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

//    GDD_GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (ErrorInformationCfgTab[i].type)
        {
            case  widget_type_button :
                {
                 Widget_CreateButton(ErrorInformationCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                         ErrorInformationCfgTab[i].position.left, ErrorInformationCfgTab[i].position.top,\
                         GDD_RectW(&ErrorInformationCfgTab[i].position),GDD_RectH(&ErrorInformationCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&ErrorInformationCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                }
                break;
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_ErrorInformation(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
//    struct WinTime time;
    hwnd =pMsg->hwnd;
//    char  buf[10];
//    int  Get_DeviceSn(char *buf);
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (ErrorInformationCfgTab[i].type)
            {
                case  type_background :
                    GDD_SetFillColor(hdc,ErrorInformationCfgTab[i].userParam);
                    GDD_FillRect(hdc,&ErrorInformationCfgTab[i].position);
                    break;
                case  widget_type_picture :
                    bmp = Get_BmpBuf(ErrorInformationCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,ErrorInformationCfgTab[i].position.left,\
                                ErrorInformationCfgTab[i].position.top,bmp);
                    }
                    break;
                case  widget_type_Power :
                    if(IsCharge() == true)
                    {
                        bmp = Get_BmpBuf(BMP_Powering_bmp);
                        if(bmp != NULL)
                        {
                            GDD_DrawBMP(hdc,ErrorInformationCfgTab[i].position.left-7,\
                                    ErrorInformationCfgTab[i].position.top,bmp);
                        }
                    }
                    bmp = Get_BmpBuf(ErrorInformationCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,ErrorInformationCfgTab[i].position.left,\
                                ErrorInformationCfgTab[i].position.top,bmp);
                    }
                break;

                case  widget_type_Line :
                    GDD_SetFillColor(hdc,ErrorInformationCfgTab[i].userParam);
                    GDD_FillRect(hdc,&ErrorInformationCfgTab[i].position);

//                    GDD_SetDrawColor(hdc,RGB(90,90,90));
//                    GDD_DrawLine(hdc,ErrorInformationCfgTab[i].position.left+20,ErrorInformationCfgTab[i].position.top+163,\
//                            ErrorInformationCfgTab[i].position.right-20,ErrorInformationCfgTab[i].position.bottom+161); //L
                    break;
                default:    break;
            }
        }

//        struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
//        struct Charset* Charsetbak = GDD_SetCharset(hdc,myCharset);
//        GDD_SetTextColor(hdc,RGB(28,32,42));
//        GDD_DrawText(hdc,error_information,-1,&ErrorInformationCfgTab[ErrorInformation_BACKGROUND].position,DT_VCENTER|DT_CENTER);
//        GDD_SetCharset(hdc,Charsetbak);

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

//子控件发来的通知消息
static enum WinType  HmiNotify_ErrorInformation(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);

    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case  ErrorInformation_BACK        :
                NextUi = WIN_Main_WIN;
                break;
            case  ErrorInformation_BackHome        :
                NextUi = WIN_Main_WIN;
                break;
            case  ErrorInformation_Fail:
                Set_CtrlQrencodeWin_Time();
                Server_SendComd(cmd_Get_DeviceKey);
                NextUi = Win_Netloading;
//                NextUi = Win_DrencodeLogin;
                break;
            default: break;
        }
    }
    return NextUi;
}

int Register_ErrorInformation()
{
    return Register_NewWin(Win_ErrorInformation,HmiCreate_ErrorInformation,HmiPaint_ErrorInformation,HmiNotify_ErrorInformation);

}

