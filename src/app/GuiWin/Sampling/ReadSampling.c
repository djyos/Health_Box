//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include "qrencode.h"
#include "cpu_peri.h"

//界面元素定义
enum ReadSampling{
    ReadSampling_BACKGROUND, //背景
    ReadSampling_Line,
    ReadSampling_WIFI,       //wifi
    ReadSampling_POWER_LOGO,
    ReadSampling_BACK,      //返回
    ReadSampling_Logo,
    ReadSampling_UserInfo,
    ReadSampling_Picture,
    ReadSampling_TEXT,      //
    ReadSampling_ExitLogin,      //重新登录
    ReadSampling_BeginSampling,    //开始采样
    ENUM_MAXNUM,//总数量
};

//==================================config======================================
static struct GUIINFO ReadSamplingCfgTab[ENUM_MAXNUM] =
{
    [ReadSampling_BACKGROUND] = {
        .position = {0,0,320,240},
        .name = "Background",
        .type = type_background,
        .userParam = RGB(255,253,235),
    },
    [ReadSampling_Line] = {
        .position = {8,31,8+303,31+2},
        .name = "DividingLine",
        .type = widget_type_Line,
        .userParam = RGB(163,162,154),//划线的颜色
    },
    [ReadSampling_BACK] = {
        .position ={0,0,80,30},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_back_bmp,
    },
    [ReadSampling_WIFI] = {
        .position = {265,8,265+17,8+19},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },
    [ReadSampling_POWER_LOGO] = {
        .position = {233,12,233+23,12+13},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },
    [ReadSampling_Logo] = {
        .position = {287,5,287+25,5+24},
        .name = "量子",
        .type = widget_type_picture,
        .userParam = BMP_Quantum_bmp,
    },
    [ReadSampling_UserInfo] = {
        .position = {13,38,310,38+16},
        .name = "用户信息",
        .type = widget_type_text,
        .userParam = RGB(115,115,115),
    },
    [ReadSampling_Picture] = {
        .position = {85,77,85+228,77+100},
        .name = "picture",
        .type = widget_type_picture,
        .userParam = BMP_Detection1_bmp,
    },
    [ReadSampling_TEXT] = {
        .position = {10,178,310,178+16},
        .name = "请按图示做好准备！",
        .type = widget_type_text,
        .userParam = RGB(255,65,106),
    },
    [ReadSampling_ExitLogin] = {
        .position ={34,207,34+80,207+24},
        .name = "退出登录",
        .type = widget_type_button,
        .userParam = BMP_ExitLogin_bmp,
    },
    [ReadSampling_BeginSampling] = {
        .position ={204,207,204+82,207+24},
        .name = "开始体检",
        .type = widget_type_button,
        .userParam = BMP_Begin_bmp,
    },
};

static int DisDetectFlag=1;
static char name_buf[100];

void SetUserName(char *buf)
{
  strcpy(name_buf,buf);
}

//按钮控件创建函数
static bool_t  ReadSamplingButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        const struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);

        if(0==strcmp(buttoninfo->name,ReadSamplingCfgTab[ReadSampling_BACK].name))//返回
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                GDD_DrawBMP(hdc,8,10,bmp);
            }
        }else if(0==strcmp(buttoninfo->name,ReadSamplingCfgTab[ReadSampling_ExitLogin].name) \
                || 0==strcmp(buttoninfo->name,ReadSamplingCfgTab[ReadSampling_BeginSampling].name))
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                GDD_DrawBMP(hdc,0,0,bmp);
            }
        }
        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}
/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_ReadSampling(struct WindowMsg *pMsg)
{
//    RECT rc0;
    HWND hwnd =pMsg->hwnd;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, ReadSamplingButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

//    GDD_GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (ReadSamplingCfgTab[i].type)
        {
            case  widget_type_button :
                {
                 Widget_CreateButton(ReadSamplingCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                         ReadSamplingCfgTab[i].position.left, ReadSamplingCfgTab[i].position.top,\
                         GDD_RectW(&ReadSamplingCfgTab[i].position),GDD_RectH(&ReadSamplingCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&ReadSamplingCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                }
                break;
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_ReadSampling(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
//    struct WinTime time;
    hwnd =pMsg->hwnd;
//    char  buf[10];
//    int  Get_DeviceSn(char *buf);
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (ReadSamplingCfgTab[i].type)
            {
                case  type_background :
                    GDD_SetFillColor(hdc,ReadSamplingCfgTab[i].userParam);
                    GDD_FillRect(hdc,&ReadSamplingCfgTab[i].position);
                    break;
                case  widget_type_picture :
                    if(i == ReadSampling_Picture)
                    {
                        if(DisDetectFlag==1)
                        {
                            DisDetectFlag = 2;
                            bmp = Get_BmpBuf(ReadSamplingCfgTab[i].userParam);
                        }else if(DisDetectFlag == 2){
                            DisDetectFlag = 3;
                            bmp = Get_BmpBuf(BMP_Detection2_bmp);
                        }else{
                            DisDetectFlag =1;
                            bmp = Get_BmpBuf(BMP_Detection3_bmp);
                        }
                    }else{
                        bmp = Get_BmpBuf(ReadSamplingCfgTab[i].userParam);
                    }

                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,ReadSamplingCfgTab[i].position.left,\
                                ReadSamplingCfgTab[i].position.top,bmp);
                    }
                    break;
                case  widget_type_Power :
                    if(IsCharge() == true)
                    {
                        bmp = Get_BmpBuf(BMP_Powering_bmp);
                        if(bmp != NULL)
                        {
                            GDD_DrawBMP(hdc,ReadSamplingCfgTab[i].position.left-7,\
                                    ReadSamplingCfgTab[i].position.top,bmp);
                        }
                    }
                    bmp = Get_BmpBuf(ReadSamplingCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,ReadSamplingCfgTab[i].position.left,\
                                ReadSamplingCfgTab[i].position.top,bmp);
                    }
                break;
                case  widget_type_Line :
                    GDD_SetFillColor(hdc,ReadSamplingCfgTab[i].userParam);
                    GDD_FillRect(hdc,&ReadSamplingCfgTab[i].position);

                    GDD_SetDrawColor(hdc,RGB(115,115,115));
                    GDD_DrawLine(hdc,ReadSamplingCfgTab[i].position.left+20,ReadSamplingCfgTab[i].position.top+165,\
                            ReadSamplingCfgTab[i].position.right-20,ReadSamplingCfgTab[i].position.bottom+163); //L
                    break;
                case  widget_type_text :
                    GDD_SetTextColor(hdc,ReadSamplingCfgTab[i].userParam);
                    if(i == ReadSampling_UserInfo)
                    {
                        struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
                        struct Charset* Charsetbak = GDD_SetCharset(hdc,myCharset);
                        GDD_DrawText(hdc,name_buf,-1,&ReadSamplingCfgTab[i].position,DT_LEFT|DT_VCENTER);
                        GDD_SetCharset(hdc,Charsetbak);
                    }else
                        GDD_DrawText(hdc,ReadSamplingCfgTab[i].name,-1,&ReadSamplingCfgTab[i].position,DT_CENTER|DT_VCENTER);
                    break;
                default:    break;
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

static int Start_DevResultCallback(int id,int ret)
{
    printf("Result id %d ret :%d \r\n",id,ret);
    Set_Time_Ctrl();
    if(ret >= 0)
    {
        Set_UartRecFlag(3);
//        Ui_Uploading_Success();
    }
    else if(ret == -6)
    {
        Set_UartRecFlag(6);
    }
    else
    {
        Set_UartRecFlag(4);
//        Ui_Uploading_Fail();
    }
    return 0;
}

//子控件发来的通知消息
static enum WinType  HmiNotify_ReadSampling(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);

    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case  ReadSampling_BACK        :
                Set_CtrlQrencodeWin_Time();
                NextUi = Win_DrencodeLogin;
                break;
            case  ReadSampling_ExitLogin:
                NextUi = WIN_Main_WIN;
                break;
            case  ReadSampling_BeginSampling:
                if(djy_gpio_read(GPIO8)){
                    Set_UartProgerss(0);
                    Set_UartRecFlag(0);
					Start_DevResult(Start_DevResultCallback,0);
					Set_Time_Ctrl();
                    NextUi = Win_StartSampling;
                }else{
                    NextUi = WIN_SamplingWarning;
                }
                break;

            default: break;
        }
    }
    return NextUi;
}

int Register_ReadSampling()
{
    return Register_NewWin(Win_ReadSampling,HmiCreate_ReadSampling,HmiPaint_ReadSampling,HmiNotify_ReadSampling);

}

