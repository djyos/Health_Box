//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "board.h"
#include "app_flash.h"
#include "inc/GuiInfo.h"
#include "inc/WinSwitch.h"
#include <wifi_app.h>
#include "inc/ServerEvent.h"
#include "VoicePrompt.h"

#define CN_TIMER_CTRL               1      //wifi调出窗口时间
#define CN_TIMER_CHARGE_FLAG        40      //检测是否充电标志

HWND tg_pMainWinHwn;
//static enum WinType eng_UpdateWindow = WIN_Main_WIN;
static enum WinType eng_LastWindow= WIN_Main_WIN;
struct WinTimer *tg_pCtrlTimer;
struct WinTimer *tg_pChargeFlagTimer;
static u32 TimerCnt = 0;

enum CtrlTimerMachine
{
    CtrlWifi,
    CtrlSuccess,
    CtrlProgerssApp,
    CtrlDetectApp,
    CtrlQrencodeWin,
    CtrlUpdateWin,
    CtrlWaitExit,
};
enum CtrlTimerMachine en_StatueMachine;

enum widgeId{
    ENUM_BACKGROUND, //背景
    ENUM_WIFI,       //wifi
    ENUM_POWER_LOGO,//
    ENUM_DATA,
    ENUM_TIME,       //时间
    ENUM_SAMPLING,
    ENUM_NETWORK,
    ENUM_TEXT,
    ENUM_QUANTUM,
    ENUM_DJYOS,
    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO MainWinCfgTab[ENUM_MAXNUM] =
{
    [ENUM_BACKGROUND] = {
        .position = {0,0,320,240},
        .name = "Background",
        .type = type_background,
        .userParam = RGB(255,253,235),
    },
    [ENUM_WIFI] = {
        .position = {265,8,265+17,8+19},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },
    [ENUM_POWER_LOGO] = {
        .position = {233,12,233+23,12+13},
        .name = "power",
        .type = widget_type_Power,
        .userParam = BMP_PowerLogo_bmp,
    },
    [ENUM_DATA] = {
        .position = {8,217,8+100,217+16},
        .name = "日期",
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },
    [ENUM_TIME] = {
        .position = {108+20,217,108+20+60,217+16},
        .name = "时间",
        .type = widget_type_time,
        .userParam = Bmp_NULL,
    },
    [ENUM_SAMPLING] = {
        .position = {94,53,94+134,53+134},
        .name = "点击检测",
        .type = widget_type_button,
        .userParam = BMP_ClickMedical_bmp,
    },
    [ENUM_NETWORK] = {
        .position = {282,2,320,2+19+30},
        .name = "设置",
        .type = widget_type_button,
        .userParam = BMP_Setting_bmp,
    },
    [ENUM_TEXT] = {
        .position = {8,10,8+87,10+15},
        .name = "新冠简易版",
        .type = widget_type_picture,
        .userParam = BMP_MedicalText_bmp,
    },
    [ENUM_QUANTUM] = {
        .position = {235,214,235+25,214+24},
        .name = "量子",
        .type = widget_type_picture,
        .userParam = BMP_Quantum_bmp,
    },
    [ENUM_DJYOS] = {
        .position = {271,219,271+48,219+16},
        .name = "DJYOS",
        .type = widget_type_picture,
        .userParam = BMP_Djyos_bmp,
    },
};

extern enumServerStatue  ServerStatue_key;
extern enumServerStatue  ServerStatue_uid;
extern enumServerStatue  ServerStatue_Update;

static char time_num_buf[16];
static int UartRecFlag = 0;

bool_t CloseScreen_Statue = false;
static u32 ShutDown_TimerCnt = 0;
void Set_ShutDown_TimerCnt(u32 cnt)
{
    ShutDown_TimerCnt = cnt;
}
void Set_CloseScreen_Statue(bool_t statue)
{
    CloseScreen_Statue = statue;
}

bool_t Get_CloseScreen_Statue(void)
{
    return CloseScreen_Statue;
}

void Set_UartRecFlag(int statue)
{
    UartRecFlag = statue;
}

enum WinType Get_LastWinType()
{
    return eng_LastWindow;
}

void Set_LastWinType(enum WinType state)
{
    eng_LastWindow = state;
}

HWND Get_WindowsHwnd()
{
    return tg_pMainWinHwn;
}

//enum WinType Get_UpdateWinType()
//{
//    return eng_UpdateWindow;
//}

//void Set_UpdateWinType(enum WinType state)
//{
//    eng_UpdateWindow = state;
//}

//void Ui_Uploading_Success()
//{
//    GDD_DestroyAllChild(tg_pMainWinHwn);
//    GDD_PostMessage(tg_pMainWinHwn, MSG_REFRESH_UI, Win_SamplingSuccess, 0);
//}
//
//void Ui_Uploading_Fail()
//{
//    GDD_DestroyAllChild(tg_pMainWinHwn);
//    GDD_PostMessage(tg_pMainWinHwn, MSG_REFRESH_UI, Win_SamplingFail, 0);
//}

void PowerLower_Win()
{
    if(Get_SelectionWinType() != Win_PowerLow)
    {
        eng_LastWindow = Get_SelectionWinType();
    }
    GDD_DestroyAllChild(tg_pMainWinHwn);
    GDD_PostMessage(tg_pMainWinHwn, MSG_REFRESH_UI, Win_PowerLow, 0);
}

void ShutPower_Win(void)
{
    if(Get_SelectionWinType() != Win_PowerOff)
    {
        eng_LastWindow = Get_SelectionWinType();
    }
    GDD_DestroyAllChild(tg_pMainWinHwn);      //删除当前控件。
    GDD_PostMessage(tg_pMainWinHwn, MSG_REFRESH_UI, Win_PowerOff, 0);
}

static bool_t BmpButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    const struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);
    hdc =GDD_BeginPaint(hwnd);
    GDD_GetClientRect(hwnd,&rc);

    if(hdc)
    {
        if(0==strcmp(buttoninfo->name,MainWinCfgTab[ENUM_NETWORK].name))//返回
        {
            GDD_SetFillColor(hdc,RGB(255,253,235));
            GDD_FillRect(hdc,&rc);
            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                GDD_DrawBMP(hdc,10,5,bmp);
            }
        }else if(0==strcmp(buttoninfo->name,MainWinCfgTab[ENUM_SAMPLING].name))//返回
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            if(bmp != NULL)
            {
                GDD_DrawBMP(hdc,0,0,bmp);
            }
        }
//        bmp = Get_BmpBuf(buttoninfo->userParam);
//        if(bmp != NULL)
//        {
//            Draw_Icon(hdc,0,0,&buttoninfo->position,bmp);
//        }

        GDD_EndPaint(hwnd,hdc);
        return true;

    }
    return false;
}

/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_MainWin(struct WindowMsg *pMsg)
{
//    RECT rc0;
    HWND hwnd =pMsg->hwnd;

//    Mainlistcb.Firstmenu = true;
    //消息处理函数表
    static struct MsgProcTable s_gBmpMsgTablebutton[] =
    {
            {MSG_PAINT,BmpButtonPaint},
    };

    static struct MsgTableLink  s_gBmpDemoMsgLinkBUtton;

    s_gBmpDemoMsgLinkBUtton.MsgNum = sizeof(s_gBmpMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gBmpMsgTablebutton;

//    GDD_GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (MainWinCfgTab[i].type)
        {
            case  widget_type_button :
            {

                HWND tmpHwnd =  Widget_CreateButton(MainWinCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                                 MainWinCfgTab[i].position.left, MainWinCfgTab[i].position.top,\
                                 GDD_RectW(&MainWinCfgTab[i].position),GDD_RectH(&MainWinCfgTab[i].position), //按钮位置和大小
                                 hwnd,i,(ptu32_t)&MainWinCfgTab[i], &s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                if(tmpHwnd == NULL)
                {
                    printf("\n\r------CreateButton Mainhwnd error  !!\n\r");
                    void heap();
                    void event();
                    heap();
                    event();
                }
            }
                break;
            default:    break;
        }
    }

    return true;
}

#if 0
enum Bmptype DisplayTime(u8 time)
{
    enum Bmptype num_bmp;
    switch(time)
    {
        case 0:
            num_bmp = BMP_time_0_bmp;
        break;
        case 1:
            num_bmp = BMP_time_1_bmp;
        break;
        case 2:
            num_bmp = BMP_time_2_bmp;
        break;
        case 3:
            num_bmp = BMP_time_3_bmp;
        break;
        case 4:
            num_bmp = BMP_time_4_bmp;
        break;
        case 5:
            num_bmp = BMP_time_5_bmp;
        break;
        case 6:
            num_bmp = BMP_time_6_bmp;
        break;
        case 7:
            num_bmp = BMP_time_7_bmp;
        break;
        case 8:
            num_bmp = BMP_time_8_bmp;
        break;
        case 9:
            num_bmp = BMP_time_9_bmp;
        break;
        default:
            num_bmp = Bmp_NULL;
        break;
    }
    return num_bmp;
}

char* Get_Weeks(u8 week)
{
    char* ret;
    switch(week)
    {
        case 1:
            ret="星期一";
        break;
        case 2:
            ret="星期二";
            break;
        case 3:
            ret="星期三";
            break;
        case 4:
            ret="星期四";
            break;
        case 5:
            ret="星期五";
            break;
        case 6:
            ret="星期六";
            break;
        case 7:
            ret="星期日";
            break;
        default:
            ret="星期日";
            break;
    }
    return ret;
}
#endif
//绘制消息处函数
static bool_t HmiPaint_MainWin(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
    struct WinTime time;

    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (MainWinCfgTab[i].type)
            {
                case  type_background :
                    GDD_SetFillColor(hdc,MainWinCfgTab[i].userParam);
                    GDD_FillRect(hdc,&MainWinCfgTab[i].position);
                    break;
                case  widget_type_picture :
                    bmp = Get_BmpBuf(MainWinCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,MainWinCfgTab[i].position.left,\
                                MainWinCfgTab[i].position.top,bmp);
                    }
                break;
                case  widget_type_Power :
                    if(IsCharge() == true)
                    {
                        bmp = Get_BmpBuf(BMP_Powering_bmp);
                        if(bmp != NULL)
                        {
                            GDD_DrawBMP(hdc,MainWinCfgTab[i].position.left-7,\
                                    MainWinCfgTab[i].position.top,bmp);
                        }
                    }
                    bmp = Get_BmpBuf(MainWinCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,MainWinCfgTab[i].position.left,\
                                MainWinCfgTab[i].position.top,bmp);
                    }
                break;
                case  widget_type_time :
                    time =  Win_GetTime();

                    GDD_SetTextColor(hdc,RGB(28,32,42));
                    if(time.hour > 12)
                        sprintf(time_num_buf,"PM %02d:%02d",time.hour,time.minute);
                    else
                        sprintf(time_num_buf,"AM %02d:%02d",time.hour,time.minute);
                    GDD_DrawText(hdc,time_num_buf,-1,&MainWinCfgTab[i].position,DT_VCENTER|DT_CENTER);
                    break;
                case  widget_type_text :
                    if(i==ENUM_DATA)
                    {
                        time =  Win_GetTime();
                        GDD_SetTextColor(hdc,RGB(28,32,42));
                        sprintf(time_num_buf,"%d.%02d.%02d",time.year,time.mon,time.mday);
                        GDD_DrawText(hdc,time_num_buf,-1,&MainWinCfgTab[i].position,DT_VCENTER|DT_LEFT);
                    }else{
                        GDD_SetTextColor(hdc,MainWinCfgTab[i].userParam);
                        GDD_DrawText(hdc,MainWinCfgTab[i].name,-1,&MainWinCfgTab[i].position,DT_LEFT|DT_VCENTER);
                    }
                    break;

                default:    break;
            }
        }
    }
    GDD_EndPaint(hwnd,hdc);
    return true;

}

//  GDD_UpdateDisplay(0);
static enum WinType HmiNotify_MainWin(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
//    int minicnt = -100 ;
    u16 event =HI16(pMsg->Param1);
    u16 id =LO16(pMsg->Param1);
//    char flag = Get_ServerInfo_Flag();

    if(event == MSG_BTN_UP)
    {
        switch(id)
        {
            case ENUM_SAMPLING:
                if(Get_Wifi_Connectedflag()){
                    Set_CtrlQrencodeWin_Time();
                    Server_SendComd(cmd_Get_DeviceKey);
                    NextUi = Win_Netloading;
//                    NextUi = Win_DrencodeLogin;
                }else{
                    NextUi = Win_NetworkFail;
                }
                break;
            case ENUM_NETWORK:
                NextUi = Win_NetFirmware;
                break;
            default:
                break;
        }
    }
    return NextUi;
}

void StartTimeWifiConnecting()
{
    TimerCnt = 0;
    en_StatueMachine = CtrlWifi;
    GDD_ResetTimer(tg_pCtrlTimer,100);
}

void Set_Time_Ctrl()
{
    TimerCnt = 0;
    en_StatueMachine = CtrlProgerssApp;
    GDD_ResetTimer(tg_pCtrlTimer,1000);
}

void Set_CtrlUpdateWin_Ctrl()
{
    TimerCnt = 0;
    en_StatueMachine = CtrlUpdateWin;
    GDD_ResetTimer(tg_pCtrlTimer,500);
}

void Set_Detect_Time()
{
    TimerCnt = 0;
    en_StatueMachine = CtrlDetectApp;
    GDD_ResetTimer(tg_pCtrlTimer,500);
}

void Set_CtrlQrencodeWin_Time()
{
    TimerCnt = 0;
    en_StatueMachine = CtrlQrencodeWin;
    GDD_ResetTimer(tg_pCtrlTimer,500);
}

static bool_t HmiTimer(struct WindowMsg *pMsg)
{
    HWND hwnd;
    struct WinTimer *pTmr;
    u16 TmrId;
    hwnd=pMsg->hwnd;
    TmrId=pMsg->Param1;
    pTmr=GDD_FindTimer(hwnd,TmrId);
//    static bool_t last_charge_flag = false, now_charge_flag;

    if(TmrId == CN_TIMER_CTRL)
    {
        if(en_StatueMachine == CtrlWifi)
        {
            if(mhdr_get_station_status() == MSG_GOT_IP)
            {
                TimerCnt = 0;
                en_StatueMachine = CtrlSuccess;
                GDD_ResetTimer(pTmr,100);
                GDD_DestroyAllChild(hwnd);
                GDD_PostMessage(hwnd, MSG_REFRESH_UI,Win_NetworkSuccess, 0);
//            }else if(mhdr_get_station_status() == MSG_PASSWD_WRONG){
//                DjyWifi_StaDisConnect();
//                TimerCnt = 0;
//                GDD_StopTimer(tg_pCtrlTimer);
//                GDD_DestroyAllChild(hwnd);
//                GDD_PostMessage(hwnd, MSG_REFRESH_UI,Win_WifiPassError, 0);
            }else if(TimerCnt > 40){
                TimerCnt = 0;
                VoicePrompt(wifi_connect_error);
                GDD_StopTimer(tg_pCtrlTimer);
                GDD_DestroyAllChild(hwnd);
                GDD_PostMessage(hwnd, MSG_REFRESH_UI,Win_NetworkFail, 0);
            }else if(Get_SelectionWinType()== Win_WifiConnecting){
                TimerCnt ++;
                GDD_ResetTimer(pTmr,1000);
                GDD_DestroyAllChild(hwnd);
                GDD_PostMessage(hwnd, MSG_REFRESH_UI, Get_SelectionWinType(), 0);
            }else{
                TimerCnt = 0;
                GDD_StopTimer(tg_pCtrlTimer);
            }
        }else if(en_StatueMachine == CtrlSuccess)
        {
            if(TimerCnt > 2)
            {
                TimerCnt = 0;
                GDD_StopTimer(tg_pCtrlTimer);
                GDD_DestroyAllChild(hwnd);
                GDD_PostMessage(hwnd, MSG_REFRESH_UI,Win_SelectWifi, 0);
            }else{
                TimerCnt ++;
                GDD_ResetTimer(pTmr,1000);
            }
        }else if(en_StatueMachine == CtrlProgerssApp)
        {
            if(UartRecFlag==1 || UartRecFlag==2)
            {
                if(TimerCnt > 2)
                {
                    TimerCnt = 0;
                    if(UartRecFlag==1)
                        GDD_StopTimer(tg_pCtrlTimer);
                    else
                        GDD_ResetTimer(pTmr,1000);
                    GDD_DestroyAllChild(hwnd);
                    if(UartRecFlag==1)
                        GDD_PostMessage(hwnd, MSG_REFRESH_UI, Win_SamplingFail, 0);
                    else
                        GDD_PostMessage(hwnd, MSG_REFRESH_UI, Win_Uploading, 0);
                    UartRecFlag = 0;
                }else{
                    TimerCnt ++;
                    GDD_ResetTimer(pTmr,1000);
                    GDD_DestroyAllChild(hwnd);
                    GDD_PostMessage(hwnd, MSG_REFRESH_UI, Get_SelectionWinType(), 0);
                }
            }else if(UartRecFlag==3 || UartRecFlag==4){
                GDD_StopTimer(tg_pCtrlTimer);
                GDD_DestroyAllChild(hwnd);
                if(UartRecFlag==3)
                    GDD_PostMessage(hwnd, MSG_REFRESH_UI, Win_SamplingSuccess, 0);
                else
                    GDD_PostMessage(hwnd, MSG_REFRESH_UI, Win_SamplingFail, 0);
                UartRecFlag = 0;
            }else if(UartRecFlag == 5){
                GDD_StopTimer(tg_pCtrlTimer);
                GDD_DestroyAllChild(hwnd);
                GDD_PostMessage(hwnd, MSG_REFRESH_UI, Win_HandLeftWin, 0);
                UartRecFlag = 0;
            }else if(UartRecFlag == 6){
                GDD_StopTimer(tg_pCtrlTimer);
                GDD_DestroyAllChild(hwnd);
                GDD_PostMessage(hwnd, MSG_REFRESH_UI, Win_ErrorInformation, 0);
                UartRecFlag = 0;
            }else if(Get_SelectionWinType()==Win_StartSampling || Get_SelectionWinType()==Win_Uploading){
                GDD_ResetTimer(pTmr,1000);
                GDD_DestroyAllChild(hwnd);
                GDD_PostMessage(hwnd, MSG_REFRESH_UI, Get_SelectionWinType(), 0);
            }else{
                GDD_StopTimer(tg_pCtrlTimer);
            }
        }else if(en_StatueMachine == CtrlDetectApp){
            if(TimerCnt > 300)
            {
                TimerCnt = 0;
                GDD_StopTimer(tg_pCtrlTimer);
                GDD_DestroyAllChild(hwnd);
                GDD_PostMessage(hwnd, MSG_REFRESH_UI,WIN_Main_WIN, 0);
            }else if(Get_SelectionWinType()== Win_ReadSampling){
                TimerCnt ++;
                GDD_ResetTimer(pTmr,1000);
                GDD_DestroyAllChild(hwnd);
                GDD_PostMessage(hwnd, MSG_REFRESH_UI, Get_SelectionWinType(), 0);
            }else if(Get_SelectionWinType()== Win_Netloading){
                if(ServerStatue_uid == Get_Success){
                    GDD_ResetTimer(pTmr,500);
                    GDD_DestroyAllChild(hwnd);
                    GDD_PostMessage(hwnd, MSG_REFRESH_UI,Win_ReadSampling, 0);
                }else if(ServerStatue_uid == Get_Illegal){
                    GDD_StopTimer(tg_pCtrlTimer);
                    GDD_DestroyAllChild(hwnd);
                    GDD_PostMessage(hwnd, MSG_REFRESH_UI,Win_ResetLogin, 0);
                }else if(ServerStatue_uid == Get_Error){
                    GDD_StopTimer(tg_pCtrlTimer);
                    GDD_DestroyAllChild(hwnd);
                    GDD_PostMessage(hwnd, MSG_REFRESH_UI,Win_NetloadFail, 0);
                }else{
                    GDD_ResetTimer(pTmr,500);
                }
            }else{
                GDD_StopTimer(tg_pCtrlTimer);
            }
        }else if(en_StatueMachine == CtrlQrencodeWin){
            if(TimerCnt > 120)
            {
                TimerCnt = 0;
                GDD_StopTimer(tg_pCtrlTimer);
                GDD_DestroyAllChild(hwnd);
                GDD_PostMessage(hwnd, MSG_REFRESH_UI,WIN_Main_WIN, 0);
            }else if(Get_SelectionWinType()== Win_Netloading){
                if(ServerStatue_key == Get_Success || ServerStatue_key == Get_Illegal){
                    GDD_ResetTimer(pTmr,500);
                    GDD_DestroyAllChild(hwnd);
                    GDD_PostMessage(hwnd, MSG_REFRESH_UI,Win_DrencodeLogin, 0);
                }else if(ServerStatue_key == Get_Error){
                    GDD_StopTimer(tg_pCtrlTimer);
                    GDD_DestroyAllChild(hwnd);
                    GDD_PostMessage(hwnd, MSG_REFRESH_UI,Win_NetloadFail, 0);
                }else{
                    GDD_ResetTimer(pTmr,500);
                }
            }else if(Get_SelectionWinType()== Win_DrencodeLogin){
                TimerCnt ++;
                GDD_ResetTimer(pTmr,500);
            }else{
                TimerCnt = 0;
                GDD_StopTimer(tg_pCtrlTimer);
            }
        }else if(en_StatueMachine == CtrlUpdateWin){
            if(Get_SelectionWinType()==WIN_Update_Proram)
            {
                GDD_ResetTimer(pTmr,1000);
                GDD_DestroyAllChild(hwnd);
                GDD_PostMessage(hwnd, MSG_REFRESH_UI, Get_SelectionWinType(), 0);
            }else if(Get_SelectionWinType()== Win_Netloading){
                if((ServerStatue_Update == Get_Success) || (ServerStatue_Update == Get_Illegal)){
                    GDD_StopTimer(tg_pCtrlTimer);
                    GDD_DestroyAllChild(hwnd);
                    GDD_PostMessage(hwnd, MSG_REFRESH_UI,WIN_Check_Update, 0);
                }else{
                    GDD_ResetTimer(pTmr,500);
                }
            }else{
                GDD_StopTimer(tg_pCtrlTimer);
            }
        }
    }else if(TmrId == CN_TIMER_CHARGE_FLAG)
    {
//        printf("CN_TIMER_CHARGE_FLAG = 1s\r\n");
        if(djy_gpio_read(GPIO34))
            SetChargeFlag(OnCharge);
        else
            SetChargeFlag(NoCharge);

#if 1
        ShutDown_TimerCnt ++;
        if((ShutDown_TimerCnt >= 600) && (CloseScreen_Statue == false))  //无操作自动关屏
        {
            ShutDown_TimerCnt = 0;
            CloseScreen_Statue = true;
            CloseScreen_Speaker();
        }
        if((ShutDown_TimerCnt >= 1200) && (CloseScreen_Statue == true))  //关屏一段时间后关机
        {
                printf("%s ,%d\r\n",__func__,__LINE__);
                Set_ShoutDown();
        }
#endif
//        now_charge_flag = IsCharge();
//        if(now_charge_flag != last_charge_flag)
//        {
//            last_charge_flag = now_charge_flag;
//            set_cell_power();
//            GDD_DestroyAllChild(hwnd);      //删除当前控件。
//            GDD_PostMessage(hwnd, MSG_REFRESH_UI, Get_SelectionWinType(), 0);
//        }

    }
    return true;
}


bool_t Refresh_GuiWin()
{
    return Refresh_SwitchWIn(tg_pMainWinHwn);
}
void Main_GuiWin(void)
{
    Init_WinSwitch();
    int Server_EventInit();
    Server_EventInit();
    //消息处理函数表
    static struct MsgProcTable s_gBmpMsgTable[] =
    {
        {MSG_CREATE,HmiCreate_MainWin},         //主窗口创建消息
        {MSG_PAINT,HmiPaint_MainWin},           //绘制消息
        {MSG_NOTIFY,HmiNotify_EasyTalk},        //子控件发来的通知消息
        {MSG_REFRESH_UI,HmiRefresh},               //刷新窗口消息
//        {MSG_KEY_DOWN,HmiKeyDown},              //按键按下消息
//        {MSG_KEY_UP,HmiKeyUp},                  //按键按下消息
        {MSG_TIMER,HmiTimer},                   //定时器消息响应函数
//        {MSG_TOUCH_MOVE,HmiMove_MainWin},
    };

    static struct MsgTableLink  s_gBmpDemoMsgLink;

    s_gBmpDemoMsgLink.MsgNum = sizeof(s_gBmpMsgTable) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLink.myTable = (struct MsgProcTable *)&s_gBmpMsgTable;
    tg_pMainWinHwn = GDD_CreateGuiApp((char*)MainWinCfgTab[ENUM_BACKGROUND].name,
                                &s_gBmpDemoMsgLink, 0x2000, CN_WINBUF_PARENT,0);
    tg_pCtrlTimer = GDD_CreateTimer(tg_pMainWinHwn, CN_TIMER_CTRL,100);
//    tg_pCloseBrightTimer = GDD_CreateTimer(tg_pMainWinHwn, CN_TIMER_CLOSE_BRIGHT,30000);
//    tg_pPowerDownTimer = GDD_CreateTimer(tg_pMainWinHwn, CN_TIMER_POWERDOWN,60000);
    tg_pChargeFlagTimer = GDD_CreateTimer(tg_pMainWinHwn, CN_TIMER_CHARGE_FLAG,1000);
    GDD_ResetTimer(tg_pChargeFlagTimer,1000);
}


int Register_Main_WIN()
{
    return Register_NewWin(WIN_Main_WIN,HmiCreate_MainWin,HmiPaint_MainWin,HmiNotify_MainWin);
}





