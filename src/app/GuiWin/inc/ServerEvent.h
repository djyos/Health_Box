/*
 * ServerEvent.h
 *
 *  Created on: 2019年12月12日
 *      Author: c
 */

#ifndef APP_GUIWIN_INC_SERVEREVENT_H_
#define APP_GUIWIN_INC_SERVEREVENT_H_
#include "stddef.h"
#include "GuiInfo.h"

typedef enum
{
    cmd_Get_DeviceKey,//获取key
    cmd_Get_uidComputer, //获取uid、计算指令
    cmd_Get_Update, //获取软件更新信息
//    cmd_Homework_StartRecording,
//    cmd_Homework_StopRecording,
    cmd_ServerCmdmax,
}enumServerCmd;

typedef enum
{
    Get_Null,
    Get_Success,
    Get_Error,
    Get_Illegal,
}enumServerStatue;

bool_t Server_SendComd(enumServerCmd cmd);


#endif /* APP_GUIWIN_INC_SERVEREVENT_H_ */
