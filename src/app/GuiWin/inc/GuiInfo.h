/*
 * BmpInfo.h
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 */

#ifndef IBOOT_GUI_INFO_H_
#define IBOOT_GUI_INFO_H_
#include "stddef.h"
#include "gdd.h"
#ifdef __cplusplus
extern "C" {
#endif
#include "widget/gdd_progressbar.h"
#ifndef GUI_INFO_FILE_C_
//extern PROGRESSBAR_DATA PowerProgressbar;
//extern HWND Powerhwnd;
#endif
//#include "../Homework/homeWork.h"
//#include "ServerInfo.h"
//#include "utf8_text.h"
//界面元素枚举
enum widgettype
{
    type_background,
    widget_type_Setting,
    widget_type_button,
//    widget_type_Keybutton,
    widget_type_msg,
    widget_type_picture,
    widget_type_text,
    widget_type_TextBox,
    widget_type_job,
    widget_type_time,
    widget_type_Line,
    widget_type_wifi,
    widget_type_Orencode,
    widget_type_Progress,
    widget_type_Power,
};

//界面元素信息
struct GUIINFO
{
    RECT position;//坐标，大小信息
    const char* name;
    enum widgettype type;
    ptu32_t userParam;
} ;

//=================Bmp枚举=======================
enum Bmptype
{
    BMP_MainInterface_bmp,       //开机界
    BMP_WIFI,                   //wifi
    BMP_WIFI_SIGNAL1,            //wifi信号弱
    BMP_WIFI_SIGNAL2,            //wifi信号中
    BMP_WIFI_SIGNAL3,            //wifi信号强
    BMP_WIFI_SIGNAL4,            //wifi信号强
    BMP_Setting_bmp,
    BMP_ClickMedical_bmp,
    BMP_Quantum_bmp,
    BMP_Djyos_bmp,

    BMP_Click_bmp,
    BMP_LoginFail_bmp,
    BMP_LoginFailText_bmp,
    BMP_ResetLogin_bmp,

    BMP_Detection1_bmp,
    BMP_Detection2_bmp,
    BMP_Detection3_bmp,
    BMP_ExitLogin_bmp,
    BMP_Begin_bmp,

    BMP_Stop_bmp,
    BMP_0_bmp,
    BMP_1_bmp,
    BMP_2_bmp,
    BMP_3_bmp,
    BMP_4_bmp,
    BMP_5_bmp,
    BMP_6_bmp,
    BMP_7_bmp,
    BMP_8_bmp,
    BMP_9_bmp,
    BMP_10_bmp,
    BMP_NetSetting_bmp,
    BMP_FirmwareUpdate_bmp,
    BMP_back_bmp,
    BMP_StartSampling_bmp,
    BMP_Success_bmp,
    BMP_SuccessText_bmp,
    BMP_Fail_bmp,
    BMP_FailText_bmp,
    BMP_Lock_bmp,
    BMP_WifFinish_bmp,
    BMP_ResetConfig_bmp,
    BMP_NetConnecting1_bmp,
    BMP_NetConnecting2_bmp,
    BMP_NetConnectingText_bmp,
    BMP_NetSuccess_bmp,
    BMP_NetFail_bmp,
    BMP_NetFailText_bmp,
    BMP_Finish_bmp,
//    BMP_UploadFail_bmp,
//    BMP_UploadFinish_bmp,
//    BMP_UploadFinishText_bmp,
    BMP_Uploading1_bmp,
    BMP_Uploading2_bmp,
    BMP_UploadingText_bmp,
    BMP_Prompt_bmp,
    BMP_PromptText_bmp,
    BMP_Confirm_bmp,
    BMP_Again_bmp,
    BMP_AgainUpload_bmp,
    BMP_MedicalText_bmp,
    BMP_NetloadFail_bmp,
    BMP_NetloadFailText_bmp,
    BMP_Netloading_bmp,
    BMP_NetloadingText_bmp,

    BMP_Ok_bmp,
    BMP_Cancel_bmp,
    BMP_Powering_bmp,
    BMP_PowerLogo_bmp,
    BMP_PowerLow_bmp,
    BMP_Shutdown_bmp,
    BMP_AgainCheck_bmp,
    BMP_HandLeft_bmp,
    BMP_ErrorInformation_bmp,
    BMP_BackHome_bmp,
    BMP_MAXNNUM, //图标数量
    Bmp_NULL,
};

enum ChargeFlag
{
    NoCharge,           //未充电
    OnCharge,           //正在充电
};

enum UpdateError
{
    Error_FAIL,           //失败
    Error_PARAM_ERROR,    //参数错误
    Error_ILLEGAL_ACCESS, //非法访问
    Error_EXCEPTION,      //服务器异常
};

const char * Get_BmpBuf(enum Bmptype type);

struct WinTime{
  int  year;
  u8   mon;        // months  - [1,12]
  u8   mday;       // day of the month - [1,31]
  u8   weeks;       //[1,7]
  u8   hour;       // hours since midnight - [0,23]
  u8   minute;        // minutesafter the hour - [0,59]
  u8   sec;
};
bool_t Get_Wifi_Connectedflag(void);
bool_t Wifi_Connectedflag(u8 flag);

bool_t Win_SetTime(struct WinTime time);
struct WinTime Win_GetTime();

void MainInterface(void);
;
void StartTimeWifiConnecting();
void Set_Time_Ctrl();
void Set_Detect_Time();
void Set_CtrlQrencodeWin_Time();
bool_t Get_PowerFlag(void);
void Set_PowerFlag(bool_t statue);
bool_t Set_MainWinPower(char Percentage);
void Set_ShoutDown();
void PowerLower_Win();
void Set_CtrlUpdateWin_Ctrl();
void ShutPower_Win(void);
void Set_CloseScreen_Statue(bool_t statue);
bool_t Get_CloseScreen_Statue(void);
int Get_Test_Mode();
void Set_Start_UartTest(int statue);
int Get_Start_UartTest();
void Set_ShutDown_TimerCnt(u32 cnt);
#ifdef __cplusplus
}
#endif

#endif /* IBOOT_GUI_INFO_H_ */
