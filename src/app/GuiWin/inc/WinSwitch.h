/*
 * BmpInfo.h
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 */

#ifndef _GUIWIN_WINSWITCH_H_
#define _GUIWIN_WINSWITCH_H_
#include "stddef.h"

#ifdef __cplusplus
extern "C" {
#endif
enum WinType
{
    WIN_Main_WIN = 0,               //主窗口界面
    Win_NetFirmware,                //网络设置 & 固件更新
    Win_DrencodeLogin,              //扫码登录界面
    Win_ReadSampling,               //准备采样
    Win_StartSampling,              //停止采样
    Win_SamplingSuccess,            //停止成功
    Win_SamplingFail,               //停止失败
    Win_ResetLogin,                 //登录失败
    Win_SelectWifi,                 //选择连接的wifi
    Win_WifiDetail,                 //wifi详情
    Win_WifiPassInput,              //密码输入
    Win_WifiConnecting,             //wifi连接中
    Win_NetworkFail,                //网络配置失败
    Win_NetworkSuccess,             //网络配置成功
    WIN_Check_Update,               //检查更新程序
    WIN_Update_Proram,              //更新程序
    WIN_SamplingWarning,            //温馨提示手放好位置
    Win_Uploading,
//    Win_UploadSuccess,
//    Win_UploadFail,
    Win_WifiPassError,
    Win_WifiSave,
    Win_Netloading,
    Win_NetloadFail,
    Win_PowerOff,
    Win_PowerLow,
    WIN_ScreenTest,
    WIN_BgeinTest,
    Win_HandLeftWin,
    Win_ErrorInformation,
    WIN_Max,                        //有效界面总数
    WIN_NotChange,                  //不改变窗口
    WIN_Error,                      //状态错误
};
//========================界面内注册接口定义===================================
int Register_Main_WIN();
int Register_NetFirmware();
int Register_DrencodeLogin();
int Register_ReadSampling();
int Register_StartSampling();
int Register_SamplingSuccess();
int Register_SamplingFail();
int Register_ResetLogin();
int Register_SelectWifi();
int Register_WifiDetail();
int Register_WifiPassInput();
int Register_WifiConnecting();
int Register_NetworkFail();
int Register_NetworkSuccess();
int Register_Check_Update();
int Register_Update_Proram();
int Register_SamplingWarning();
int Register_Uploading();
//int Register_UploadSuccess();
//int Register_UploadFail();
int Register_WifiPassError();
int Register_WifiSave();
int Register_NetloadFail();
int Register_Netloading();
int Register_PowerOff();
int Register_PowerLow();
int Register_ScreenTest();
int Register_BgeinTest();
int Register_HandLeftWin();

int Init_WinSwitch();
enum WinType Get_SelectionWinType();
bool_t Refresh_SwitchWIn(HWND hwnd);
//enum WinType Get_UpdateWinType();

typedef bool_t (*T_HmiCreate)(struct WindowMsg *pMsg);//界面控件创建
typedef bool_t (*T_HmiPaint)(struct WindowMsg *pMsg);//界面绘制
typedef enum WinType (*T_DoMsg)(struct WindowMsg *pMsg);//界面消息响应

struct GuiWinCb
{
    T_HmiCreate HmiCreate;//界面控件创建
    T_HmiPaint HmiPaint;//界面绘制
    T_DoMsg DoMsg;//界面消息响应
};
int Register_NewWin(enum WinType id,T_HmiCreate HmiCreate,T_HmiPaint HmiPaint, T_DoMsg DoMsg);
bool_t HmiRefresh(struct WindowMsg *pMsg);
bool_t HmiNotify_EasyTalk(struct WindowMsg *pMsg);
enum WinType Get_LastWinType();

#define MSG_REFRESH_UI     (MSG_WM_USER + 0x0001)  //
#define CN_RECREAT_WIDGET       0
#define CN_ONLY_RESHOW          1
bool_t Refresh_GuiWin();
int Update_WinType(enum WinType type);
bool_t Set_CheckUpdateWin(void);
void Draw_Icon(HDC hdc,s32 x,s32 y,const RECT *rc,const char *bmp);
void Draw_Icon_2(HDC hdc,s32 x,s32 y,s32 width,s32 height,const char *bmp);
void Draw_Circle_Button(HDC hdc,RECT *rc,s32 r,u32 color);
#ifdef __cplusplus
}
#endif

#endif /* _GUIWIN_WINSWITCH_H_ */
