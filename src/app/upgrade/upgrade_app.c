#include "stdint.h"
#include "stddef.h"
#include "stdlib.h"
#include <sys/socket.h>
#include <gpio_pub.h>
#include <sys/dhcp.h>
#include <sys/dhcp.h>
#include <app_flash.h>
#include "rw_pub.h"
#include "wlan_ui_pub.h"
#include "project_config.h"
#include "board.h"
#include <Iboot_info.h>
#include <dbug.h>
#include "qspi_pub.h"
#include <wdt_pub.h>
#include "djyos.h"
#include "shell.h"
#include "int.h"
#include "../djyfs/filesystems.h"
#include <upgrade.h>

#define NETWORK_UPDATEAPP_FILE_NAME      "djyapp.bin"
#define VERIFY_BIN_LEN   272
char *update_addr = 0;
char *update_start_addr = 0;
static int mark_precent = 0;
static enum UpdateResult update_result = UpdateResult_no;
volatile enum UpdateFlag update_flag = NoUpdate;
static enum CheckVerify check_verify = VerifyNo;
extern void CPU_RestartSystem(u32 key);
extern void flash_protection_op(UINT8 mode, PROTECT_TYPE type);

int Get_Update_Precent(void)
{
    return mark_precent;
}

char ReceiveSN_Num[16];

s32 printk (const char *fmt, ...);

void *GetUpdateBufAddr();
int GetSrvUpgradeInfo(unsigned char *product_time, int prod_time_len, unsigned char *serial_num, int ser_num_len);

void set_update_result_flag(enum UpdateResult flag)
{
    update_result = flag;
}
enum UpdateResult get_update_result_flag(void)
{
    return update_result;
}

void Set_update_flag (enum UpdateFlag flag)
{
    update_flag = flag;
}

enum UpdateFlag Get_update_flag (void)
{
    return update_flag;
}

void Set_check_verify (enum CheckVerify flag)
{
    check_verify = flag;
}

enum CheckVerify Get_check_verify (void)
{
    return check_verify;
}


int network_update(u8 *buf,s32 len, int fsize, int timeout)
{
    (void)timeout;
    static int mark_pos = 0;
	u32 file_size;
    struct AppHead *apphead;
    if(update_addr == 0)
    {
        update_addr = GetUpdateBufAddr();//my_malloc(3*1024*1024);
        update_start_addr = update_addr;
        mark_pos = 0;
        printf("update_addr   = %x\r\n", (u32)update_addr);
        printf("downloading ...    ");
    }
    if(update_addr)
    {
        memcpy(update_addr,buf,len);
        update_addr += len;
//        printf("buf %02x-%02x-%02x\r\n", buf[0],buf[1],buf[2]);
        int pos = update_addr-update_start_addr;

        if (mark_precent != pos*100/fsize) {
            mark_precent = pos*100/fsize;
            printf("\b\b\b%2d%%", mark_precent);
        }
    }

    if (len > 0 ) mark_pos += len;

    if(fsize > 0 && mark_pos >= fsize)
    {
		apphead = (struct AppHead *)update_start_addr;
        file_size = update_addr - update_start_addr;
        if((apphead->app_bin_size != apphead->file_size) || (apphead->file_size != file_size))
        {
			printf("ERROR: file size diff.apphead_appbinsize = %d, apphead_filesize = %d, file_size = %d!\r\n",apphead->app_bin_size, apphead->file_size, file_size);
			Set_check_verify(VerifyFail);
		}
		else
		{
			if(XIP_AppFileCheck(update_start_addr))
			{
				printf("File check success\r\n");
				Set_check_verify(VerifySuccess);
			}
			else
			{
				printf("File check fail\r\n");
				Set_check_verify(VerifyFail);
			}
		}
    }
    return len;
}

extern u32 gc_ptIbootSize;
//用户写的iboot升级代码
bool_t Iboot_UserUpdateIboot(char *param)
{
    (void)param;
    char run_mode = Iboot_GetRunMode();
    char Iboot_Update[MutualPathLen];
    char *psram_update_addr = NULL;
    char percentage_last = 0, percentage = 0;
    u32 i;
    u32 residue_size = 0, file_size = 0, res;
    FILE *xipiboot = 0;
    u8 readbuf[VERIFY_BIN_LEN], verifybuf[VERIFY_BIN_LEN];
    if(Iboot_GetUpdateSource() == 1)
    {
        if(run_mode == 1)
        {
            if(Iboot_GetUpdateIboot() == true)
            {
//                qspi_dcache_drv_desc qspi_cfg;
//
//                qspi_cfg.mode = 0;        // 0: 1 line mode    3: 4 line mode
//                qspi_cfg.clk_set = 0x0;
//                qspi_cfg.wr_command = 0x02;        //write
//                qspi_cfg.rd_command = 0x03;        //read
//                qspi_cfg.wr_dummy_size = 0;
//                qspi_cfg.rd_dummy_size = 0x00;
//
//                bk_qspi_mode_start(1, 0);
//                if(bk_qspi_dcache_configure(&qspi_cfg))
//                {
//                    printk("QSPI init fail.\r\n");
//                    return false;
//                }

                if(Iboot_GetMutualUpdatePath(Iboot_Update, sizeof(Iboot_Update)) == false)
                {
                    printk("Get iboot update info fail.\r\n");
                    return false;
                }
                for(i = 0; i < 22; i ++)
                {
                    if(Iboot_Update[i] == -1)
                    {
                        Iboot_Update[i] = 0;
                    }
                }
                psram_update_addr = (char *)atoi(Iboot_Update);
                printk("psram_update_addr   = %x,\r\n",psram_update_addr);
                if(psram_update_addr)
                {
                    file_size = atoi(Iboot_Update + 11);
                    printk("file_size   = %d,\r\n",file_size);
                    if(file_size)
                    {
                        residue_size = file_size;
                        memcpy(readbuf, psram_update_addr, VERIFY_BIN_LEN);
                        memcpy(verifybuf, readbuf, VERIFY_BIN_LEN);
                        i = 0;
                        while(i < VERIFY_BIN_LEN)
                        {
                           i += 32;
                           memset(verifybuf + i, 0xff, 2);
                           i += 2;
                        }
                        void calc_crc(u32 *buf, u32 packet_num);
                        calc_crc((u32 *)verifybuf, 256 / 32);
                        SetOperFalshMode(true);
                        for(i = 0; i < VERIFY_BIN_LEN; i++)
                        {
                           if(verifybuf[i] != readbuf[i])
                               SetOperFalshMode(false);
                        }
//                        if(GetOperFalshMode() == false)
//                            printk("没有crc的bin文件");
//                        else
//                            printk("有crc的bin文件");
                        if(GetOperFalshMode() == false)
                        {
                            if(file_size > (gc_ptIbootSize))
                            {
                                printk("file size = %d too long",file_size);
                                return false;
                            }
                        }
                        else
                        {
                            if(file_size > (gc_ptIbootSize * 34 / 32))
                            {
                                printk("file size = %d too long",file_size);
                                return false;
                            }
                        }

                        flash_protection_op(0, FLASH_PROTECT_NONE);
                        sddev_control(WDT_DEV_NAME, WCMD_RELOAD_PERIOD, 0); //喂硬狗
                        xipiboot = fopen("/xip-iboot/iboot.bin", "w+");
                        if(xipiboot)
                        {
                            printk("\r\nloading       ");
                            while(1)
                            {
                                percentage = 100 - ((char)((residue_size * 100)/ file_size));
                                if(percentage != percentage_last)
                                {
                                    printk("\b\b\b%2d%%",percentage);
                                    percentage_last = percentage;
                                }
                                sddev_control(WDT_DEV_NAME, WCMD_RELOAD_PERIOD, 0);
                                if(residue_size > 1024)
                                {
                                    res = fwrite(psram_update_addr, 1, 1024, xipiboot);
                                    if(res != 1024)
                                        printf("write file xip-iboot error  \r\n ");
                                    psram_update_addr += res;
                                    residue_size -= res;
                                }
                                else
                                {
                                    res = fwrite(psram_update_addr, 1, residue_size, xipiboot);
                                    if(res != residue_size)
                                        printf("write file xip-iboot error  \r\n ");
                                    psram_update_addr += res;
                                    residue_size -= res;
                                }
                                if(residue_size == 0)
                                {
                                    info_printf("IAP","iboot update success.\r\n");
                                    break;
                                }
                                if((s32)residue_size < 0)
                                {
                                    info_printf("IAP","iboot update fail.\r\n");
                                    break;
                                }
                            }

                            fclose(xipiboot);
                            Iboot_ClearRunAppUpdateIboot();
                            flash_protection_op(0,FLASH_PROTECT_ALL);
                            sddev_control(WDT_DEV_NAME, WCMD_RELOAD_PERIOD, 0);
                            if(residue_size != 0)
                            {
                                printk("update success   residue_size  =%d .\r\n",residue_size);
                                return false;
                            }
                            else
                            {
                                printk("update iboot .\r\n");
                                return true;
                            }
                        }
                        flash_protection_op(0,FLASH_PROTECT_ALL);
                    }
                }
            }
        }
    }
    return false;
}

#define USER_DEFINE_UPDATE_IBOOT_PATH       "/SD/iboot.bin"
bool_t update_iboot(char *param)
{
    (void)param;
    FILE *srciboot = NULL;
    char *iboot_update_addr = 0;
    char iboot_info[MutualPathLen];
    struct stat test_stat;
    u32 i,readsize = 0, file_size = 0;
    if(!stat(USER_DEFINE_UPDATE_IBOOT_PATH,&test_stat))
    {
        printf("find file   = %s,\r\n",USER_DEFINE_UPDATE_IBOOT_PATH);
        if(test_stat.st_size < 0x400000)
        {
            iboot_update_addr = GetUpdateBufAddr();//my_malloc(512*1024);
            printf("iboot_update_addr   = %x,\r\n",(u32)iboot_update_addr);
            if(iboot_update_addr)
            {
                srciboot = fopen(USER_DEFINE_UPDATE_IBOOT_PATH, "r+");
                if(srciboot)
                {
                    file_size = (u32)test_stat.st_size;
                    printf("file_size   = %d,\r\n",file_size);
                    readsize = fread(iboot_update_addr, 1, file_size, srciboot);
                    if(readsize == file_size)
                    {
                        printf("read memory successful\r\n");
                        memset(iboot_info, 0xff, MutualPathLen);
                        itoa((s32)iboot_update_addr, iboot_info, 10);
                        itoa((s32)file_size, iboot_info + 11, 10);
                        for(i = 0; i < 22; i ++)
                        {
                            if(iboot_info[i] == 0)
                            {
                                iboot_info[i] = 0xff;
                            }
                        }
                        Iboot_SetRunAppUpdateIboot();
                        set_upgrade_info(iboot_info, MutualPathLen);
                        fclose(srciboot);
                        sddev_control(WDT_DEV_NAME, WCMD_POWER_DOWN, 0);
                        Iboot_SetUpdateSource(SOURCE_ADDR_MEMORY);  //设置升级方式为，从ram获取iboot数据
                        Set_RunAppFlag();
                        CPU_RestartSystem(0);

                    }
                    else
                    {
                        printf("read memory fail\r\n");
                        fclose(srciboot);
                    }
                }
            }
        }
        else
        {
            printf("file size too large\r\n");
        }
    }
    else
        printf("no file found。\r\n");
    return true;
}

void network_update_exe(void)
{
    int ret;
    u32 file_size;
    u8 time_buf[4],production[5];
    char app_info[MutualPathLen];
    u8 app_info_size = 0;

    if(Get_check_verify() == VerifySuccess)
    {
        file_size = update_addr - update_start_addr;
        memset(app_info, 0, MutualPathLen);
        memcpy(app_info + app_info_size, &update_start_addr, sizeof(update_start_addr));
//            printf("sizeof(update_start_addr)       =        %d.\r\n",sizeof(update_start_addr));
        app_info_size += sizeof(update_start_addr);
        memcpy(app_info + app_info_size, &file_size, sizeof(file_size));
//          printf("sizeof(file_size)       =        %d.\r\n",sizeof(file_size));
        app_info_size += sizeof(file_size);
        ret = GetSrvUpgradeInfo(time_buf, 4, production, 5);
        if(ret != -1)
        {
            printf("time_buf = %4.4s    production = %5.5s   \r\n",time_buf, production);
            memcpy(app_info + app_info_size, time_buf, sizeof(time_buf));
        }
//            printf("sizeof(time_buf)       =        %d.\r\n",sizeof(time_buf));
        app_info_size += sizeof(time_buf);

        if(ret != -1)
            memcpy(app_info + app_info_size, &production, sizeof(production));
        app_info_size += sizeof(production);

        if((app_info_size + sizeof(NETWORK_UPDATEAPP_FILE_NAME)) > MutualPathLen)
        {
            printf("ERROR: iboot_info length exceeded!\r\n");
            return  ;
        }
        memcpy(app_info + app_info_size, NETWORK_UPDATEAPP_FILE_NAME, sizeof(NETWORK_UPDATEAPP_FILE_NAME));
        set_update_result_flag(UpdateResult_success);
        printf("======Set_RunIbootUpdateApp=====\r\n");
        Iboot_SetRunIbootUpdateApp();
        set_upgrade_info(app_info, MutualPathLen);
        printf("update_start_addr   = %x,\r\n",(u32)update_start_addr);
        printf("update_addr   = %x,\r\n",(u32)update_addr);
        printf("read memory successful。\r\n");
        Iboot_SetUpdateSource(SOURCE_ADDR_MEMORY);  //设置升级方式为，从ram获取app数据
        DJY_EventDelay(2000*1000);
        DjyWifi_StaDisConnect();//关wifi
        DJY_EventDelay(1000*1000);
        sddev_control(WDT_DEV_NAME, WCMD_POWER_DOWN, 0);
        Set_RunIbootFlag();
        CPU_RestartSystem(0);
    }
    else if(Get_check_verify() == VerifyFail)
    {
        set_update_result_flag(UpdateResult_check_fail);
        DJY_EventDelay(10000*1000);
//            Set_update_flag(0);
        runapp(0);
    }
}



ADD_TO_ROUTINE_SHELL(sdupdateiboot,update_iboot,"读sd卡里的iboot文件升级iboot，COMMAND:sdupdateiboot+enter.");
