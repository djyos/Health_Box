#include "mongoose.h"
#include "cJSON.h"
#include <Iboot_info.h>
//#include "../comm/comm_api.h"
#include <time.h>
#include <cpu_peri_gpio.h>
#include <gpio_pub.h>
#include <upgrade.h>
#include <time.h>

#ifndef free
#define free(x) M_Free(x)
#endif

#define UPGRADE_MODE    "update"  //"burn"or"update"
#define UPGRADE_MODE_REBURN  "reburn"
typedef struct StDevUpgrade {
    char host[100];
    unsigned short port;
}StDevUpgrade;

typedef struct StUpdData {
    int status;
    char *new_data;
    int new_len;
}StUpdData;

struct StDevUpgrade gDevUpgrade;

int str2bcd(char *strDate, unsigned char *buf, int len);

int DoUpgradeJson(char *body, int len, char *outbuf, int outlen)
{
	int ret = 0;
	cJSON *cjson = 0;
	char *pJsonStr = 0;
	if (body == 0 || len <= 0) return -1;
	cJSON*  sub_item = 0;

	cjson = cJSON_Parse(body);
	if (cjson)
	{
		cJSON*  results = cJSON_GetObjectItem(cjson, "version");
		if (results)
		{
			sub_item = cJSON_GetObjectItem(results, "url");
			if (sub_item)
			{
				pJsonStr = cJSON_PrintUnformatted(cjson);
				//printf("json_str: %s!\r\n\r\n", pJsonStr);
				ret = strlen(pJsonStr) + 1;
				if (outbuf == 0 || outlen < ret) {
					ret = -2;
					goto END_FUN;
				}
				strcpy(outbuf, pJsonStr);
				ret = 1;
			}
		}
		else {//发送确认升级回应包
		    cJSON*  results = cJSON_GetObjectItem(cjson, "status");
		    if (results) {
		        pJsonStr = cJSON_PrintUnformatted(cjson);
                ret = strlen(pJsonStr) + 1;
                if (outbuf == 0 || outlen < ret) {
                    ret = -2;
                    goto END_FUN;
                }
                strcpy(outbuf, pJsonStr);
                ret = 1;
		    }
		}
	}

END_FUN:
	if (cjson) cJSON_Delete(cjson);
	if (pJsonStr) free(pJsonStr);
	return ret;
}

static void cb_upgrade_ev_handler(struct mg_connection *nc, int ev, void *ev_data) {
    struct http_message *hm = (struct http_message *) ev_data;
    struct StUpdData *pQuestData = (struct StUpdData*)nc->user_data;
    char *p = 0;

    int is_chunked = 0;
    struct mg_str *s;
    if (hm) {
        if ((s = mg_get_http_header(hm, "Transfer-Encoding")) != NULL &&
            mg_vcasecmp(s, "chunked") == 0) {
            is_chunked = 1;
        }
    }

    switch (ev) {
    case MG_EV_CONNECT:
        if (*(int *)ev_data != 0) {
            /*  fprintf(stderr, "connect() failed: %s\n", strerror(*(int *)ev_data));*/
            pQuestData->status = -1;
        }
        if (pQuestData->new_data) {
            free(pQuestData->new_data);
        }
        pQuestData->new_data = 0;
        pQuestData->new_len = 0;
        break;
    case MG_EV_HTTP_REPLY:
    {
        switch (hm->resp_code) {
        case 200:
            nc->flags |= MG_F_CLOSE_IMMEDIATELY;

            if (hm->body.len <= 0) {
                if (pQuestData->status == 0) pQuestData->status = -1;
                break;
            }

            //process the whole body.
            pQuestData->status = -1;
            p = realloc(pQuestData->new_data, hm->body.len+1);
            if (p == 0) {
                printf("error: cb_ev_handler->realloc MG_EV_HTTP_REPLY, p==null!\r\n");
                break;
            }
            pQuestData->new_data = p;
            pQuestData->new_len = hm->body.len+1;
            memcpy(pQuestData->new_data, hm->body.p, hm->body.len);
            pQuestData->new_data[hm->body.len] = 0;

            if (DoUpgradeJson(pQuestData->new_data, pQuestData->new_len, pQuestData->new_data, pQuestData->new_len) > 0) {
                printf("%s\r\n", pQuestData->new_data);
                pQuestData->status = 1;
            }
            break;
        case 302:
            pQuestData->status = -302;
            break;
        case 401:
            pQuestData->status = -401;
            break;
        default:
            pQuestData->status = -1;
            break;
        }
        break;
    }

    case MG_EV_HTTP_CHUNK:
    {
        if (!is_chunked) break;

        nc->flags = MG_F_DELETE_CHUNK;
        //hex_dump("cb_http_upload_handler MG_EV_HTTP_CHUNK:", hm->body.p, hm->body.len);
        if (hm->body.len > 0) {
            p = realloc(pQuestData->new_data, pQuestData->new_len + hm->body.len);
            if (p == 0) {
                printf("error: cb_ev_handler->realloc MG_EV_HTTP_CHUNK, p==null!\r\n");
                break;
            }
            pQuestData->new_data = p;
            memcpy(&pQuestData->new_data[pQuestData->new_len], hm->body.p, hm->body.len);
            pQuestData->new_len += hm->body.len;
        }
        else if (hm->body.len == 0) {//end flag
            if (pQuestData->status != 0) break;
            p = realloc(pQuestData->new_data, pQuestData->new_len+1);
            if (p == 0) {
                printf("error: cb_ev_handler->realloc MG_EV_HTTP_CHUNK, p==null!\r\n");
                break;
            }
            pQuestData->new_data = p;
            pQuestData->new_data[pQuestData->new_len] = 0;
            pQuestData->new_len += 1;

            //process the whole body.
            pQuestData->status = -1;

            if (DoUpgradeJson(pQuestData->new_data, pQuestData->new_len, pQuestData->new_data, pQuestData->new_len) > 0) {
                printf("%s\r\n", pQuestData->new_data);
                pQuestData->status = 1;
            }
        }
        break;
    }
    case MG_EV_CLOSE:
        break;
    default:
        break;
    }
}



int DevUpgradeCommon(char *path, char *out_json, int len)
{
    struct StUpdData user_data;
    memset(&user_data, 0, sizeof(user_data));
    struct StDevUpgrade *pDevMgr = &gDevUpgrade;
    int ret = 0;
    char *meta = 0;
    char *url = 0;
    struct mg_connection *nc = 0;
    struct mg_mgr mgr;


    mg_mgr_init(&mgr, NULL);

    meta = (char*)malloc(1024);
    if (meta == 0) return -1;

    url = (char*)malloc(1024);
    if (url == 0) return -1;

    user_data.new_len = 0;
    user_data.new_data = 0;
    user_data.status = 0;

    sprintf(meta,
        "Accept: */*\r\n"
        "Connection: keep-alive\r\n"
        "User-Agent: Mongoose/6.15\r\n"
        "Cache-Control: no-cache\r\n"
        "Content-Type: application/json\r\n");

    sprintf(url, "http://%s%s", pDevMgr->host, path);
    nc = mg_connect_http(&mgr, cb_upgrade_ev_handler, url, meta, 0);
    if (nc == 0) goto END_QUEST;
    nc->user_data = &user_data;
    unsigned int timemark = DJY_GetSysTime()/1000;
    unsigned int timeout_ms = 15000;
    while (user_data.status == 0) {
        if (DJY_GetSysTime()/1000-timemark > timeout_ms) {
            printf("DevGetCommon: timeout break!\r\n");
            break;
        }
        mg_mgr_poll(&mgr, 500);
        DJY_EventDelay(10*1000);
    }
    if (user_data.status == 1) {
        if (user_data.new_data) {
            int min = strlen(user_data.new_data) + 1;
            min = min < len ? min : len;
            memcpy(out_json, user_data.new_data, min);
            user_data.status = min;
        }
        else {
            user_data.status = 0;
        }
    }
    ret = user_data.status;
END_QUEST:
    mg_mgr_free(&mgr);
    if (meta) free(meta);
    if (url) free(url);
    if (user_data.new_data) {
        free(user_data.new_data);
        user_data.new_data = 0;
        user_data.new_len = 0;
    }
    return ret;
}

int DevUpgradeQuest(char *version, const char *serial_num, char *out_json, int len,char *mode)
{
    char path[200] = { 0 };
    printf("ENTRY: %s!\r\n", __FUNCTION__);
    int is_wifi_connected();
    if (!is_wifi_connected()) return -1;
    //&action=burn表示生产第一次烧录，=updatae表示使用的时候进行升级
//    sprintf(path, "/api/version?model_no=%6.6s&sn=%s&version=%s&action=%s", serial_num, serial_num, version, mode);
    sprintf(path, "/api/version?model_no=%6.6s&sn=%6.6s%s&version=%s&action=%s", serial_num, serial_num, "20090000Q",version, mode);
    int ret = DevUpgradeCommon(path, out_json, len);
    printf("EXIT: %s!\r\n", __FUNCTION__);
    return ret;
}


static unsigned char gserial_num[5] = {0,0,0,0,0};
static unsigned char gproduct_time[4] = {0,0,0,0};
static unsigned char gupgrade_url[256];
static  char gupgrade_version[20];
static enum UpgradeMode gupgrade_mode = 0;

char *Get_Upgrade_version(void)
{
    return gupgrade_version;
}

int GetSrvUpgradeInfo(unsigned char *product_time, int prod_time_len, unsigned char *serial_num, int ser_num_len)
{
	int ret = 0;
	if (gserial_num[0] == 0 || gproduct_time[0] == 0) return -1;

	if (serial_num && ((int)sizeof(gserial_num) <= ser_num_len)) {
	    memcpy(serial_num, gserial_num, sizeof(gserial_num));
//	    *serial_num = gserial_num;
	}
	if (product_time && ((int)sizeof(gproduct_time) <= prod_time_len)) {
	    memcpy(product_time, gproduct_time, sizeof(gproduct_time));
	}
//	memset(gserial_num, 0, sizeof(gserial_num));
//	memset(gproduct_time, 0, sizeof(gproduct_time));
	return ret;
}

enum UpgradeMode GetUpgradeMode(void)
{
    return gupgrade_mode;
}
int DevDoUpgrade()
{
    int ret = -1;
    char buf[30] = {0};
	cJSON *cjson = 0;
	cJSON *cjson_version = 0;
	cJSON *cjson_device = 0;
	int weeks = 0;
	char *out_json = malloc(1024);
	if (out_json==0) return -1;
//    struct AppHead app_head;
    char VersionNum[9];
    char SN_Num[16];
    memset(VersionNum, 0, sizeof(VersionNum));
    memset(SN_Num, 0, sizeof(SN_Num));
//    memset(&app_head, 0, sizeof(struct AppHead));
//    bool_t Iboot_GetAppHead(void * apphead);
//    Iboot_GetAppHead(&app_head);
//    sprintf(VersionNum, "%d.%d.%d", (int)app_head.VersionNumber[0],
//            (int)app_head.VersionNumber[1] ,(int)app_head.VersionNumber[2]);

    if(Iboot_GetAPP_ProductInfo(APP_HEAD_VERSION_NUM, VersionNum, sizeof(VersionNum)) == false)
        goto FUN_RET;

    printf("--------send VersionNumber  = %s ",VersionNum);

    if(Iboot_GetAPP_ProductInfo(APP_HEAD_FINGER, SN_Num, sizeof(SN_Num)) == false)
        goto FUN_RET;

    printf(" SN_Num   = %s,\r\n", SN_Num);
	memset(out_json, 0, 1024);
    if (DevUpgradeQuest(VersionNum, SN_Num, out_json, 1024, UPGRADE_MODE) <= 0) {
		goto FUN_RET;
	}

	cjson = cJSON_Parse(out_json);
	cJSON*  results = 0;
	if (cjson)
	{
		cjson_version = cJSON_GetObjectItem(cjson, "version");
		if (cjson_version) {
			results = cJSON_GetObjectItem(cjson_version, "url");
			if (results) {
			    printf("==url==: %s!\r\n", results->valuestring);
			    strcpy((char*)gupgrade_url, results->valuestring);
			    results = cJSON_GetObjectItem(cjson_version, "upgrade");
	            if (results) {
	                printf("==upgrade==: %d!\r\n", results->valueint);
	                gupgrade_mode = results->valueint;
	                ret = 1;
	            }
			}

			results = cJSON_GetObjectItem(cjson_version, "version");
			if (results){
			    printf("==version==: %s!\r\n", results->valuestring);
			    if(sizeof(gupgrade_version) > (strlen(results->valuestring)+1))
			        strcpy((char*)gupgrade_version, results->valuestring);
			}
		}
		//device
		cjson_device = cJSON_GetObjectItem(cjson, "device");
		if (cjson_device) {
		    results = cJSON_GetObjectItem(cjson_device, "weekday");
		    if (results) {
                printf("==weekday==: %d!\r\n", results->valueint);
                weeks = results->valueint;
		    }
            results = cJSON_GetObjectItem(cjson_device, "productNo");
            if (results) {
                printf("==productNo==: %s!\r\n", results->valuestring);
                memset(gserial_num, 0, sizeof(gserial_num));
                memcpy(gserial_num, results->valuestring, sizeof(gserial_num));
            }
            results = cJSON_GetObjectItem(cjson_device, "produceTime");
            if (results) {
                printf("==produceTime==: %s!\r\n", results->valuestring);
                struct tm stm = { 0 };
                int cnt = sscanf(results->valuestring, "%d-%d-%d %d:%d:%d", &stm.tm_year, &stm.tm_mon, &stm.tm_mday, &stm.tm_hour, &stm.tm_min, &stm.tm_sec);
                if (cnt == 6) {
                    memset(buf, 0, sizeof(buf));
                    //sprintf(buf, "%04d%02d", stm.tm_year, weeks);
                    sprintf(buf, "%02d%02d", stm.tm_year%100, weeks);
                    printf("===>PRODUCT TIME: %s!\r\n", buf);
                    memset(gproduct_time, 0, sizeof(gproduct_time));
                    memcpy(gproduct_time, buf, sizeof(gproduct_time));
                    ret = 1;
                }
            }
		}
	}
FUN_RET:
    if (out_json) free(out_json);
    return ret;
}
//"20191112550"
int str2bcd(char *strDate, unsigned char *buf, int len)
{
	unsigned char ch = 0;
	size_t i = 0;
	int j = 0;
	if (strDate == 0) return 0;
	for (i=0; i<strlen(strDate); i++){
		if (i % 2 == 0) {
			ch = (strDate[i] - '0') << 4;
		}
		else {
			ch |= (strDate[i] - '0');
			j = i / 2;
			if (j<len) {
				buf[j] = ch;
			}
		}
	}
	return j?j+1:0;
}

int web_upgrade_firmware(int check)
{
//    static int do_update = 0;
    int ret = -2;
    char *p1 = 0;
    char *p2 = 0;
    char domain[100] = { 0 };
    memset(&gDevUpgrade, 0, sizeof(gDevUpgrade));
    strcpy(gDevUpgrade.host, PRODUCT_OTA_ADDRESS);
    gDevUpgrade.port = 80;

//    if (do_update==0) {
        gupgrade_url[0] = 0;
        ret = DevDoUpgrade();
//    }
    if (check==1) {

        if (gupgrade_url[0] != 0) {
//            do_update = 1;
            return 1;
        }
        else {
//            do_update = 0;
            return -1;
        }
    }
    memset(domain, 0, sizeof(domain));
    p1 = 0;
    p2 = 0;
    if (gupgrade_url[0]) {
        p1 = (char*)gupgrade_url;
        if (strstr((char*)gupgrade_url, "https://") == (char*)gupgrade_url) {
            p1 = p1 + strlen("https://");
            p2 = strstr(p1, "/");
            if (p1 && p2 && p2>p1) {
                memcpy(domain, p1, p2 - p1);
            }
        }
        else if (strstr((char*)gupgrade_url, "http://") == (char*)gupgrade_url) {
            p1 = (char*)&gupgrade_url[strlen("http://")];
            p2 = strstr(p1, "/");
            if (p1 && p2 && p2>p1) {
                memcpy(domain, p1, p2 - p1);
            }
        }

        printf("domain=%s, path=%s!\r\n", domain, p2);
        if (domain[0] && p2) {
            int network_update(u8 *buf,s32 len, int fsize, int timeout);
            ret = WebDownload(domain, 80, p2, network_update, 5000);
            printf("=====WebDownload ret=%d======!\r\n", ret);
        }
    }
    if((ret == -1) || (ret == 0))
    {
        set_update_result_flag(UpdateResult_network_download_fail);
        DJY_EventDelay(10000*1000);
//        Set_update_flag(0);
        runapp(0);
    }
//
//    if(ret == -1)
//        do_update = 0;

    return ret;
}
