
//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * app_main.c
 *
 *  Created on: 2014-5-28
 *      Author: Administrator
 */

#include "stdint.h"
#include "stddef.h"
#include "stdio.h"
#include <cpu_peri_gpio.h>
#include <gpio_pub.h>
#include <Iboot_info.h>
#include "qspi_pub.h"
//#include "qspi/qspi_pub.h"
#include <flash_pub.h>
#include <dbug.h>
#include <wdt_pub.h>
#include <string.h>
#include <djyfs/filesystems.h>
//#include <boot.h>
#include "shell.h"

#define VERIFY_BIN_LEN   272
extern void flash_protection_op(UINT8 mode, PROTECT_TYPE type);
extern void calc_crc(u32 *buf, u32 packet_num);
extern void OpenBackLight();

static bool_t updateapp(char *addr,u32 file_size, char *file_name)
{
    FILE *xipapp = 0;
    u32 res, residue_size = (s32)file_size;
    char xipapppath[MutualPathLen];
    char percentage_last = 0, percentage = 0;
    flash_protection_op(0,FLASH_PROTECT_NONE);
    if(strlen(file_name) > 30)
    {
        printf("Filename is too long  %s ,\r\n", file_name);
        return false;
    }
    sprintf(xipapppath, "%s%s", "/xip-app/", file_name);
    xipapp = fopen(xipapppath, "w+");
    if(xipapp)
    {
        printf("open file success   %s ,\r\n", xipapppath);
        printf("\r\nloading       ");
        while(1)
        {
            percentage = 100 - ((char)((residue_size * 100)/ file_size));
            if(percentage != percentage_last)
            {
                printf("\b\b\b%2d%%",percentage);
                percentage_last = percentage;
            }
            if(residue_size > 1024)
            {
                res = fwrite(addr, 1, 1024, xipapp);
                if(res != 1024)
                    printf("write file xip-app error  \r\n ");
                addr += res;
                residue_size -= res;
            }
            else
            {
                res = fwrite(addr, 1, residue_size, xipapp);
                if(res != residue_size)
                    printf("write file xip-app error  \r\n ");
                addr += res;
                residue_size -= res;
            }
            if(residue_size == 0)
            {
                info_printf("IAP","app update success.\r\n");
                break;
            }
            if((s32)residue_size < 0)
            {
                info_printf("IAP","app update fail.\r\n");
                break;
            }
        }
        fclose(xipapp);
        Iboot_ClearRunIbootUpdateApp();
        if(residue_size == 0)
        {
            Iboot_SetUpdateRunModet(1);
            Iboot_UpdateToRun();
        }
    }
    else
        printf("open file fail,\r\n");

    flash_protection_op(0,FLASH_PROTECT_ALL);
    return true;
}
//void init_qspi_psram(void)
//{
//    qspi_dcache_drv_desc qspi_cfg;
//
//    qspi_cfg.mode = 0;        // 0: 1 line mode    3: 4 line mode
//    qspi_cfg.clk_set = 0x0;
//    qspi_cfg.wr_command = 0x02;        //write
//    qspi_cfg.rd_command = 0x03;        //read
//    qspi_cfg.wr_dummy_size = 0;
//    qspi_cfg.rd_dummy_size = 0x00;
//
//    bk_qspi_mode_start(1, 0);
//    if(bk_qspi_dcache_configure(&qspi_cfg))
//    {
//        printf("QSPI init fail.\r\n");
//    }
//}
extern bool_t Iboot_UpdateApp(void);
//extern void usb_charge_check_cb(void);
//extern void usb_plug_func_open(void);
//u16  testuart;
//ptu32_t uartreceive(void)
//{
//    u32 serial = 0;
//    u32 num;
//    u32 offset;
//    volatile u32 error=0,right=0;
//    u8 buf[256];
//    u8 bak = 255;
//    while(1)
//    {
//        num = fread(buf,1,256,stdin);
//        for(offset = 0; offset < num; offset++)
//        {
//            if(buf[offset] != (u8)(bak+1))
//                error++;
//            else
//                right++;
//            bak = buf[offset];
//            serial++;
//        }
//    }
//}
//extern const u8 StartBitmap;

extern u32 gc_pAppRange;
ptu32_t djy_main(void)
{
    int i = 0;//,j = 49;
    struct stat test_stat;
    char app_info[MutualPathLen];
    char *psram_update_addr = 0;
    u32  res, file_size = 0,limitsize;
    FILE *fp = NULL;
    u8 readbuf[VERIFY_BIN_LEN], verifybuf[VERIFY_BIN_LEN];
    struct ProductInfo p_productinfo;
    char production_num[sizeof(p_productinfo.ProductionNumber)],production_time[sizeof(p_productinfo.ProductionTime)];
    char file_name[MutualPathLen - sizeof(psram_update_addr) - sizeof(file_size) - sizeof(production_time) - sizeof(production_num)];
    u8 app_info_size = 0;

//    u8 *StartBMP = StartBitmap;

//    testuart = DJY_EvttRegist(EN_CORRELATIVE,CN_PRIO_RRS,0,0,
//        uartreceive,NULL,0x1000, "testuart");
//    //事件的两个参数暂设为0,如果用shell启动,可用来采集shell命令行参数
//    DJY_EventPop(testuart,NULL,0,0,0,0);
//    u8 buf[256];
//    for(i = 0; i < 256; i++)
//        buf[i] = i;
//    while(1)
//    {
//        fwrite(buf, 1, 256, stdout);
//    }



//  usb_plug_func_open();   //充电功能开启函数
//    Vbat = vbat_voltage_get();
//    printf("Vbat = %d  \r\n", Vbat);    //显示当前的电池电压
//    usb_charge_check_cb();      //判断是否开始和结束充电的函数

    sddev_control(WDT_DEV_NAME, WCMD_POWER_DOWN, 0);

    if(Iboot_GetUpdateApp() == true)
    {
        OpenBackLight();
        DJY_EventDelay(1000*1000);
        if(Iboot_GetMutualUpdatePath(app_info, sizeof(app_info)) == false)
        {
            printf("app file info get fail\r\n");
            return false;
        }
        if(Iboot_GetUpdateSource() == 0)
        {
            if((!stat(CFG_FORCED_UPDATE_PATH,&test_stat)) || (!stat(CFG_APP_UPDATE_NAME,&test_stat)) || (!stat(app_info,&test_stat)))
            {
                if(!stat(CFG_FORCED_UPDATE_PATH,&test_stat))
                {
                    fp = fopen(CFG_FORCED_UPDATE_PATH, "r");
                }
                else if(!stat(app_info,&test_stat))
                {
                    fp = fopen(app_info, "r");
                }
                else if(!stat(CFG_APP_UPDATE_NAME,&test_stat))
                {
                    fp = fopen(CFG_APP_UPDATE_NAME, "r");
                }
                if(fp)
                {
                    memset(readbuf, 0xFF, VERIFY_BIN_LEN);
                    memset(verifybuf, 0xFF, VERIFY_BIN_LEN);
                    res = fread(readbuf, 1, VERIFY_BIN_LEN, fp);
                    fclose(fp);
                    if(res == VERIFY_BIN_LEN)
                    {
                        memcpy(verifybuf, readbuf, VERIFY_BIN_LEN);
                        while(i < VERIFY_BIN_LEN)
                        {
                            i += 32;
                            memset(verifybuf + i, 0xff, 2);
                            i += 2;
                        }
                        calc_crc((u32 *)verifybuf, 256 / 32);
                        SetOperFalshMode(true);
                        limitsize = gc_pAppRange * 34 /32;
                        for(i = 0; i < VERIFY_BIN_LEN; i++)
                        {
                            if(verifybuf[i] != readbuf[i])
                            {
                                SetOperFalshMode(false);
                                limitsize = gc_pAppRange;
                                break;
                            }
                        }
                        if(test_stat.st_size <= limitsize)
                        {
                            flash_protection_op(0,FLASH_PROTECT_NONE);
                            printf("Start erase flash\r\n");
                            if(!File_Format("/xip-app"))
                            {
                                printf("Erase flash succeed\r\n");
                                Iboot_SetRunIbootUpdateApp();
                                Iboot_UpdateApp();
//                                if(GetOperFalshMode() == false)
//                                {
//                                    printf("没有crc的bin");
////                                    printf("开始擦除flash\r\n");
////                                    if(!File_Format("/xip-app"))
////                                    {
////                                        printf("擦除flash完成\r\n");
//                                    Iboot_SetRunIbootUpdateApp();
//                                    Iboot_UpdateApp();
////                                    }
//                                }
//                                else
//                                {
//                                    printf("有crc的bin");
//                                    Iboot_SetRunIbootUpdateApp();
//                                    Iboot_UpdateApp();
//                                }
                            }
                            flash_protection_op(0,FLASH_PROTECT_ALL);
                        }
                        else
                        {
                            printf("update file size too long\r\n");
                            fclose(fp);
                        }
                    }
                    else
                    {
                        printf("update file size too little\r\n");
                        fclose(fp);
                    }
                }
            }
            else
                printf("update file not found\r\n");
        }
        else if(Iboot_GetUpdateSource() == 1)
        {
            if(Iboot_GetUpdateApp() == true)
            {
                extern int ModuleInstall_QSPI_PSRAM(void);
                ModuleInstall_QSPI_PSRAM();
                Iboot_GetProductInfo(&p_productinfo);
                memcpy(&psram_update_addr, app_info + app_info_size, sizeof(psram_update_addr));
                printf("psram_update_addr   = %x,\r\n", psram_update_addr);
                app_info_size += sizeof(psram_update_addr);
//                file_size = atoi(app_info + 11);
                memcpy(&file_size, app_info + app_info_size, sizeof(file_size));
                printf("file_size   = %d,\r\n",file_size);
                app_info_size += sizeof(file_size);

                if((p_productinfo.VersionNumber[0] == 0) &&
                        (p_productinfo.VersionNumber[1] == 0) && (p_productinfo.VersionNumber[2] == 0))
                {
                    memcpy(&production_time, app_info + app_info_size, sizeof(production_time));
                    printf("production_time  = %4.4s,\r\n",production_time);
                    app_info_size += sizeof(production_time);

                    memcpy(&production_num, app_info + app_info_size, sizeof(production_num));
                    printf("production_num   = %5.5s,\r\n",production_num);
                    app_info_size += sizeof(production_num);
                }
                else
                {
                    memcpy(&production_time, p_productinfo.ProductionTime, sizeof(production_time));
                    printf("production_time  = %4.4s,\r\n",production_time);
                    app_info_size += sizeof(production_time);

                    memcpy(&production_num, p_productinfo.ProductionNumber, sizeof(production_num));
                    printf("production_num   = %5.5s,\r\n",production_num);
                    app_info_size += sizeof(production_num);
                }


                strcpy(file_name, app_info + app_info_size);
                printf("file_name   = %s,\r\n",file_name);
                if(file_size < 0x380000)
                {
                    if(XIP_AppFileCheck(psram_update_addr))
                    {

                        printf("File check success\r\n");
                        memcpy(readbuf, psram_update_addr, VERIFY_BIN_LEN);
                        memcpy(verifybuf, readbuf, VERIFY_BIN_LEN);
                        i = 0;
                        while(i < VERIFY_BIN_LEN)
                        {
                            i += 32;
                            memset(verifybuf + i, 0xff, 2);
                            i += 2;
                        }
                        calc_crc((u32 *)verifybuf, 256 / 32);
                        SetOperFalshMode(true);
                        for(i = 0; i < VERIFY_BIN_LEN; i++)
                        {
                            if(verifybuf[i] != readbuf[i])
                                SetOperFalshMode(false);
                        }
                        printf("Start erase flash\r\n");
                        if(!File_Format("/xip-app"))
                        {
                            printf("Erase flash succeed\r\n");
                            Iboot_RewriteProductInfoNumTime(psram_update_addr + sizeof(struct AppHead), production_time, production_num);
                            updateapp(psram_update_addr, file_size, file_name);
                        }
                    }
                    else
                    {
                        printf("File check fail\r\n");
                    }
                }
                else
                {
                    printf("update file size too long\r\n");
                }
            }
        }
    }
    else
    {
        if(!Iboot_GetRestartAppFlag())
        {
            Set_RunAppFlag();
            CPU_RestartSystem(0);
        }
    }
    while(1)
    {
        djy_gpio_write(GPIO11,0);
        DJY_EventDelay(300*1000);
        djy_gpio_write(GPIO11,1);
        DJY_EventDelay(300*1000);

    }
    return 0;
}

