/****************************************************
 *  Automatically-generated file. Do not edit!	*
 ****************************************************/

void keep_shell(void)
{

    volatile void *keep;
    (void)keep;

    extern void *djysh_initjtag;
    keep = (void *)djysh_initjtag;

    extern void *djysh_format;
    keep = (void *)djysh_format;

    extern void *djysh_heap;
    keep = (void *)djysh_heap;

    extern void *djysh_evtt;
    keep = (void *)djysh_evtt;

    extern void *djysh_iapmode;
    keep = (void *)djysh_iapmode;

    extern void *djysh_resetshell;
    keep = (void *)djysh_resetshell;

    extern void *djysh_help;
    keep = (void *)djysh_help;

    extern void *djysh_f;
    keep = (void *)djysh_f;

}